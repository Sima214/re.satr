#include <LoadScheduler.hpp>
#include <Logger.hpp>
#include <RenderLoop.hpp>
#include <Trace.hpp>
#include <glfw.hpp>
#include <glm/vec3.hpp>
#include <opengl/buffer/VAO.hpp>
#include <opengl/core/Types.hpp>
#include <opengl/program/Program.hpp>
#include <opengl/program/Shader.hpp>
#include <opengl/renderer/CompositeRenderLoops.hpp>
#include <opengl/renderer/Presenter.hpp>
#include <opengl/texture/Framebuffer.hpp>
#include <opengl/texture/Scene.hpp>
#include <opengl/utils/DrawCommands.hpp>
#include <opengl/utils/LoadScheduler.hpp>
#include <opengl/utils/StateUtils.hpp>

#include <cstdint>
#include <cstdlib>
#include <string>

#include <glad.h>
#include <pthread.h>

const std::string VERTEX_SRC = "#version 330\n"
                               "in vec3 position;\n"
                               "void main() {\n"
                               "  gl_Position = vec4(position, 1.0f);\n"
                               "}\n";

const std::string FRAGMENT_SRC = "#version 330\n"
                                 "out vec4 outputColor;\n"
                                 "uniform vec3 color;\n"
                                 "void main() {\n"
                                 "  outputColor = vec4(color, 1.0f);\n"
                                 "}";

const glm::vec3 TRIANGLE[] = {{-1.0f, -.9f, 0.0f}, {-1.0f, .1f, 0.0f}, {-.9f, 0.0f, 0.0f}};

using namespace resatr::opengl;

class TriangleUI final : public NuklearOpenGLScene {
   protected:
    enum { EASY, HARD };
    int op = EASY;
    int property = 20;

   public:
    TriangleUI(OpenGLRenderLoop& root, Scene& parent, glm::ivec2 size) :
        NuklearOpenGLScene(root, parent, size) {}

    virtual void render(nk_context* ctx) {
        if (nk_begin(ctx, "Demo", nk_rect(50, 50, 230, 250),
                     NK_WINDOW_BORDER | NK_WINDOW_MOVABLE | NK_WINDOW_SCALABLE |
                          NK_WINDOW_MINIMIZABLE | NK_WINDOW_TITLE)) {
            nk_layout_row_static(ctx, 30, 80, 1);
            if (nk_button_label(ctx, "button"))
                logger.logd("button pressed!");
            nk_layout_row_dynamic(ctx, 30, 2);
            if (nk_option_label(ctx, "easy", op == EASY))
                op = EASY;
            if (nk_option_label(ctx, "hard", op == HARD))
                op = HARD;

            nk_layout_row_dynamic(ctx, 25, 1);
            nk_property_int(ctx, "Compression:", 0, &property, 100, 10, 1);

            nk_layout_row_dynamic(ctx, 20, 1);
            nk_label(ctx, "background:", NK_TEXT_LEFT);
            nk_layout_row_dynamic(ctx, 25, 1);
        }
        nk_end(ctx);
    }
};

class Triangle final : public OpenGLScene {
   protected:
    Program simple = Program::create_invalid();
    Buffer triangle;
    VertexAttributeObject bindings;

   public:
    Triangle(OpenGLRenderLoop& root, Scene& parent, glm::ivec2 size) :
        OpenGLScene(root, parent, size, true),
        // Upload resources.
        triangle(root.create_array_draw_buffer(TRIANGLE)),
        bindings(root.create_vertex_attributes()) {
        set_clear_color(glm::vec4(0.8, 0.5, 0.5, 0.0));
        VertexShader vertex = root.create_vertex_shader(VERTEX_SRC);
        FragmentShader frag = root.create_fragment_shader(FRAGMENT_SRC);
        // Initialize resources.
        simple = root.create_program(vertex, frag);
        simple.get_uniforms()["color"] = glm::vec3(1.0, 1.0, 1.0);
        auto simple_attr0 = simple.get_attributes()["position"];
        bindings.bind(simple_attr0, triangle, 3, Types::Float);
    }

    virtual bool render() override {
        clear();
        ScopedBind triangle_spec(simple, bindings);
        draw_arrays(VertexFormats::Triangles, 0, 3);
        return false;
    }

    virtual bool on_keyboard_button_press(const resatr::KeyboardButtonPress& event) override {
        logger.logi("Keypress: ", event);
        if (event.get_key() == resatr::KeyboardButtonPress::Keys::R) {
            if (!get_root().get_context().is_mouse_captured()) {
                get_root().capture_mouse();
            }
            else {
                get_root().release_mouse();
            }
            return true;
        }
        return false;
    }

    virtual bool on_mouse_button_press(const resatr::MouseButtonPress& event) override {
        logger.logi("Mouse press: ", event);
        return false;
    }

    virtual bool on_mouse_movement(const resatr::MouseMovement& event) override {
        logger.logi("Mouse move: ", event);
        return false;
    }

    virtual bool on_mouse_relative_movement(const resatr::MouseMovement& event) override {
        logger.logi("Mouse relative movement: ", event);
        return false;
    }

    virtual bool on_text_input(const char* str) override {
        logger.logi("Input text: ", str);
        return false;
    }
};

int main() {
    pthread_setname_np(pthread_self(), "triangle");
    logger.configure(true, spec::Logger::DEBUG, "triangle.log", spec::Logger::ALL);
    // Initialization.
    logger.logi("Re:Satr in startup...");
    glfw::GLFW& glfw = glfw::GLFW::get_instance();
    logger.logi("Loaded GLFW, compiled version: ", glfw.get_compiled_version(),
                ", runtime version: ", glfw.get_runtime_version());
    // Window.
    glfw::WindowHints window_hints;
    window_hints.set_maximized(true);
    window_hints.set_srgb_capable(true);
    window_hints.set_opengl_profile(glfw::WindowHints::OpenGLCoreProfile);
    window_hints.set_opengl_forward_compat(true);
    window_hints.set_context_version_major(3);
    window_hints.set_context_version_minor(3);
    window_hints.set_red_bits(8);
    window_hints.set_green_bits(8);
    window_hints.set_blue_bits(8);
    window_hints.set_alpha_bits(0);
    window_hints.set_depth_bits(0);
    window_hints.set_stencil_bits(0);
    glfw::Window window("Re:Satr Triangle", window_hints);
    // Engine setup.
    resatr::opengl::OffLoadScheduler loader;
    resatr::opengl::NoopOpenGLPresenter presenter(window, loader);
    BasicOpenGLRenderLoop renderer(window, loader, presenter);
    renderer.set_clear_color(glm::vec3(0.8, 0.5, 0.5));
    Triangle main_scene(renderer, renderer, renderer.get_size());
    renderer.set_3d_scene(&main_scene);
    TriangleUI ui(renderer, renderer, renderer.get_size());
    renderer.set_ui(&ui);
    // Main Loop
    renderer.loop();
    // Cleanup.
    spec::trace("main::exit");
    return EXIT_SUCCESS;
}
