#include <LoadScheduler.hpp>
#include <Logger.hpp>
#include <Scene.hpp>
#include <glfw.hpp>
#include <glm/gtc/matrix_access.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/mat4x3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <opengl/buffer/Buffer.hpp>
#include <opengl/buffer/VAO.hpp>
#include <opengl/core/Types.hpp>
#include <opengl/program/Program.hpp>
#include <opengl/renderer/CompositeRenderLoops.hpp>
#include <opengl/renderer/Presenter.hpp>
#include <opengl/renderer/RenderLoop.hpp>
#include <opengl/texture/Scene.hpp>
#include <opengl/utils/DrawCommands.hpp>

#include <cmath>

#include <glad.h>
#include <pthread.h>

static const char* SCALED_OFFSETTED_VERTEX_SRC =
     "#version 330\n"
     "uniform vec2 offset = vec2(0.0);\n"
     "uniform vec2 scale = vec2(1.0);\n"
     "\n"
     "in vec2 vertex;\n"
     "\n"
     "void main() {\n"
     "  gl_Position = vec4(vertex * scale + offset, 0.0f, 1.0f);\n"
     "}\n";

static const char* CUBIC_CURVE_VERTEX_SRC = "#version 330\n"
                                            "uniform vec3 c0;\n"
                                            "uniform vec3 c1;\n"
                                            "uniform vec3 c2;\n"
                                            "uniform vec3 c3;\n"
                                            "\n"
                                            "in float u;\n"
                                            "\n"
                                            "void main() {\n"
                                            "  float u2 = u * u;\n"
                                            "  float u3 = u2 * u;\n"
                                            "  vec3 p = c0;\n"
                                            "  p += c1 * u;\n"
                                            "  p += c2 * u2;\n"
                                            "  p += c3 * u3;\n"
                                            "  gl_Position = vec4(p, 1.0f);\n"
                                            "}\n";

/**
 * Inspired by https://developer.roblox.com/en-us/articles/Bezier-curves
 */
static const char* BEZIER6_LERP_CURVE_VERTEX_SRC =
     "#version 330\n"
     "uniform vec2 p0;\n"
     "uniform vec2 p1;\n"
     "uniform vec2 p2;\n"
     "uniform vec2 p3;\n"
     "uniform vec2 p4;\n"
     "uniform vec2 p5;\n"
     "uniform vec2 p6;\n"
     "\n"
     "in float u;\n"
     "\n"
     "vec2 resatr_lerp(in vec2 a, in vec2 b, in float f) {\n"
     "  return a + (b - a) * f;"
     "}\n"
     "\n"
     "void main() {\n"
     "  vec2 l00 = resatr_lerp(p0, p1, u);\n"
     "  vec2 l01 = resatr_lerp(p1, p2, u);\n"
     "  vec2 l02 = resatr_lerp(p2, p3, u);\n"
     "  vec2 l03 = resatr_lerp(p3, p4, u);\n"
     "  vec2 l04 = resatr_lerp(p4, p5, u);\n"
     "  vec2 l05 = resatr_lerp(p5, p6, u);\n"
     "\n"
     "  vec2 l10 = resatr_lerp(l00, l01, u);\n"
     "  vec2 l11 = resatr_lerp(l01, l02, u);\n"
     "  vec2 l12 = resatr_lerp(l02, l03, u);\n"
     "  vec2 l13 = resatr_lerp(l03, l04, u);\n"
     "  vec2 l14 = resatr_lerp(l04, l05, u);\n"
     "\n"
     "  vec2 l20 = resatr_lerp(l10, l11, u);\n"
     "  vec2 l21 = resatr_lerp(l11, l12, u);\n"
     "  vec2 l22 = resatr_lerp(l12, l13, u);\n"
     "  vec2 l23 = resatr_lerp(l13, l14, u);\n"
     "\n"
     "  vec2 l30 = resatr_lerp(l20, l21, u);\n"
     "  vec2 l31 = resatr_lerp(l21, l22, u);\n"
     "  vec2 l32 = resatr_lerp(l22, l23, u);\n"
     "\n"
     "  vec2 l40 = resatr_lerp(l30, l31, u);\n"
     "  vec2 l41 = resatr_lerp(l31, l32, u);\n"
     "\n"
     "  vec2 b = resatr_lerp(l40, l41, u);\n"
     "  gl_Position = vec4(b, 0.0f, 1.0f);\n"
     "}\n";

static const char* BEZIER3_LERP_CURVE_VERTEX_SRC =
     "#version 330\n"
     "uniform vec2 p0;\n"
     "uniform vec2 p1;\n"
     "uniform vec2 p2;\n"
     "uniform vec2 p3;\n"
     "\n"
     "in float u;\n"
     "\n"
     "void main() {\n"
     "  vec2 b0 = pow(1 - u, 3.0f) * p0;\n"
     "  vec2 b1 = 3 * pow(1 - u, 2.0f) * u * p1;\n"
     "  vec2 b2 = 3 * (1 - u) * pow(u, 2.0f) * p2;\n"
     "  vec2 b3 = pow(u, 3.0f) * p3;\n"
     "  vec2 b = b0 + b1 + b2 + b3;\n"
     "  gl_Position = vec4(b, 0.0f, 1.0f);\n"
     "}\n";

static const char* SOLID_COLOR_FRAGMENT_SRC = "#version 330\n"
                                              "out vec4 output_color;\n"
                                              "uniform vec3 color;\n"
                                              "void main() {\n"
                                              "  output_color = vec4(color, 1.0f);\n"
                                              "}\n";

float distance(glm::vec2 a, glm::vec2 b) {
    glm::vec2 ab = b - a;
    ab *= ab;
    float accum = ab.x + ab.y;
    return std::sqrt(accum);
}

class DualCubicScene final : public resatr::opengl::OpenGLScene {
   protected:
    static constexpr float CONTROL_POINT_SCALE = 0.003;
    static constexpr int CURVE_POINT_COUNT = 1024;
    /* OpenGL resources */
    resatr::opengl::Program _control_point_program = resatr::opengl::Program::create_invalid();
    resatr::opengl::Program::Uniform _control_point_program_offset;
    resatr::opengl::Program::Uniform _control_point_program_size;
    resatr::opengl::VertexAttributeObject _control_point_bindings =
         resatr::opengl::VertexAttributeObject::create_invalid();

    resatr::opengl::Program _curve_program = resatr::opengl::Program::create_invalid();
    resatr::opengl::Program::Uniform _curve_program_c0;
    resatr::opengl::Program::Uniform _curve_program_c1;
    resatr::opengl::Program::Uniform _curve_program_c2;
    resatr::opengl::Program::Uniform _curve_program_c3;
    resatr::opengl::Program::Uniform _curve_program_color;
    resatr::opengl::Buffer _curve_points = resatr::opengl::Buffer::create_invalid();
    resatr::opengl::VertexAttributeObject _curve_bindings =
         resatr::opengl::VertexAttributeObject::create_invalid();

    /* Generic state */
    bool _dirty = true;
    glm::vec2 _last_mouse_pos = glm::vec2(0.0);
    int _dragging_index = -1;

    /* Curve state */
    glm::vec2 _control_points[7] = {{0.9, 0.0},  {0.6, 0.0},  {0.3, 0.0}, {0.0, 0.0},
                                    {-0.3, 0.0}, {-0.6, 0.0}, {-0.9, 0.0}};

   public:
    DualCubicScene(resatr::opengl::OpenGLRenderLoop& root) :
        OpenGLScene(root, root, root.get_size(), true, true, false, true) {
        set_clear_color(glm::vec3(0.1, 0.1, 0.1));
        set_clear_depth(1.0);
        logger.logi("Loading Dual cubic curves!");
        auto cpv = root.create_vertex_shader(SCALED_OFFSETTED_VERTEX_SRC);
        auto scf = root.create_fragment_shader(SOLID_COLOR_FRAGMENT_SRC);
        _control_point_program = root.create_program(cpv, scf);
        _control_point_program_offset = _control_point_program.get_uniforms()["offset"];
        _control_point_program_size = _control_point_program.get_uniforms()["scale"];
        float aspect = get_size().x / (float) get_size().y;
        _control_point_program_size =
             glm::vec2(CONTROL_POINT_SCALE, CONTROL_POINT_SCALE * aspect);
        _control_point_program.get_uniforms()["color"] = glm::vec3(0.33, 0.86, 0.14);
        auto _control_point_program_vertex = _control_point_program.get_attributes()["vertex"];
        _control_point_bindings = root.create_vertex_attributes();
        _control_point_bindings.bind(_control_point_program_vertex, root.get_2d_quad_buffer(),
                                     2, resatr::opengl::Types::Float);

        float _precalc_curve_points[CURVE_POINT_COUNT];
        for (int i = 0; i < CURVE_POINT_COUNT; i++) {
            _precalc_curve_points[i] = i / (float) (CURVE_POINT_COUNT - 1);
        }
        _curve_points = root.create_array_draw_buffer(_precalc_curve_points);

        auto cv = root.create_vertex_shader(CUBIC_CURVE_VERTEX_SRC);
        _curve_program = root.create_program(cv, scf);
        auto curve_program_uniforms = _curve_program.get_uniforms();
        _curve_program_c0 = curve_program_uniforms["c0"];
        _curve_program_c1 = curve_program_uniforms["c1"];
        _curve_program_c2 = curve_program_uniforms["c2"];
        _curve_program_c3 = curve_program_uniforms["c3"];
        _curve_program_color = curve_program_uniforms["color"];

        _curve_bindings = root.create_vertex_attributes();
        auto curve_program_u = _curve_program.get_attributes()["u"];
        _curve_bindings.bind(curve_program_u, _curve_points, 1, resatr::opengl::Types::Float);
    }

    void upload_curve_matrix(int curve) {
        glm::mat4x3 control_point_matrix(glm::vec3(_control_points[curve * 3], 0.0),
                                         glm::vec3(_control_points[curve * 3 + 1], 0.0),
                                         glm::vec3(_control_points[curve * 3 + 2], 0.0),
                                         glm::vec3(_control_points[curve * 3 + 3], 0.0));
        auto C = calculate_curve_matrix(control_point_matrix);
        _curve_program_c0 = C[0];
        _curve_program_c1 = C[1];
        _curve_program_c2 = C[2];
        _curve_program_c3 = C[3];
    }

    glm::mat4x3 calculate_curve_matrix(const glm::mat4x3& P) {
        static const glm::mat4x4 M = glm::mat4x4(1, 0, 0, 0, -5.5, 9, -4.5, 1, 9, -22.5, 18,
                                                 -4.5, -4.5, 13.5, -13.5, 4.5);
        glm::mat4x3 C = glm::mat4x3(0);
        for (int r = 0; r < 4; r++) {
            auto Mr = M[r];
            for (int c = 0; c < 3; c++) {
                // Actually column, but OpenGL.
                auto Pc = glm::row(P, c);
                C[r][c] = glm::dot(Mr, Pc);
            }
        }
        return C;
    }

    virtual void render() override {
        if (!_dirty) {
            return;
        }
        _dirty = false;
        clear();
        // Render two curves.
        _curve_bindings.bind();
        _curve_program.bind();
        // First curve.
        upload_curve_matrix(0);
        _curve_program_color = glm::vec3(0.8, 0.1, 0.23);
        resatr::opengl::draw_arrays(resatr::opengl::VertexFormats::LineStrip, 0,
                                    CURVE_POINT_COUNT);
        upload_curve_matrix(1);
        _curve_program_color = glm::vec3(0.24, 0.1, 0.86);
        resatr::opengl::draw_arrays(resatr::opengl::VertexFormats::LineStrip, 0,
                                    CURVE_POINT_COUNT);
        _curve_program.unbind();
        _curve_bindings.unbind();
        // Render all control points(on top).
        _control_point_bindings.bind();
        _control_point_program.bind();
        for (int i = 0; i < 7; i++) {
            _control_point_program_offset = _control_points[i];
            resatr::opengl::draw_arrays(resatr::opengl::VertexFormats::TriangleStrip, 0, 4);
        }
        _control_point_program.unbind();
        _control_point_bindings.unbind();
    }

    virtual bool on_mouse_button_press(const resatr::MouseButtonPress& e) override {
        if (e.get_key() == resatr::MouseButtonPress::Keys::Left) {
            if (e.is_pressed()) {
                for (int i = 0; i < 7; i++) {
                    float d = distance(_last_mouse_pos, _control_points[i]);
                    if (d <= CONTROL_POINT_SCALE * 2) {
                        logger.logi("Selected control point: ", i);
                        _dragging_index = i;
                        _dirty = true;
                        return true;
                    }
                }
            }
            else if (_dragging_index != -1) {
                _dragging_index = -1;
                _dirty = true;
            }
        }
        return false;
    }

    virtual bool on_mouse_movement(const resatr::MouseMovement& e) override {
        _last_mouse_pos = e.normalized(get_root().get_context().get_window_size(),
                                       get_root().get_context().get_framebuffer_size());
        if (_dragging_index != -1) {
            _control_points[_dragging_index] = _last_mouse_pos;
            _dirty = true;
        }
        return true;
    }

    virtual void set_size(glm::ivec2 size) override {
        resatr::opengl::OpenGLScene::set_size(size);
        float aspect = size.x / (float) size.y;
        _control_point_program_size =
             glm::vec2(CONTROL_POINT_SCALE, CONTROL_POINT_SCALE * aspect);
        _dirty = true;
    }
};

class Bezier6Scene final : public resatr::opengl::OpenGLScene {
   protected:
    static constexpr float CONTROL_POINT_SCALE = 0.003;
    static constexpr int CURVE_POINT_COUNT = 1024 * 8;
    /* OpenGL resources */
    resatr::opengl::Program _control_point_program = resatr::opengl::Program::create_invalid();
    resatr::opengl::Program::Uniform _control_point_program_offset;
    resatr::opengl::Program::Uniform _control_point_program_size;
    resatr::opengl::VertexAttributeObject _control_point_bindings =
         resatr::opengl::VertexAttributeObject::create_invalid();

    resatr::opengl::Program _curve_program = resatr::opengl::Program::create_invalid();
    resatr::opengl::Program::Uniform _curve_program_p0;
    resatr::opengl::Program::Uniform _curve_program_p1;
    resatr::opengl::Program::Uniform _curve_program_p2;
    resatr::opengl::Program::Uniform _curve_program_p3;
    resatr::opengl::Program::Uniform _curve_program_p4;
    resatr::opengl::Program::Uniform _curve_program_p5;
    resatr::opengl::Program::Uniform _curve_program_p6;
    resatr::opengl::Buffer _curve_points = resatr::opengl::Buffer::create_invalid();
    resatr::opengl::VertexAttributeObject _curve_bindings =
         resatr::opengl::VertexAttributeObject::create_invalid();

    /* Generic state */
    bool _dirty = true;
    glm::vec2 _last_mouse_pos = glm::vec2(0.0);
    int _dragging_index = -1;

    /* Curve state */
    glm::vec2 _control_points[6] = {{0.0, 0.7},  {0.7, 0.5},   {0.7, 0.0},
                                    {0.0, -0.7}, {-0.7, -0.5}, {-0.7, 0.0}};

   public:
    Bezier6Scene(resatr::opengl::OpenGLRenderLoop& root) :
        OpenGLScene(root, root, root.get_size(), true, true, false, true) {
        set_clear_color(glm::vec3(0.1, 0.1, 0.1));
        set_clear_depth(1.0);
        logger.logi("Loading 6th degree Bezier closed curve!");
        auto cpv = root.create_vertex_shader(SCALED_OFFSETTED_VERTEX_SRC);
        auto scf = root.create_fragment_shader(SOLID_COLOR_FRAGMENT_SRC);
        _control_point_program = root.create_program(cpv, scf);
        _control_point_program_offset = _control_point_program.get_uniforms()["offset"];
        _control_point_program_size = _control_point_program.get_uniforms()["scale"];
        float aspect = get_size().x / (float) get_size().y;
        _control_point_program_size =
             glm::vec2(CONTROL_POINT_SCALE, CONTROL_POINT_SCALE * aspect);
        _control_point_program.get_uniforms()["color"] = glm::vec3(0.33, 0.86, 0.14);
        auto _control_point_program_vertex = _control_point_program.get_attributes()["vertex"];
        _control_point_bindings = root.create_vertex_attributes();
        _control_point_bindings.bind(_control_point_program_vertex, root.get_2d_quad_buffer(),
                                     2, resatr::opengl::Types::Float);

        float _precalc_curve_points[CURVE_POINT_COUNT];
        for (int i = 0; i < CURVE_POINT_COUNT; i++) {
            _precalc_curve_points[i] = i / (float) (CURVE_POINT_COUNT - 1);
        }
        _curve_points = root.create_array_draw_buffer(_precalc_curve_points);

        auto cv = root.create_vertex_shader(BEZIER6_LERP_CURVE_VERTEX_SRC);
        _curve_program = root.create_program(cv, scf);
        auto curve_program_uniforms = _curve_program.get_uniforms();
        _curve_program_p0 = curve_program_uniforms["p0"];
        _curve_program_p1 = curve_program_uniforms["p1"];
        _curve_program_p2 = curve_program_uniforms["p2"];
        _curve_program_p3 = curve_program_uniforms["p3"];
        _curve_program_p4 = curve_program_uniforms["p4"];
        _curve_program_p5 = curve_program_uniforms["p5"];
        _curve_program_p6 = curve_program_uniforms["p6"];
        curve_program_uniforms["color"] = glm::vec3(0.86, 0.22, 0.34);

        _curve_bindings = root.create_vertex_attributes();
        auto curve_program_u = _curve_program.get_attributes()["u"];
        _curve_bindings.bind(curve_program_u, _curve_points, 1, resatr::opengl::Types::Float);
    }

    void upload_curve_points() {
        _curve_program_p0 = _control_points[0];
        _curve_program_p1 = _control_points[1];
        _curve_program_p2 = _control_points[2];
        _curve_program_p3 = _control_points[3];
        _curve_program_p4 = _control_points[4];
        _curve_program_p5 = _control_points[5];
        _curve_program_p6 = _control_points[0];
    }

    virtual void render() override {
        if (!_dirty) {
            return;
        }
        _dirty = false;
        clear();
        // Render two curves.
        _curve_bindings.bind();
        _curve_program.bind();
        // First curve.
        upload_curve_points();
        resatr::opengl::draw_arrays(resatr::opengl::VertexFormats::LineStrip, 0,
                                    CURVE_POINT_COUNT);
        _curve_program.unbind();
        _curve_bindings.unbind();
        // Render all control points(on top).
        _control_point_bindings.bind();
        _control_point_program.bind();
        for (int i = 0; i < 6; i++) {
            _control_point_program_offset = _control_points[i];
            resatr::opengl::draw_arrays(resatr::opengl::VertexFormats::TriangleStrip, 0, 4);
        }
        _control_point_program.unbind();
        _control_point_bindings.unbind();
    }

    virtual bool on_mouse_button_press(const resatr::MouseButtonPress& e) override {
        if (e.get_key() == resatr::MouseButtonPress::Keys::Left) {
            if (e.is_pressed()) {
                for (int i = 0; i < 6; i++) {
                    float d = distance(_last_mouse_pos, _control_points[i]);
                    if (d <= CONTROL_POINT_SCALE * 2) {
                        logger.logi("Selected control point: ", i);
                        _dragging_index = i;
                        _dirty = true;
                        return true;
                    }
                }
            }
            else if (_dragging_index != -1) {
                _dragging_index = -1;
                _dirty = true;
            }
        }
        return false;
    }

    virtual bool on_mouse_movement(const resatr::MouseMovement& e) override {
        _last_mouse_pos = e.normalized(get_root().get_context().get_window_size(),
                                       get_root().get_context().get_framebuffer_size());
        if (_dragging_index != -1) {
            _control_points[_dragging_index] = _last_mouse_pos;
            _dirty = true;
        }
        return true;
    }

    virtual void set_size(glm::ivec2 size) override {
        resatr::opengl::OpenGLScene::set_size(size);
        float aspect = size.x / (float) size.y;
        _control_point_program_size =
             glm::vec2(CONTROL_POINT_SCALE, CONTROL_POINT_SCALE * aspect);
        _dirty = true;
    }
};

class DualCubicBezierScene final : public resatr::opengl::OpenGLScene {
   protected:
    static constexpr float CONTROL_POINT_SCALE = 0.003;
    static constexpr int CURVE_POINT_COUNT = 2048;
    /* OpenGL resources */
    resatr::opengl::Program _control_point_program = resatr::opengl::Program::create_invalid();
    resatr::opengl::Program::Uniform _control_point_program_offset;
    resatr::opengl::Program::Uniform _control_point_program_size;
    resatr::opengl::VertexAttributeObject _control_point_bindings =
         resatr::opengl::VertexAttributeObject::create_invalid();

    resatr::opengl::Program _curve_program = resatr::opengl::Program::create_invalid();
    resatr::opengl::Program::Uniform _curve_program_p0;
    resatr::opengl::Program::Uniform _curve_program_p1;
    resatr::opengl::Program::Uniform _curve_program_p2;
    resatr::opengl::Program::Uniform _curve_program_p3;
    resatr::opengl::Program::Uniform _curve_program_color;
    resatr::opengl::Buffer _curve_points = resatr::opengl::Buffer::create_invalid();
    resatr::opengl::VertexAttributeObject _curve_bindings =
         resatr::opengl::VertexAttributeObject::create_invalid();

    /* Generic state */
    bool _dirty = true;
    glm::vec2 _last_mouse_pos = glm::vec2(0.0);
    int _dragging_index = -1;

    /* Curve state */
    glm::vec2 _control_points[7] = {{0.9, 0.0},  {0.6, 0.0},  {0.3, 0.0}, {0.0, 0.0},
                                    {-0.3, 0.0}, {-0.6, 0.0}, {-0.9, 0.0}};

   public:
    DualCubicBezierScene(resatr::opengl::OpenGLRenderLoop& root) :
        OpenGLScene(root, root, root.get_size(), true, true, false, true) {
        set_clear_color(glm::vec3(0.1, 0.1, 0.1));
        set_clear_depth(1.0);
        logger.logi("Loading Dual cubic bezier (not splines)!");
        auto cpv = root.create_vertex_shader(SCALED_OFFSETTED_VERTEX_SRC);
        auto scf = root.create_fragment_shader(SOLID_COLOR_FRAGMENT_SRC);
        _control_point_program = root.create_program(cpv, scf);
        _control_point_program_offset = _control_point_program.get_uniforms()["offset"];
        _control_point_program_size = _control_point_program.get_uniforms()["scale"];
        float aspect = get_size().x / (float) get_size().y;
        _control_point_program_size =
             glm::vec2(CONTROL_POINT_SCALE, CONTROL_POINT_SCALE * aspect);
        _control_point_program.get_uniforms()["color"] = glm::vec3(0.33, 0.86, 0.14);
        auto _control_point_program_vertex = _control_point_program.get_attributes()["vertex"];
        _control_point_bindings = root.create_vertex_attributes();
        _control_point_bindings.bind(_control_point_program_vertex, root.get_2d_quad_buffer(),
                                     2, resatr::opengl::Types::Float);

        float _precalc_curve_points[CURVE_POINT_COUNT];
        for (int i = 0; i < CURVE_POINT_COUNT; i++) {
            _precalc_curve_points[i] = i / (float) (CURVE_POINT_COUNT - 1);
        }
        _curve_points = root.create_array_draw_buffer(_precalc_curve_points);

        auto cv = root.create_vertex_shader(BEZIER3_LERP_CURVE_VERTEX_SRC);
        _curve_program = root.create_program(cv, scf);
        auto curve_program_uniforms = _curve_program.get_uniforms();
        _curve_program_p0 = curve_program_uniforms["p0"];
        _curve_program_p1 = curve_program_uniforms["p1"];
        _curve_program_p2 = curve_program_uniforms["p2"];
        _curve_program_p3 = curve_program_uniforms["p3"];
        _curve_program_color = curve_program_uniforms["color"];

        _curve_bindings = root.create_vertex_attributes();
        auto curve_program_u = _curve_program.get_attributes()["u"];
        _curve_bindings.bind(curve_program_u, _curve_points, 1, resatr::opengl::Types::Float);
    }

    void upload_curve_points(int curve) {
        _curve_program_p0 = _control_points[curve * 3 + 0];
        _curve_program_p1 = _control_points[curve * 3 + 1];
        _curve_program_p2 = _control_points[curve * 3 + 2];
        _curve_program_p3 = _control_points[curve * 3 + 3];
    }

    virtual void render() override {
        if (!_dirty) {
            return;
        }
        _dirty = false;
        clear();
        // Render two curves.
        _curve_bindings.bind();
        _curve_program.bind();
        // First curve.
        upload_curve_points(0);
        _curve_program_color = glm::vec3(0.86, 0.08, 0.18);
        resatr::opengl::draw_arrays(resatr::opengl::VertexFormats::LineStrip, 0,
                                    CURVE_POINT_COUNT);
        upload_curve_points(1);
        _curve_program_color = glm::vec3(0.14, 0.04, 0.86);
        resatr::opengl::draw_arrays(resatr::opengl::VertexFormats::LineStrip, 0,
                                    CURVE_POINT_COUNT);
        _curve_program.unbind();
        _curve_bindings.unbind();
        // Render all control points(on top).
        _control_point_bindings.bind();
        _control_point_program.bind();
        for (int i = 0; i < 7; i++) {
            _control_point_program_offset = _control_points[i];
            resatr::opengl::draw_arrays(resatr::opengl::VertexFormats::TriangleStrip, 0, 4);
        }
        _control_point_program.unbind();
        _control_point_bindings.unbind();
    }

    virtual bool on_mouse_button_press(const resatr::MouseButtonPress& e) override {
        if (e.get_key() == resatr::MouseButtonPress::Keys::Left) {
            if (e.is_pressed()) {
                for (int i = 0; i < 7; i++) {
                    float d = distance(_last_mouse_pos, _control_points[i]);
                    if (d <= CONTROL_POINT_SCALE * 2) {
                        logger.logi("Selected control point: ", i);
                        _dragging_index = i;
                        _dirty = true;
                        return true;
                    }
                }
            }
            else if (_dragging_index != -1) {
                _dragging_index = -1;
                _dirty = true;
            }
        }
        return false;
    }

    virtual bool on_mouse_movement(const resatr::MouseMovement& e) override {
        _last_mouse_pos = e.normalized(get_root().get_context().get_window_size(),
                                       get_root().get_context().get_framebuffer_size());
        if (_dragging_index != -1) {
            auto diff = _last_mouse_pos - _control_points[_dragging_index];
            _control_points[_dragging_index] = _last_mouse_pos;
            // Ensure geometric properties for C1 are retained.
            if (_dragging_index == 3) {
                // When transiting point moves,
                // move adjacent control points,
                // to retain geometric properties.
                _control_points[2] += diff;
                _control_points[4] += diff;
            }
            // Ensure collinearity
            if (_dragging_index == 2) {
                _control_points[4] = glm::vec2(2.0f) * _control_points[3] - _control_points[2];
            }
            if (_dragging_index == 4) {
                _control_points[2] = glm::vec2(2.0f) * _control_points[3] - _control_points[4];
            }
            _dirty = true;
        }
        return true;
    }

    virtual void set_size(glm::ivec2 size) override {
        resatr::opengl::OpenGLScene::set_size(size);
        float aspect = size.x / (float) size.y;
        _control_point_program_size =
             glm::vec2(CONTROL_POINT_SCALE, CONTROL_POINT_SCALE * aspect);
        _dirty = true;
    }
};

class UI final : public resatr::opengl::NuklearOpenGLScene {
   protected:
    enum Demos { Null, DualCubic, Bezier6, DualCubicBezier };
    Demos current_option = Demos::Null;
    Demos last_option = current_option;

   public:
    UI(resatr::opengl::BasicOpenGLRenderLoop& root) :
        NuklearOpenGLScene(root, root, root.get_size()) {}

    virtual void render(nk_context* ctx) override {
        if (nk_begin(ctx, "Project part chooser", nk_rect(35, 35, 260, 220),
                     NK_WINDOW_BORDER | NK_WINDOW_MOVABLE | NK_WINDOW_SCALABLE |
                          NK_WINDOW_MINIMIZABLE | NK_WINDOW_TITLE)) {
            nk_layout_row_dynamic(ctx, 25, 1);
            current_option = nk_option_label(ctx, "Null", current_option == Demos::Null) ?
                                  Demos::Null :
                                  current_option;
            current_option = nk_option_label(ctx, "Dual Cubic Parametric Curve",
                                             current_option == Demos::DualCubic) ?
                                  Demos::DualCubic :
                                  current_option;
            current_option = nk_option_label(ctx, "6th Degree Bezier Closed Curve",
                                             current_option == Demos::Bezier6) ?
                                  Demos::Bezier6 :
                                  current_option;
            current_option = nk_option_label(ctx, "Dual Cubic Bezier Curve",
                                             current_option == Demos::DualCubicBezier) ?
                                  Demos::DualCubicBezier :
                                  current_option;
        }
        nk_end(ctx);
    }

    virtual void draw() override {
        resatr::opengl::NuklearOpenGLScene::draw();
        // Switch to new 3D scene, while the last frame is finishing rendering.
        if (current_option != last_option) {
            auto& root = dynamic_cast<resatr::opengl::BasicOpenGLRenderLoop&>(get_root());
            last_option = current_option;
            logger.logi("Switching scene...");
            resatr::opengl::OpenGLScene* old_scene = nullptr;
            switch (current_option) {
                case Demos::Null: {
                    old_scene = root.set_3d_scene(nullptr);
                } break;
                case Demos::DualCubic: {
                    DualCubicScene* new_scene = new DualCubicScene(root);
                    old_scene = root.set_3d_scene(new_scene);
                } break;
                case Demos::Bezier6: {
                    Bezier6Scene* new_scene = new Bezier6Scene(root);
                    old_scene = root.set_3d_scene(new_scene);
                } break;
                case Demos::DualCubicBezier: {
                    DualCubicBezierScene* new_scene = new DualCubicBezierScene(root);
                    old_scene = root.set_3d_scene(new_scene);
                } break;
            }
            if (old_scene != nullptr) {
                delete old_scene;
            }
        }
    }

    ~UI() {
        auto& root = dynamic_cast<resatr::opengl::BasicOpenGLRenderLoop&>(get_root());
        resatr::opengl::OpenGLScene* scene = root.get_3d_scene();
        if (scene != nullptr) {
            delete scene;
        }
    }
};

int main() {
    pthread_setname_np(pthread_self(), "parametric");
    logger.configure(true, spec::Logger::DEBUG, "parametric.log", spec::Logger::ALL);
    // Initialization.
    logger.logi("Re:Satr in startup...");
    glfw::GLFW& glfw = glfw::GLFW::get_instance();
    logger.logi("Loaded GLFW, compiled version: ", glfw.get_compiled_version(),
                ", runtime version: ", glfw.get_runtime_version());
    // Window.
    glfw::WindowHints window_hints;
    window_hints.set_maximized(true);
    window_hints.set_srgb_capable(true);
    window_hints.set_opengl_profile(glfw::WindowHints::OpenGLCoreProfile);
    window_hints.set_opengl_forward_compat(true);
    window_hints.set_context_version_major(3);
    window_hints.set_context_version_minor(3);
    window_hints.set_red_bits(8);
    window_hints.set_green_bits(8);
    window_hints.set_blue_bits(8);
    window_hints.set_alpha_bits(0);
    window_hints.set_depth_bits(0);
    window_hints.set_stencil_bits(0);
    glfw::Window window("Re:Satr Parametric Curves Project", window_hints);
    // Engine setup.
    resatr::NullLoadScheduler loader;
    resatr::opengl::NoopOpenGLPresenter presenter(window, loader);
    resatr::opengl::BasicOpenGLRenderLoop renderer(window, loader, presenter);
    renderer.set_clear_color(glm::vec3(0.8, 0.5, 0.5));
    UI ui(renderer);
    renderer.set_ui(&ui);
    // Main Loop
    renderer.loop();
    return EXIT_SUCCESS;
}
