#include <LoadScheduler.hpp>
#include <Logger.hpp>
#include <Scene.hpp>
#include <glfw.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/mat4x4.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <opengl/buffer/Buffer.hpp>
#include <opengl/buffer/VAO.hpp>
#include <opengl/core/Types.hpp>
#include <opengl/program/Program.hpp>
#include <opengl/renderer/CompositeRenderLoops.hpp>
#include <opengl/renderer/Presenter.hpp>
#include <opengl/renderer/RenderLoop.hpp>
#include <opengl/texture/Scene.hpp>
#include <opengl/utils/DrawCommands.hpp>

#include <glad.h>
#include <pthread.h>

static const glm::vec3 CUBE_STRIP[] = {{-1.f, 1.f, 1.f},  {1.f, 1.f, 1.f},    {-1.f, -1.f, 1.f},
                                       {1.f, -1.f, 1.f},  {1.f, -1.f, -1.f},  {1.f, 1.f, 1.f},
                                       {1.f, 1.f, -1.f},  {-1.f, 1.f, 1.f},   {-1.f, 1.f, -1.f},
                                       {-1.f, -1.f, 1.f}, {-1.f, -1.f, -1.f}, {1.f, -1.f, -1.f},
                                       {-1.f, 1.f, -1.f}, {1.f, 1.f, -1.f}};

static const char* PERSPECTIVE_VERTEX_SRC = "#version 330\n"
                                            "uniform mat4 proj = mat4(1.0);\n"
                                            "\n"
                                            "in vec3 vertex;\n"
                                            "\n"
                                            "void main() {\n"
                                            "  gl_Position = proj * vec4(vertex, 1.0f);\n"
                                            "}\n";

static const char* PERSPECTIVE_FRAGMENT_SRC = "#version 330\n"
                                              "out vec4 output;\n"
                                              "uniform vec3 color;\n"
                                              "void main() {\n"
                                              "  output = vec4(color, 1.0f);\n"
                                              "}\n";

class PerspectiveBoxScene final : public resatr::opengl::OpenGLScene {
   protected:
    resatr::opengl::RenderSpec _render_conf;
    resatr::opengl::Program _perspective = resatr::opengl::Program::create_invalid();
    resatr::opengl::Program::Uniform _perspective_proj;
    resatr::opengl::Buffer _cube = resatr::opengl::Buffer::create_invalid();
    resatr::opengl::VertexAttributeObject _cube_vao =
         resatr::opengl::VertexAttributeObject::create_invalid();

    glm::vec3 _camera_pos = glm::vec3(0.0, 0.0, -2.0);

    bool _dirty = true;
    bool _shift_mode = false;

   public:
    PerspectiveBoxScene(resatr::opengl::OpenGLRenderLoop& root) :
        OpenGLScene(root, root, root.get_size(), true, true, false, true), _render_conf() {
        set_clear_color(glm::vec3(0.1, 0.1, 0.1));
        set_clear_depth(1.0);
        logger.logi("Loading PerspectiveBox!");
        // Load shaders.
        auto vert = root.create_vertex_shader(PERSPECTIVE_VERTEX_SRC);
        auto frag = root.create_fragment_shader(PERSPECTIVE_FRAGMENT_SRC);
        _perspective = root.create_program(vert, frag);
        // Upload model.
        _cube = root.create_array_draw_buffer(CUBE_STRIP);
        _cube_vao = root.create_vertex_attributes();
        // Setup variables.
        auto _perspective_attr0 = _perspective.get_attributes()["vertex"];
        _cube_vao.bind(_perspective_attr0, _cube, 3, resatr::opengl::Types::Float);
        _perspective_proj = _perspective.get_uniforms()["proj"];
        _perspective.get_uniforms()["color"] = glm::vec3(0.95, 0.95, 0.95);
        _render_conf.set_culling(resatr::opengl::RenderSpec::CullFace::Front);
        _render_conf.set_depth_test();
    }

    virtual void render() override {
        if (!_dirty) {
            // Nothing has changed, so no need to render.
            return;
        }
        _dirty = false;
        clear();
        _perspective_proj = calculate_perspective_matrix();
        _render_conf.bind();
        _cube_vao.bind();
        _perspective.bind();
        resatr::opengl::draw_arrays(resatr::opengl::VertexFormats::TriangleStrip, 0, 14);
        _perspective.unbind();
        _cube_vao.unbind();
        _render_conf.unbind();
    }

    glm::mat4 calculate_perspective_matrix() {
        glm::dvec2 fbsize = get_size();
        float aspect = fbsize.x / fbsize.y;
        glm::mat4 proj = glm::perspective(50.0f, aspect, 0.1f, 4.0f);
        glm::mat4 camera = glm::translate(glm::mat4(1.0), _camera_pos);
        return proj * camera;
    }

    virtual void set_size(glm::ivec2 size) override {
        resatr::opengl::OpenGLScene::set_size(size);
        _dirty = true;
    }

    virtual bool on_keyboard_button_press(const resatr::KeyboardButtonPress& e) override {
        if (e.get_key() == resatr::KeyboardButtonPress::Keys::LeftShift ||
            e.get_key() == resatr::KeyboardButtonPress::Keys::RightShift) {
            _shift_mode = e.is_pressed();
            _dirty = true;
            return true;
        }
        return false;
    }

    virtual bool on_mouse_button_press(const resatr::MouseButtonPress& e) override {
        if (e.is_scroll()) {
            auto v = e.get_scroll_vector() * glm::vec2(0.1);
            _camera_pos += glm::vec3(v.x, 0.0, v.y);
            _dirty = true;
            return true;
        }
        if (e.get_key() == resatr::MouseButtonPress::Keys::Middle) {
            if (e.is_pressed()) {
                get_root().capture_mouse();
            }
            else {
                get_root().release_mouse();
            }
        }
        return false;
    }

    virtual bool on_mouse_relative_movement(const resatr::MouseMovement& e) override {
        glm::vec2 offset = e.normalized(get_root().get_context().get_window_size(),
                                        get_root().get_context().get_framebuffer_size());
        if (_shift_mode) {
            offset *= glm::vec2(0.05);
            _camera_pos += glm::vec3(offset, 0.0);
        }
        else {
            // offset *= glm::vec2(0.03);
            // _camera = glm::rotate(_camera, offset.y, glm::vec3(-1.0f, 0.0f, 0.0f));
            // _camera = glm::rotate(_camera, offset.x, glm::vec3(0.0f, 1.0f, 0.0f));
        }
        _dirty = true;
        return true;
    }
};

class OrthoBoxScene final : public resatr::opengl::OpenGLScene {
   public:
    OrthoBoxScene(resatr::opengl::OpenGLRenderLoop& root) :
        OpenGLScene(root, root, root.get_size(), true, true, false, true) {
        set_clear_color(glm::vec3(0.8, 0.5, 0.5));
        logger.logi("Loading OrthoBox!");
    }

    virtual void render() override {
        clear();
    }
};

class UI final : public resatr::opengl::NuklearOpenGLScene {
   protected:
    enum Demos {
        Null,
        PerspectiveBox,
        OrthoBox,
    };
    Demos current_option = Demos::Null;
    Demos last_option = current_option;

   public:
    UI(resatr::opengl::BasicOpenGLRenderLoop& root) :
        NuklearOpenGLScene(root, root, root.get_size()) {}

    virtual void render(nk_context* ctx) override {
        if (nk_begin(ctx, "Demo Switcher", nk_rect(50, 50, 230, 300),
                     NK_WINDOW_BORDER | NK_WINDOW_MOVABLE | NK_WINDOW_SCALABLE |
                          NK_WINDOW_MINIMIZABLE | NK_WINDOW_TITLE)) {
            nk_layout_row_dynamic(ctx, 25, 1);
            current_option = nk_option_label(ctx, "Null", current_option == Demos::Null) ?
                                  Demos::Null :
                                  current_option;
            current_option = nk_option_label(ctx, "Perspective Box",
                                             current_option == Demos::PerspectiveBox) ?
                                  Demos::PerspectiveBox :
                                  current_option;
            current_option =
                 nk_option_label(ctx, "Ortho Box", current_option == Demos::OrthoBox) ?
                      Demos::OrthoBox :
                      current_option;
        }
        nk_end(ctx);
    }

    virtual void draw() override {
        resatr::opengl::NuklearOpenGLScene::draw();
        // Switch to new 3D scene, while the last frame is finishing rendering.
        if (current_option != last_option) {
            auto& root = dynamic_cast<resatr::opengl::BasicOpenGLRenderLoop&>(get_root());
            last_option = current_option;
            logger.logi("Switching scene...");
            resatr::opengl::OpenGLScene* old_scene = nullptr;
            switch (current_option) {
                case Demos::Null: {
                    old_scene = root.set_3d_scene(nullptr);
                } break;
                case Demos::PerspectiveBox: {
                    PerspectiveBoxScene* new_scene = new PerspectiveBoxScene(root);
                    old_scene = root.set_3d_scene(new_scene);
                } break;
                case Demos::OrthoBox: {
                    OrthoBoxScene* new_scene = new OrthoBoxScene(root);
                    old_scene = root.set_3d_scene(new_scene);
                } break;
            }
            if (old_scene != nullptr) {
                delete old_scene;
            }
        }
    }

    ~UI() {
        auto& root = dynamic_cast<resatr::opengl::BasicOpenGLRenderLoop&>(get_root());
        resatr::opengl::OpenGLScene* scene = root.get_3d_scene();
        if (scene != nullptr) {
            delete scene;
        }
    }
};

int main() {
    pthread_setname_np(pthread_self(), "triangle");
    logger.configure(true, spec::Logger::DEBUG, "triangle.log", spec::Logger::ALL);
    // Initialization.
    logger.logi("Re:Satr in startup...");
    glfw::GLFW& glfw = glfw::GLFW::get_instance();
    logger.logi("Loaded GLFW, compiled version: ", glfw.get_compiled_version(),
                ", runtime version: ", glfw.get_runtime_version());
    // Window.
    glfw::WindowHints window_hints;
    window_hints.set_maximized(true);
    window_hints.set_srgb_capable(true);
    window_hints.set_opengl_profile(glfw::WindowHints::OpenGLCoreProfile);
    window_hints.set_opengl_forward_compat(true);
    window_hints.set_context_version_major(3);
    window_hints.set_context_version_minor(3);
    window_hints.set_red_bits(8);
    window_hints.set_green_bits(8);
    window_hints.set_blue_bits(8);
    window_hints.set_alpha_bits(0);
    window_hints.set_depth_bits(0);
    window_hints.set_stencil_bits(0);
    glfw::Window window("Re:Satr Triangle", window_hints);
    // Engine setup.
    resatr::NullLoadScheduler loader;
    resatr::opengl::NoopOpenGLPresenter presenter(window, loader);
    resatr::opengl::BasicOpenGLRenderLoop renderer(window, loader, presenter);
    renderer.set_clear_color(glm::vec3(0.8, 0.5, 0.5));
    UI ui(renderer);
    renderer.set_ui(&ui);
    // Main Loop
    renderer.loop();
    return EXIT_SUCCESS;
}
