#ifndef RESATR_SCENE_HPP
#define RESATR_SCENE_HPP
/**
 * @file
 * @brief Scenes are complex Renderables with input capabilities.
 */
#include <Attr.hpp>
#include <Utils.hpp>
#include <glfw.hpp>
#include <glm/vec2.hpp>

#include <ostream>

#include <macros.h>

namespace resatr {

class KeyboardButtonPress final {
   public:
    enum Keys {
        Space = GLFW_KEY_SPACE,
        Apostrophe = GLFW_KEY_APOSTROPHE,
        Comma = GLFW_KEY_COMMA,
        Minus = GLFW_KEY_MINUS,
        Period = GLFW_KEY_PERIOD,
        Slash = GLFW_KEY_SLASH,
        _0 = GLFW_KEY_0,
        _1 = GLFW_KEY_1,
        _2 = GLFW_KEY_2,
        _3 = GLFW_KEY_3,
        _4 = GLFW_KEY_4,
        _5 = GLFW_KEY_5,
        _6 = GLFW_KEY_6,
        _7 = GLFW_KEY_7,
        _8 = GLFW_KEY_8,
        _9 = GLFW_KEY_9,
        Semicolon = GLFW_KEY_SEMICOLON,
        Equal = GLFW_KEY_EQUAL,
        A = GLFW_KEY_A,
        B = GLFW_KEY_B,
        C = GLFW_KEY_C,
        D = GLFW_KEY_D,
        E = GLFW_KEY_E,
        F = GLFW_KEY_F,
        G = GLFW_KEY_G,
        H = GLFW_KEY_H,
        I = GLFW_KEY_I,
        J = GLFW_KEY_J,
        K = GLFW_KEY_K,
        L = GLFW_KEY_L,
        M = GLFW_KEY_M,
        N = GLFW_KEY_N,
        O = GLFW_KEY_O,
        P = GLFW_KEY_P,
        Q = GLFW_KEY_Q,
        R = GLFW_KEY_R,
        S = GLFW_KEY_S,
        T = GLFW_KEY_T,
        U = GLFW_KEY_U,
        V = GLFW_KEY_V,
        W = GLFW_KEY_W,
        X = GLFW_KEY_X,
        Y = GLFW_KEY_Y,
        Z = GLFW_KEY_Z,
        LeftBracket = GLFW_KEY_LEFT_BRACKET,
        Backslash = GLFW_KEY_BACKSLASH,
        RightBracket = GLFW_KEY_RIGHT_BRACKET,
        GraveAccent = GLFW_KEY_GRAVE_ACCENT,
        World1 = GLFW_KEY_WORLD_1,
        World2 = GLFW_KEY_WORLD_2,
        Escape = GLFW_KEY_ESCAPE,
        Enter = GLFW_KEY_ENTER,
        Tab = GLFW_KEY_TAB,
        Backspace = GLFW_KEY_BACKSPACE,
        Insert = GLFW_KEY_INSERT,
        Delete = GLFW_KEY_DELETE,
        Right = GLFW_KEY_RIGHT,
        Left = GLFW_KEY_LEFT,
        Down = GLFW_KEY_DOWN,
        Up = GLFW_KEY_UP,
        PageUp = GLFW_KEY_PAGE_UP,
        PageDown = GLFW_KEY_PAGE_DOWN,
        Home = GLFW_KEY_HOME,
        End = GLFW_KEY_END,
        CapsLock = GLFW_KEY_CAPS_LOCK,
        ScrollLock = GLFW_KEY_SCROLL_LOCK,
        NumLock = GLFW_KEY_NUM_LOCK,
        PrintScreen = GLFW_KEY_PRINT_SCREEN,
        Pause = GLFW_KEY_PAUSE,
        F1 = GLFW_KEY_F1,
        F2 = GLFW_KEY_F2,
        F3 = GLFW_KEY_F3,
        F4 = GLFW_KEY_F4,
        F5 = GLFW_KEY_F5,
        F6 = GLFW_KEY_F6,
        F7 = GLFW_KEY_F7,
        F8 = GLFW_KEY_F8,
        F9 = GLFW_KEY_F9,
        F10 = GLFW_KEY_F10,
        F11 = GLFW_KEY_F11,
        F12 = GLFW_KEY_F12,
        F13 = GLFW_KEY_F13,
        F14 = GLFW_KEY_F14,
        F15 = GLFW_KEY_F15,
        F16 = GLFW_KEY_F16,
        F17 = GLFW_KEY_F17,
        F18 = GLFW_KEY_F18,
        F19 = GLFW_KEY_F19,
        F20 = GLFW_KEY_F20,
        F21 = GLFW_KEY_F21,
        F22 = GLFW_KEY_F22,
        F23 = GLFW_KEY_F23,
        F24 = GLFW_KEY_F24,
        F25 = GLFW_KEY_F25,
        KP0 = GLFW_KEY_KP_0,
        KP1 = GLFW_KEY_KP_1,
        KP2 = GLFW_KEY_KP_2,
        KP3 = GLFW_KEY_KP_3,
        KP4 = GLFW_KEY_KP_4,
        KP5 = GLFW_KEY_KP_5,
        KP6 = GLFW_KEY_KP_6,
        KP7 = GLFW_KEY_KP_7,
        KP8 = GLFW_KEY_KP_8,
        KP9 = GLFW_KEY_KP_9,
        KPDecimal = GLFW_KEY_KP_DECIMAL,
        KPDivide = GLFW_KEY_KP_DIVIDE,
        KPMultiply = GLFW_KEY_KP_MULTIPLY,
        KPSubtract = GLFW_KEY_KP_SUBTRACT,
        KPAdd = GLFW_KEY_KP_ADD,
        KPEnter = GLFW_KEY_KP_ENTER,
        KPEqual = GLFW_KEY_KP_EQUAL,
        LeftShift = GLFW_KEY_LEFT_SHIFT,
        LeftControl = GLFW_KEY_LEFT_CONTROL,
        LeftAlt = GLFW_KEY_LEFT_ALT,
        LeftSuper = GLFW_KEY_LEFT_SUPER,
        RightShift = GLFW_KEY_RIGHT_SHIFT,
        RightControl = GLFW_KEY_RIGHT_CONTROL,
        RightAlt = GLFW_KEY_RIGHT_ALT,
        RightSuper = GLFW_KEY_RIGHT_SUPER,
        Menu = GLFW_KEY_MENU,
    };

    static const char* to_string(Keys k);

   protected:
    /** Type of key pressed. */
    const Keys _key;
    const bool _pressed : 1;
    const bool _shift : 1;
    const bool _control : 1;
    const bool _alt : 1;
    const bool _super : 1;
    const bool _caps_lock : 1;
    const bool _num_lock : 1;

   public:
    /**
     * Not all values generated from glfw are valid for this class.
     */
    KeyboardButtonPress(int key, int action, int mods);

    Keys get_key() const {
        return _key;
    }
    bool is_pressed() const {
        return _pressed;
    }
    bool has_shift() const {
        return _shift;
    }
    bool has_control() const {
        return _control;
    }
    bool has_alt() const {
        return _alt;
    }
    bool has_super() const {
        return _super;
    }
    bool has_caps_lock() const {
        return _caps_lock;
    }
    bool has_num_lock() const {
        return _num_lock;
    }
};

std::ostream& operator<<(std::ostream& o, const KeyboardButtonPress& v);

class MouseButtonPress final {
   public:
    enum Keys {
        Left = GLFW_MOUSE_BUTTON_1,
        Right = GLFW_MOUSE_BUTTON_2,
        Middle = GLFW_MOUSE_BUTTON_3,
        AUX1 = GLFW_MOUSE_BUTTON_4,
        AUX2 = GLFW_MOUSE_BUTTON_5,
        AUX3 = GLFW_MOUSE_BUTTON_6,
        AUX4 = GLFW_MOUSE_BUTTON_7,
        AUX5 = GLFW_MOUSE_BUTTON_8,
        SCROLL_UP,
        SCROLL_DOWN,
        SCROLL_LEFT,
        SCROLL_RIGHT
    };

    static const char* to_string(Keys k);

   protected:
    const Keys _key : 16;
    const bool _pressed : 1;
    const bool _shift : 1;
    const bool _control : 1;
    const bool _alt : 1;
    const bool _super : 1;
    const bool _caps_lock : 1;
    const bool _num_lock : 1;
    const float _offset;

   public:
    /**
     * Normal button press constructor.
     */
    MouseButtonPress(int key, int action, int mods);

    /**
     * Scroll wheel button constructor.
     */
    MouseButtonPress(Keys key, double offset);

    Keys get_key() const {
        return _key;
    }
    bool is_pressed() const {
        return _pressed;
    }
    bool has_shift() const {
        return _shift;
    }
    bool has_control() const {
        return _control;
    }
    bool has_alt() const {
        return _alt;
    }
    bool has_super() const {
        return _super;
    }
    bool has_caps_lock() const {
        return _caps_lock;
    }
    bool has_num_lock() const {
        return _num_lock;
    }

    bool is_scroll() const;

    float get_scroll_offset() const {
        return _offset;
    }

    glm::vec2 get_scroll_vector() const;
};

std::ostream& operator<<(std::ostream& o, const MouseButtonPress& v);

class MouseMovement final {
   protected:
    double _x = 0;
    double _y = 0;

   public:
    MouseMovement() = default;

    MouseMovement(double x, double y) : _x(x), _y(y) {}

    MouseMovement(glm::dvec2 p) : _x(p.x), _y(p.y) {}

    MouseMovement& operator+=(const MouseMovement& o) {
        _x += o._x;
        _y += o._y;
        return *this;
    }

    friend MouseMovement operator-(const MouseMovement& a, const MouseMovement& b) {
        return MouseMovement(static_cast<glm::dvec2>(a) - static_cast<glm::dvec2>(b));
    }

    bool operator!=(double v) const {
        return _x != v && _y != v;
    }

    operator glm::dvec2() const {
        return glm::dvec2(_x, _y);
    }

    /**
     * Convert to normalized screen coordinates.
     */
    glm::dvec2 normalized(glfw::Window& w) const {
        return normalized(w.get_window_size(), w.get_framebuffer_size());
    }
    /**
     * Convert to normalized screen coordinates.
     */
    glm::dvec2 normalized(glm::ivec2 window_size, glm::ivec2 framebuffer_size) const;

    double get_x() const {
        return _x;
    }

    double get_y() const {
        return _y;
    }
};

std::ostream& operator<<(std::ostream& o, const MouseMovement& v);

class RenderLoop;

class Scene : public virtual spec::INonCopyable {
   protected:
    /**
     * If true this scene will be rendered.
     */
    bool _visible = true;
    /**
     * If true this scene will receive input events.
     */
    bool _interactive = true;
    /**
     * RenderLoop is always the starting point.
     */
    RenderLoop& _root;
    /**
     * The scene which contains this.
     */
    Scene& _parent;

   public:
    /* Input events */
    /**
     * Receives an event for each key press/release event.
     *
     * Return true to consume this event.
     */
    virtual bool on_keyboard_button_press(unused const KeyboardButtonPress& event) {
        return false;
    }

    /**
     * Receives an event for each key press/release event.
     *
     * Return true to consume this event.
     */
    virtual bool on_mouse_button_press(unused const MouseButtonPress& event) {
        return false;
    }

    /**
     * Receives an event for each mouse movement.
     * Coordinates are absolute and in
     * reference to the OpenGL window.
     *
     * Return true to consume this event.
     */
    virtual bool on_mouse_movement(unused const MouseMovement& event) {
        return false;
    }

    /**
     * Receives an accumulated event each frame
     * for relative mouse movement.
     * This must be explicitly enabled on the root.
     *
     * Return true to consume this event.
     */
    virtual bool on_mouse_relative_movement(unused const MouseMovement& event) {
        return false;
    }

    /**
     * Receives the accumulated input text for each frame
     * independently from the KeyboardButtonPress events.
     *
     * The \p str is formatted as a utf8 string and contains
     * all typed and clipboard pasted text.
     *
     * Return true to consume this event.
     */
    virtual bool on_text_input(unused const char* str) {
        return false;
    }

    /* Setters */
    void set_visible(bool v) {
        _visible = v;
    }
    void set_interactive(bool v) {
        _interactive = v;
    }

    /* Getters */
    bool get_visible() {
        return _visible;
    }
    bool get_interactive() {
        return _interactive;
    }
    RenderLoop& get_root() {
        return _root;
    }
    Scene& get_parent_scene() {
        return _parent;
    }

    Scene(RenderLoop& root, Scene& parent) : _root(root), _parent(parent) {}
    virtual ~Scene() = default;
};

}  // namespace resatr

#endif /*RESATR_SCENE_HPP*/
