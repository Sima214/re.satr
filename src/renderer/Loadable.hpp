#ifndef RESATR_LOADABLE_HPP
#define RESATR_LOADABLE_HPP
/**
 * @file
 * @brief Represents an OpenGL/Vulkan entity which can be loaded asynchronously.
 */

#include <Uploadable.hpp>

namespace resatr {

class LoadScheduler;

class Loadable {
   public:
    /**
     * Status codes that represent the lifetime of a Loadable.
     */
    struct Status {
        bool yield;
        bool upload;
        bool destroy;
    };

    /**
     * Called from the renderer thread when a loadable
     * is added to a LoadScheduler's queue.
     */
    virtual void attach(LoadScheduler&, Loadable&, Uploadable&) {}

    /**
     * Override with code that should execute in a worker thread.
     */
    virtual Status load() = 0;

    /**
     * Release allocated memory used by load().
     */
    virtual void destroy() = 0;

    /**
     * Returns a value between [0, 1] representing
     * the current state of this loadable.
     *
     * Could be called from any thread.
     */
    virtual float get_load_progress();

    /**
     * Called from the renderer thread before uploading.
     */
    virtual void prepare_upload(LoadScheduler&, Loadable&, Uploadable&) {}

    /**
     * Called from the renderer thread after uploading is complete.
     */
    virtual void complete_upload(LoadScheduler&, Loadable&, Uploadable&) {}

    Loadable() = default;
    virtual ~Loadable() = default;
};

}  // namespace resatr

#endif /*RESATR_LOADABLE_HPP*/
