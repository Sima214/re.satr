#include "VAO.hpp"

#include <Trace.hpp>

#include <glad.h>

namespace resatr::opengl {

VertexAttributeObject::VertexAttributeObject() : _uid(0) {
    glGenVertexArrays(1, &_uid);
    spec::trace("vao::new(", _uid, ")");
}

VertexAttributeObject::~VertexAttributeObject() {
    if (_uid != 0) {
        spec::trace("vao::delete(", _uid, ")");
        glDeleteVertexArrays(1, &_uid);
    }
}

void VertexAttributeObject::_bind(Program::Attribute& attr, int32_t size,
                                 Types type, bool normalized, uint32_t stride,
                                 uintptr_t offset) {
    glVertexAttribPointer(attr.get_location(), size, type, normalized ? GL_TRUE : GL_FALSE,
                          stride, (const void*) offset);
    glEnableVertexAttribArray(attr.get_location());
}

void VertexAttributeObject::bind(Program::Attribute& attr, Buffer& buf, int32_t size,
                                 Types type, bool normalized, uint32_t stride,
                                 uintptr_t offset) {
    buf.bind();
    bind();
    _bind(attr, size, type, normalized, stride, offset);
    unbind();
    buf.unbind();
}

void VertexAttributeObject::unbind(Program::Attribute& attr) {
    bind();
    glDisableVertexAttribArray(attr.get_location());
    unbind();
}

void VertexAttributeObject::bind() const {
    glBindVertexArray(_uid);
}

void VertexAttributeObject::unbind() const {
    glBindVertexArray(0);
}

}  // namespace resatr::opengl
