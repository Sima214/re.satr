#include "Buffer.hpp"

#include <Trace.hpp>

#include <cstddef>

namespace resatr::opengl {

Buffer::Buffer(Type type) : _uid(0), _type(type), _state(State::EMPTY) {
    glGenBuffers(1, &_uid);
    spec::trace("buffer::new(", _uid, ")");
}

Buffer::Buffer(Type type, BufferData& data) : Buffer(type) {
    set_data(data);
}

Buffer::~Buffer() {
    if (_uid != 0) {
        spec::trace("buffer::delete(", _uid, ")");
        glDeleteBuffers(1, &_uid);
    }
}

void Buffer::set_data(BufferData& data) {
    _data = &data;
}

void Buffer::clear_data() {
    _data = nullptr;
}

void Buffer::bind() const {
    glBindBuffer(_type, _uid);
}

void Buffer::unbind() const {
    glBindBuffer(_type, 0);
}

size_t Buffer::get_upload_cost() {
    if (_data != nullptr) {
        size_t buffer_size = _data->get_upload_data().first;
        size_t tokens = buffer_size / 1024;
        if ((buffer_size % 1024) != 0) {
            tokens += 1;
        }
        return tokens;
    }
    return 0;
}

void Buffer::upload() {
    if (_data != nullptr) {
        auto data = _data->get_upload_data();
        bind();
        spec::trace("buffer::upload(", _uid, ", ", data.first, ")");
        glBufferData(_type, data.first, data.second, _data->get_usage());
        unbind();
    }
}

bool Buffer::ready() {
    return _state == State::PARTIAL || _state == State::READY;
}

void Buffer::deallocate() {
    if (_uid != 0) {
        spec::trace("buffer::clear(", _uid, ")");
        bind();
        // May not work on all drivers.
        glBufferData(_type, 0, nullptr, Usage::StaticDraw);
        unbind();
    }
}

}  // namespace resatr::opengl
