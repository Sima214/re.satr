#ifndef RESATR_OPENGL_VAO_HPP
#define RESATR_OPENGL_VAO_HPP
/**
 * @file
 * @brief Vertex Attribute Bindings. Their usage is required in core context.
 */

#include <Trace.hpp>
#include <opengl/buffer/Buffer.hpp>
#include <opengl/core/Bindable.hpp>
#include <opengl/core/Types.hpp>
#include <opengl/program/Program.hpp>

#include <cstdint>
#include <utility>

namespace resatr::opengl {

class VertexAttributeObject : public Bindable {
   protected:
    uint32_t _uid;

   public:
    VertexAttributeObject(VertexAttributeObject&& o) : _uid(std::exchange(o._uid, 0)) {
        spec::trace("vao::new_move(", _uid, ")");
    }

    VertexAttributeObject& operator=(VertexAttributeObject&& o) {
        _uid = std::exchange(o._uid, 0);
        spec::trace("vao::assign_move(", _uid, ")");
        return *this;
    }

    virtual ~VertexAttributeObject();

   protected:
    /**
     * Constructs an empty VAO.
     */
    VertexAttributeObject();

    constexpr VertexAttributeObject(void*) : _uid(0) {}

   public:
    /**
     * Bind and enable attribute.
     * Objects must be externally bound.
     */
    void _bind(Program::Attribute& attr, int32_t size, Types type, bool normalized = false,
               uint32_t stride = 0, uintptr_t offset = 0);
    /**
     * Bind and enable attribute to buffer.
     */
    void bind(Program::Attribute& attr, Buffer& buf, int32_t size, Types type,
              bool normalized = false, uint32_t stride = 0, uintptr_t offset = 0);

    /**
     * Disable the specified vertex attribute index.
     */
    void unbind(Program::Attribute& attr);

    /** Bindable */
    virtual void bind() const override;
    virtual void unbind() const override;

    static VertexAttributeObject create_invalid() {
        return VertexAttributeObject(0);
    }

    friend class StaticFactory;
};

}  // namespace resatr::opengl

#endif /*RESATR_OPENGL_VAO_HPP*/
