#ifndef RESATR_OPENGL_BUFFER_HPP
#define RESATR_OPENGL_BUFFER_HPP
/**
 * @file
 * @brief OpenGL buffers.
 */

#include <Loadable.hpp>
#include <Trace.hpp>
#include <Uploadable.hpp>
#include <Utils.hpp>
#include <opengl/core/Bindable.hpp>

#include <cstdint>
#include <utility>

#include <glad.h>

namespace resatr::opengl {

class BufferData;

/**
 * Base OpenGL buffer.
 */
class Buffer : public Bindable, public Uploadable {
   public:
    enum State { INVALID, EMPTY, PARTIAL, READY };
    enum Type {
        ArrayBuffer = GL_ARRAY_BUFFER,
        CopyReadBuffer = GL_COPY_READ_BUFFER,
        CopyWriteBuffer = GL_COPY_WRITE_BUFFER,
        ElementArrayBuffer = GL_ELEMENT_ARRAY_BUFFER,
        PixelPackBuffer = GL_PIXEL_PACK_BUFFER,
        PixelUnpackBuffer = GL_PIXEL_UNPACK_BUFFER,
        TextureBuffer = GL_TEXTURE_BUFFER,
        TransformFeedbackBuffer = GL_TRANSFORM_FEEDBACK_BUFFER,
        UniformBuffer = GL_UNIFORM_BUFFER
    };
    enum Usage {
        StreamDraw = GL_STREAM_DRAW,
        StreamRead = GL_STREAM_READ,
        StreamCopy = GL_STREAM_COPY,
        StaticDraw = GL_STATIC_DRAW,
        StaticRead = GL_STATIC_READ,
        StaticCopy = GL_STATIC_COPY,
        DynamicDraw = GL_DYNAMIC_DRAW,
        DynamicRead = GL_DYNAMIC_READ,
        DynamicCopy = GL_DYNAMIC_COPY
    };

   protected:
    uint32_t _uid;
    Type _type = Type::ArrayBuffer;
    State _state;
    BufferData* _data = nullptr;

   public:
    /**
     * Constructs an invalid buffer.
     */
    Buffer(State state) : _uid(0), _state(state) {}

    Buffer(Buffer&& o) :
        _uid(std::exchange(o._uid, 0)), _type(std::exchange(o._type, Type::ArrayBuffer)),
        _state(std::exchange(o._state, State::INVALID)) {
        spec::trace("buffer::new_move(", _uid, ")");
    }

    Buffer& operator=(Buffer&& o) {
        _uid = std::exchange(o._uid, 0);
        _type = std::exchange(o._type, Type::ArrayBuffer);
        _state = std::exchange(o._state, State::INVALID);
        spec::trace("buffer::assign_move(", _uid, ")");
        return *this;
    }

    virtual ~Buffer();

   protected:
    /**
     * Allocate new empty buffer.
     */
    Buffer(Type type);

    /**
     * Allocate new empty buffer and prepare data for upload.
     */
    Buffer(Type type, BufferData& data);

   public:
    /**
     * Sets data to be uploaded.
     */
    void set_data(BufferData& data);

    /**
     * Clears any set data.
     */
    void clear_data();

    /** Bindable */
    virtual void bind() const override;
    virtual void unbind() const override;

    /** Uploadable */
    virtual size_t get_upload_cost() override;
    virtual void upload() override;
    virtual bool ready() override;
    virtual void deallocate() override;

    static Buffer create_invalid() {
        return Buffer(State::INVALID);
    }

    friend class StaticFactory;
};

/**
 * Class for preparing data for upload to OpenGL buffers.
 */
class BufferData : public spec::INonCopyable {
   public:
    virtual Buffer::Usage get_usage() const {
        return Buffer::Usage::StaticDraw;
    }

    virtual std::pair<size_t, const void*> get_upload_data() const = 0;

    virtual ~BufferData() = default;
};

class StaticBufferData final : public BufferData {
   protected:
    std::size_t _length;
    const void* _data;

   public:
    template<typename T, size_t N> constexpr StaticBufferData(const T (&vec_array)[N]) {
        _length = N * sizeof(T);
        _data = (const void*) &vec_array[0];
    }

    virtual std::pair<size_t, const void*> get_upload_data() const {
        return std::pair(_length, _data);
    };
};

}  // namespace resatr::opengl

#endif /*RESATR_OPENGL_BUFFER_HPP*/
