#include "Texture.hpp"

#include <Trace.hpp>
#include <opengl/core/Error.hpp>
#include <opengl/core/Types.hpp>

#include <glad.h>

namespace resatr::opengl {

static GLenum dimensionality2binding(ITexture::Dimensionality dims) {
    switch (dims) {
        case ITexture::Dimensionality::Texture1D: return GL_TEXTURE_BINDING_1D;
        case ITexture::Dimensionality::Texture2D: return GL_TEXTURE_BINDING_2D;
        case ITexture::Dimensionality::Texture3D: return GL_TEXTURE_BINDING_3D;
        case ITexture::Dimensionality::Texture1Darray: return GL_TEXTURE_BINDING_1D_ARRAY;
        case ITexture::Dimensionality::Texture2Darray: return GL_TEXTURE_BINDING_2D_ARRAY;
        case ITexture::Dimensionality::TextureRectangle: return GL_TEXTURE_BINDING_RECTANGLE;
        case ITexture::Dimensionality::TextureCubeMap: return GL_TEXTURE_BINDING_CUBE_MAP;
        case ITexture::Dimensionality::Texture2DMultisample:
            return GL_TEXTURE_BINDING_2D_MULTISAMPLE;
        case ITexture::Dimensionality::Texture2DMultisampleArray:
            return GL_TEXTURE_BINDING_2D_MULTISAMPLE_ARRAY;
        case ITexture::Dimensionality::TextureBuffer: return GL_TEXTURE_BINDING_BUFFER;
        default: throw interface_error("Invalid texture dimensionality to binding conversion!");
    }
}

/**
 * Get a pair of Format and Type than can be used with null
 * data without triggering an invalid operation error.
 */
static std::pair<Formats, Types> get_valid_upload_format(ITexture::InternalFormats f) {
    switch (f) {
        /* Single color */
        case ITexture::InternalFormats::Red:
        case ITexture::InternalFormats::R8:
        case ITexture::InternalFormats::R8Snorm:
        case ITexture::InternalFormats::R16:
        case ITexture::InternalFormats::R16Snorm:
        case ITexture::InternalFormats::R16F:
        case ITexture::InternalFormats::R32F:
        case ITexture::InternalFormats::R8I:
        case ITexture::InternalFormats::R8UI:
        case ITexture::InternalFormats::R16I:
        case ITexture::InternalFormats::R16UI:
        case ITexture::InternalFormats::R32I:
        case ITexture::InternalFormats::R32UI: {
            return std::pair<Formats, Types>(Formats::Red, Types::UnsignedByte);
        }
        /* RG color */
        case ITexture::InternalFormats::RG:
        case ITexture::InternalFormats::RG8:
        case ITexture::InternalFormats::RG8Snorm:
        case ITexture::InternalFormats::RG16:
        case ITexture::InternalFormats::RG16Snorm:
        case ITexture::InternalFormats::RG16F:
        case ITexture::InternalFormats::RG32F:
        case ITexture::InternalFormats::RG8I:
        case ITexture::InternalFormats::RG8UI:
        case ITexture::InternalFormats::RG16I:
        case ITexture::InternalFormats::RG16UI:
        case ITexture::InternalFormats::RG32I:
        case ITexture::InternalFormats::RG32UI: {
            return std::pair<Formats, Types>(Formats::RedGreen, Types::UnsignedByte);
        }
        /* RGB color */
        case ITexture::InternalFormats::RGB:
        case ITexture::InternalFormats::R3G3B2:
        case ITexture::InternalFormats::RGB4:
        case ITexture::InternalFormats::RGB5:
        case ITexture::InternalFormats::RGB8:
        case ITexture::InternalFormats::RGB8Snorm:
        case ITexture::InternalFormats::RGB10:
        case ITexture::InternalFormats::RGB12:
        case ITexture::InternalFormats::RGB16Snorm:
        case ITexture::InternalFormats::RGB16F:
        case ITexture::InternalFormats::SRGB8:
        case ITexture::InternalFormats::RGB32F:
        case ITexture::InternalFormats::R11FG11FB10F:
        case ITexture::InternalFormats::RGB9E5:
        case ITexture::InternalFormats::RGB8I:
        case ITexture::InternalFormats::RGB8UI:
        case ITexture::InternalFormats::RGB16I:
        case ITexture::InternalFormats::RGB16UI:
        case ITexture::InternalFormats::RGB32I:
        case ITexture::InternalFormats::RGB32UI: {
            return std::pair<Formats, Types>(Formats::RGB, Types::UnsignedByte);
        }
        /* RGBA color */
        case ITexture::InternalFormats::RGBA:
        case ITexture::InternalFormats::SRGB8A8:
        case ITexture::InternalFormats::RGB5A1:
        case ITexture::InternalFormats::RGB10A2:
        case ITexture::InternalFormats::RGB10A2UI:
        case ITexture::InternalFormats::RGBA12:
        case ITexture::InternalFormats::RGBA16:
        case ITexture::InternalFormats::RGBA16F:
        case ITexture::InternalFormats::RGBA8:
        case ITexture::InternalFormats::RGBA8Snorm:
        case ITexture::InternalFormats::RGBA2:
        case ITexture::InternalFormats::RGBA4:
        case ITexture::InternalFormats::RGBA32F:
        case ITexture::InternalFormats::RGBA8I:
        case ITexture::InternalFormats::RGBA8UI:
        case ITexture::InternalFormats::RGBA16I:
        case ITexture::InternalFormats::RGBA16UI:
        case ITexture::InternalFormats::RGBA32I:
        case ITexture::InternalFormats::RGBA32U: {
            return std::pair<Formats, Types>(Formats::RGBA, Types::UnsignedByte);
        }
        /* Depth */
        case ITexture::InternalFormats::DepthComponent:
        case ITexture::InternalFormats::DepthComponent16:
        case ITexture::InternalFormats::DepthComponent24:
        case ITexture::InternalFormats::DepthComponent32:
        case ITexture::InternalFormats::DepthComponent32F: {
            return std::pair<Formats, Types>(Formats::DepthComponent, Types::UnsignedByte);
        }
        /* Stencil */
        case ITexture::InternalFormats::StencilIndex:
        case ITexture::InternalFormats::StencilIndex8:
        /* Depth|Stencil */
        case ITexture::InternalFormats::DepthStencil:
        case ITexture::InternalFormats::Depth24Stencil8:
        case ITexture::InternalFormats::Depth32FStencil8: {
            return std::pair<Formats, Types>(Formats::StencilIndex, Types::UnsignedByte);
        }
        default: {
            return std::pair<Formats, Types>(Formats::Red, Types::UnsignedByte);
        }
    }
}

ITexture::ITexture(Dimensionality dims, InternalFormats format) :
    _uid(0), _state(State::EMPTY), _dims(dims), _format(format) {
    glGenTextures(1, &_uid);
    if (_uid == 0) {
        throw opengl_error("Cannot allocate texture!");
    }
    // Bind to set texture dimensionality.
    int32_t old_bound_uid;
    glGetIntegerv(dimensionality2binding(_dims), &old_bound_uid);
    glBindTexture(_dims, _uid);
    glBindTexture(_dims, old_bound_uid);
}

void ITexture::set_filtering(Filtering min, Filtering mag) {
    bind();
    glTexParameteri(_dims, GL_TEXTURE_MIN_FILTER, min);
    glTexParameteri(_dims, GL_TEXTURE_MAG_FILTER, mag);
    unbind();
}

void ITexture::set_wrapping(Wrapping s, Wrapping t, Wrapping r) {
    bind();
    glTexParameteri(_dims, GL_TEXTURE_WRAP_S, s);
    glTexParameteri(_dims, GL_TEXTURE_WRAP_T, t);
    glTexParameteri(_dims, GL_TEXTURE_WRAP_R, r);
    unbind();
}

void ITexture::bind(int32_t active_texture_index) const {
    glActiveTexture(GL_TEXTURE0 + active_texture_index);
    bind();
}
void ITexture::unbind(int32_t active_texture_index) const {
    glActiveTexture(GL_TEXTURE0 + active_texture_index);
    unbind();
    glActiveTexture(GL_TEXTURE0);
}

void ITexture::bind() const {
    glBindTexture(_dims, _uid);
}
void ITexture::unbind() const {
    glBindTexture(_dims, 0);
}

bool ITexture::ready() {
    return _state == State::PARTIAL || _state == State::COMPLETE;
}
void ITexture::deallocate() {
    if (_uid != 0) {
        spec::trace("texture::deallocate(", _uid, ")");
        empty();
        _state = State::EMPTY;
    }
}

ITexture::~ITexture() {
    if (_uid != 0) {
        spec::trace("texture::delete(", _uid, ")");
        glDeleteTextures(1, &_uid);
        _uid = 0;
        _state = State::INVALID;
    }
}

template<> glm::uvec3 TextureData::get_size<glm::uvec3>() const {
    return glm::uvec3(get_size(0), get_size(1), get_size(2));
}

template<> glm::uvec2 TextureData::get_size<glm::uvec2>() const {
    return glm::uvec2(get_size(0), get_size(1));
}

template<> glm::uvec1 TextureData::get_size<glm::uvec1>() const {
    return glm::uvec1(get_size(0));
}

uint32_t TextureData::get_data_length() const {
    uint32_t accum = get_size(0);
    for (uint32_t i = 1; i < get_dimensions(); i++) {
        accum *= get_size(i);
    }
    accum *= opengl_sizeof(get_type(), get_format());
    return accum;
}

template<> void Texture<ITexture::Dimensionality::Texture1D>::empty() {
    spec::trace("texture::empty(", _uid, ", ", _size.x, ")");
    bind();
    auto format = get_valid_upload_format(_format);
    glTexImage1D(_dims, 0, _format, _size.x, 0, format.first, format.second, nullptr);
    _state = State::EMPTY;
    unbind();
    opengl_error::debug();
}
template<> void Texture<ITexture::Dimensionality::Texture1D>::upload() {
    if (_data != nullptr) {
        spec::trace("texture::upload(", _uid, ", ", _size.x, ")");
        set_size(_data->get_size<SizeType>());
        bind();
        glTexImage1D(_dims, 0, _format, _size.x, 0, _data->get_format(), _data->get_type(),
                     _data->get_upload_data());
        _state = State::COMPLETE;
        unbind();
        opengl_error::debug();
    }
}

template<> void Texture<ITexture::Dimensionality::Texture2D>::empty() {
    spec::trace("texture::empty(", _uid, ", ", _size.x, ", ", _size.y, ")");
    bind();
    auto format = get_valid_upload_format(_format);
    glTexImage2D(_dims, 0, _format, _size.x, _size.y, 0, format.first, format.second, nullptr);
    _state = State::EMPTY;
    unbind();
    opengl_error::debug();
}
template<> void Texture<ITexture::Dimensionality::Texture2D>::upload() {
    if (_data != nullptr) {
        spec::trace("texture::upload(", _uid, ", ", _size.x, ", ", _size.y, ")");
        set_size(_data->get_size<SizeType>());
        bind();
        glTexImage2D(_dims, 0, _format, _size.x, _size.y, 0, _data->get_format(),
                     _data->get_type(), _data->get_upload_data());
        _state = State::COMPLETE;
        unbind();
        opengl_error::debug();
    }
}

template<> void Texture<ITexture::Dimensionality::Texture3D>::empty() {
    spec::trace("texture::empty(", _uid, ", ", _size.x, ", ", _size.y, ", ", _size.z, ")");
    bind();
    auto format = get_valid_upload_format(_format);
    glTexImage3D(_dims, 0, _format, _size.x, _size.y, _size.z, 0, format.first, format.second,
                 nullptr);
    _state = State::EMPTY;
    unbind();
    opengl_error::debug();
}
template<> void Texture<ITexture::Dimensionality::Texture3D>::upload() {
    if (_data != nullptr) {
        set_size(_data->get_size<SizeType>());
        spec::trace("texture::upload(", _uid, ", ", _size.x, ", ", _size.y, ", ", _size.z, ")");
        bind();
        glTexImage3D(_dims, 0, _format, _size.x, _size.y, _size.z, 0, _data->get_format(),
                     _data->get_type(), _data->get_upload_data());
        _state = State::COMPLETE;
        unbind();
        opengl_error::debug();
    }
}

}  // namespace resatr::opengl
