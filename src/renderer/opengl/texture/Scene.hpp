#ifndef RESATR_OPENGL_SCENE_HPP
#define RESATR_OPENGL_SCENE_HPP
/**
 * @file
 * @brief OpenGL framebuffer with rendering and input event apis.
 */

#include <Scene.hpp>
#include <StopWatch.hpp>
#include <glm/vec2.hpp>
#include <opengl/StaticFactory.hpp>
#include <opengl/buffer/Buffer.hpp>
#include <opengl/buffer/VAO.hpp>
#include <opengl/program/Program.hpp>
#include <opengl/renderer/RenderLoop.hpp>
#include <opengl/texture/Layer.hpp>
#include <opengl/texture/Texture.hpp>
#include <opengl/utils/DrawCommands.hpp>

#include <utility>
#include <vector>

#include <nuklear.h>

namespace resatr::opengl {

class OpenGLScene : public OpenGLLayer, public resatr::Scene {
   public:
    OpenGLScene(OpenGLRenderLoop& root, Scene& parent, glm::ivec2 size, bool colors = false,
                bool colors_hdr = true, bool colors_alpha = false, bool depth = false,
                bool stencil = false) :
        OpenGLLayer(static_cast<StaticFactory&>(root), size, colors, colors_hdr, colors_alpha,
                    depth, stencil),
        Scene(static_cast<RenderLoop&>(root), parent) {}

    virtual ~OpenGLScene() = default;

    /* Layer */
    using OpenGLLayer::clear;
    using OpenGLLayer::dispatch_render;
    using OpenGLLayer::draw;
    using OpenGLLayer::has_colors;
    using OpenGLLayer::has_depth;
    using OpenGLLayer::has_stencil;
    using OpenGLLayer::is_dirty;
    using OpenGLLayer::mark_dirty;
    using OpenGLLayer::ready;
    using OpenGLLayer::set_clear_color;
    using OpenGLLayer::set_clear_depth;
    using OpenGLLayer::set_clear_stencil;
    using OpenGLLayer::target;
    using OpenGLLayer::unmark_dirty;

    /* Scene */
    using Scene::get_interactive;
    using Scene::get_parent_scene;
    using Scene::get_root;
    using Scene::get_visible;
    using Scene::on_keyboard_button_press;
    using Scene::on_mouse_button_press;
    using Scene::on_mouse_movement;
    using Scene::on_mouse_relative_movement;
    using Scene::on_text_input;
    using Scene::set_interactive;
    using Scene::set_visible;

   protected:
    /* Layer */
    using OpenGLLayer::on_size_update;
    using OpenGLLayer::render;
    using OpenGLLayer::tick;
};

class NuklearOpenGLScene : public OpenGLScene {
   protected:
    class NuklearBufferLoader final : public BufferData {
       protected:
        nk_buffer _buf;

       public:
        virtual Buffer::Usage get_usage() const {
            return Buffer::Usage::StreamDraw;
        }

        virtual std::pair<size_t, const void*> get_upload_data() const {
            return std::pair<size_t, const void*>(_buf.memory.size, _buf.memory.ptr);
        };

        nk_buffer* get_nk_buffer() {
            return &_buf;
        }

        void clear() {
            nk_buffer_clear(&_buf);
        }

        NuklearBufferLoader() {
            nk_buffer_init_default(&_buf);
        }
        ~NuklearBufferLoader() {
            nk_buffer_free(&_buf);
        }
    };

   protected:
    nk_context _nk_ctx;
    nk_buffer _nk_cmds;
    nk_font_atlas _nk_atlas;
    nk_draw_null_texture _nk_texture_null;
    Program _nk_program = Program::create_invalid();
    Program::Uniform _nk_program_size;
    Program::Uniform _nk_program_bounds;
    VertexAttributeObject _nk_buffer_bindings = VertexAttributeObject::create_invalid();
    Buffer _nk_buffer_vertices = Buffer::create_invalid();
    NuklearBufferLoader _nk_buffer_loader_vertices = NuklearBufferLoader();
    Buffer _nk_buffer_indexes = Buffer::create_invalid();
    NuklearBufferLoader _nk_buffer_loader_indexes = NuklearBufferLoader();
    Texture2D _nk_atlas_texture = Texture2D::create_invalid();
    RenderSpec _nk_render_config = RenderSpec();

    std::vector<uint8_t> _nk_cmds_last;

    glm::dvec2 _last_mouse_pos = {0, 0};
    bool _in_double_click = false;
    spec::StopWatch _double_click_timer = spec::StopWatch();

   public:
    NuklearOpenGLScene(OpenGLRenderLoop& root, Scene& parent, glm::ivec2 size);

    virtual ~NuklearOpenGLScene();

    /**
     * Override and declare the UI.
     */
    virtual void render(nk_context* ctx) = 0;

    /* Scene */
    virtual bool on_keyboard_button_press(const KeyboardButtonPress& event) override;
    virtual bool on_mouse_button_press(const MouseButtonPress& event) override;
    virtual bool on_mouse_movement(const MouseMovement& event) override;
    virtual bool on_text_input(const char* str) override;

   protected:
    /* Layer */
    virtual bool render() override final;
    virtual void tick() override final;

    virtual void on_size_update(glm::ivec2, glm::ivec2) override;
};

}  // namespace resatr::opengl

#endif /*RESATR_OPENGL_SCENE_HPP*/