#ifndef RESATR_OPENGL_TEXTURE_HPP
#define RESATR_OPENGL_TEXTURE_HPP
/**
 * @file
 * @brief Generic texture interface and subclasses for disk loading.
 */

#include <Trace.hpp>
#include <Uploadable.hpp>
#include <Utils.hpp>
#include <glm/gtc/vec1.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <opengl/core/Bindable.hpp>
#include <opengl/core/Error.hpp>
#include <opengl/core/Types.hpp>

#include <cstdint>
#include <type_traits>
#include <utility>

#include <glad.h>

namespace resatr::opengl {

/**
 * Generic-dimensionality OpenGL texture templateless interface.
 */
class ITexture : public Bindable, public Uploadable {
   public:
    enum State {
        INVALID,
        /** No size or format yet specified. */
        EMPTY,
        /** Not all pixels or mipmaps have been loaded. */
        PARTIAL,
        COMPLETE
    };

    enum Dimensionality {
        None = GL_NONE,
        Texture1D = GL_TEXTURE_1D,
        Texture2D = GL_TEXTURE_2D,
        Texture3D = GL_TEXTURE_3D,
        Texture1Darray = GL_TEXTURE_1D_ARRAY,
        Texture2Darray = GL_TEXTURE_2D_ARRAY,
        TextureRectangle = GL_TEXTURE_RECTANGLE,
        TextureCubeMap = GL_TEXTURE_CUBE_MAP,
        Texture2DMultisample = GL_TEXTURE_2D_MULTISAMPLE,
        Texture2DMultisampleArray = GL_TEXTURE_2D_MULTISAMPLE_ARRAY,
        TextureBuffer = GL_TEXTURE_BUFFER
    };

    /**
     * OpenGL texture internal formats.
     */
    enum InternalFormats {
        /* Invalid */
        InvalidFormat = GL_NONE,
        /* Base formats */
        DepthComponent = GL_DEPTH_COMPONENT,
        DepthStencil = GL_DEPTH_STENCIL,
        StencilIndex = GL_STENCIL_INDEX,
        Red = GL_RED,
        RG = GL_RG,
        RGB = GL_RGB,
        RGBA = GL_RGBA,
        /* Sized formats */
        R8 = GL_R8,
        R8Snorm = GL_R8_SNORM,
        R16 = GL_R16,
        R16Snorm = GL_R16_SNORM,
        RG8 = GL_RG8,
        RG8Snorm = GL_RG8_SNORM,
        RG16 = GL_RG16,
        RG16Snorm = GL_RG16_SNORM,
        R3G3B2 = GL_R3_G3_B2,
        RGB4 = GL_RGB4,
        RGB5 = GL_RGB5,
        RGB8 = GL_RGB8,
        RGB8Snorm = GL_RGB8_SNORM,
        RGB10 = GL_RGB10,
        RGB12 = GL_RGB12,
        RGB16Snorm = GL_RGB16_SNORM,
        RGBA2 = GL_RGBA2,
        RGBA4 = GL_RGBA4,
        RGB5A1 = GL_RGB5_A1,
        RGBA8 = GL_RGBA8,
        RGBA8Snorm = GL_RGBA8_SNORM,
        RGB10A2 = GL_RGB10_A2,
        RGB10A2UI = GL_RGB10_A2UI,
        RGBA12 = GL_RGBA12,
        RGBA16 = GL_RGBA16,
        SRGB8 = GL_SRGB8,
        SRGB8A8 = GL_SRGB8_ALPHA8,
        R16F = GL_R16F,
        RG16F = GL_RG16F,
        RGB16F = GL_RGB16F,
        RGBA16F = GL_RGBA16F,
        R32F = GL_R32F,
        RG32F = GL_RG32F,
        RGB32F = GL_RGB32F,
        RGBA32F = GL_RGBA32F,
        R11FG11FB10F = GL_R11F_G11F_B10F,
        RGB9E5 = GL_RGB9_E5,
        R8I = GL_R8I,
        R8UI = GL_R8UI,
        R16I = GL_R16I,
        R16UI = GL_R16UI,
        R32I = GL_R32I,
        R32UI = GL_R32UI,
        RG8I = GL_RG8I,
        RG8UI = GL_RG8UI,
        RG16I = GL_RG16I,
        RG16UI = GL_RG16UI,
        RG32I = GL_RG32I,
        RG32UI = GL_RG32UI,
        RGB8I = GL_RGB8I,
        RGB8UI = GL_RGB8UI,
        RGB16I = GL_RGB16I,
        RGB16UI = GL_RGB16UI,
        RGB32I = GL_RGB32I,
        RGB32UI = GL_RGB32UI,
        RGBA8I = GL_RGBA8I,
        RGBA8UI = GL_RGBA8UI,
        RGBA16I = GL_RGBA16I,
        RGBA16UI = GL_RGBA16UI,
        RGBA32I = GL_RGBA32I,
        RGBA32U = GL_RGBA32UI,
        DepthComponent16 = GL_DEPTH_COMPONENT16,
        DepthComponent24 = GL_DEPTH_COMPONENT24,
        DepthComponent32 = GL_DEPTH_COMPONENT32,
        DepthComponent32F = GL_DEPTH_COMPONENT32F,
        Depth24Stencil8 = GL_DEPTH24_STENCIL8,
        Depth32FStencil8 = GL_DEPTH32F_STENCIL8,
        StencilIndex8 = GL_STENCIL_INDEX8,
        /* Compressed formats */
        CompressedRed = GL_COMPRESSED_RED,
        CompressedRG = GL_COMPRESSED_RG,
        CompressedRGB = GL_COMPRESSED_RGB,
        CompressedRGBA = GL_COMPRESSED_RGBA,
        CompressedSRGB = GL_COMPRESSED_SRGB,
        CompressedSRGBA = GL_COMPRESSED_SRGB_ALPHA,
        CompressedRedRGTC1 = GL_COMPRESSED_RED_RGTC1,
        CompressedSignedRedRGTC1 = GL_COMPRESSED_SIGNED_RED_RGTC1,
        CompressedRGRGTC2 = GL_COMPRESSED_RG_RGTC2,
        CompressedSignedRGRGTC2 = GL_COMPRESSED_SIGNED_RG_RGTC2
    };

    enum Filtering {
        Nearest = GL_NEAREST,
        Linear = GL_LINEAR,
        NearestMipmapNearest = GL_NEAREST_MIPMAP_NEAREST,
        LinearMipmapNearest = GL_LINEAR_MIPMAP_NEAREST,
        NearestMipmapLinear = GL_NEAREST_MIPMAP_LINEAR,
        LinearMipmapLinear = GL_LINEAR_MIPMAP_LINEAR
    };

    enum Wrapping {
        ClampToEdge = GL_CLAMP_TO_EDGE,
        ClampToBorder = GL_CLAMP_TO_BORDER,
        MirroredRepeat = GL_MIRRORED_REPEAT,
        Repeat = GL_REPEAT
    };

   protected:
    uint32_t _uid;
    State _state;
    Dimensionality _dims;
    InternalFormats _format;

   public:
    ITexture(ITexture&& o) :
        _uid(std::exchange(o._uid, 0)), _state(std::exchange(o._state, State::INVALID)),
        _dims(std::exchange(o._dims, Dimensionality::None)),
        _format(std::exchange(o._format, InternalFormats::InvalidFormat)) {
        spec::trace("itexture::new_move(", _uid, ")");
    }

    ITexture& operator=(ITexture&& o) {
        _uid = std::exchange(o._uid, 0);
        _state = std::exchange(o._state, State::INVALID);
        _dims = std::exchange(o._dims, Dimensionality::None);
        _format = std::exchange(o._format, InternalFormats::InvalidFormat);
        spec::trace("itexture::assign_move(", _uid, ", ", o._uid, ")");
        return *this;
    }

    virtual ~ITexture();

    /**
     * Invalid texture constructor.
     */
    ITexture() :
        _uid(0), _state(State::INVALID), _dims(Dimensionality::None),
        _format(InternalFormats::InvalidFormat) {}

   protected:
    /**
     * Default texture constructor.
     */
    ITexture(Dimensionality dims, InternalFormats format);

   public:
    void set_filtering(Filtering min = Filtering::NearestMipmapLinear,
                       Filtering mag = Filtering::Linear);
    void set_wrapping(Wrapping s = Wrapping::Repeat, Wrapping t = Wrapping::Repeat,
                      Wrapping r = Wrapping::Repeat);
    /**
     * Releases storage without deleting the texture.
     */
    virtual void empty() = 0;

    void bind(int32_t active_texture_index) const;
    void unbind(int32_t active_texture_index) const;

    /* Bindable */
    virtual void bind() const;
    virtual void unbind() const;

    /* Uploadable */
    virtual bool ready();
    virtual void deallocate();
};

/**
 * Used for providing data for texture upload.
 */
class TextureData : public spec::INonCopyable {
   public:
    virtual uint32_t get_dimensions() const = 0;

    virtual uint32_t get_size(uint32_t dimension) const = 0;

    virtual Formats get_format() const = 0;

    virtual Types get_type() const = 0;

    virtual const void* get_upload_data() const = 0;

    virtual ~TextureData() = default;

    /**
     * Get texture buffer size as a glm vector type.
     */
    template<typename S> S get_size() const;
    /**
     * Get data length in bytes.
     */
    uint32_t get_data_length() const;
};

/**
 * Texture implementation for 1D, 2D and 3D textures.
 */
template<ITexture::Dimensionality dims> class Texture : public ITexture {
   public:
    static_assert(dims == Dimensionality::Texture1D || dims == Dimensionality::Texture2D ||
                       dims == Dimensionality::Texture3D,
                  "Texture class must be either of 1D or 2D or 3D dimensionality.");

    using SizeType = typename std::conditional<
         dims == Dimensionality::Texture1D, glm::uvec1,
         typename std::conditional<dims == Dimensionality::Texture2D, glm::uvec2,
                                   glm::uvec3>::type>::type;

   protected:
    SizeType _size;
    const TextureData* _data = nullptr;

    static const size_t Dimensions = sizeof(_size) / sizeof(int32_t);

   public:
    Texture(Texture&& o) : ITexture(std::forward<ITexture>(o)), _size(o._size) {
        spec::trace("texture::new_move(", _uid, ")");
    }
    Texture& operator=(Texture&& o) {
        ITexture::operator=(std::forward<ITexture>(o));
        _size = o._size;
        spec::trace("texture::assign_move(", _uid, ", ", o._uid, ")");
        return *this;
    }

    virtual ~Texture() = default;

   protected:
    Texture() : ITexture() {}

    Texture(ITexture::InternalFormats format) : ITexture(dims, format) {}

    Texture(ITexture::InternalFormats format, SizeType size) :
        ITexture(dims, format), _size(size) {}

    Texture(ITexture::InternalFormats format, const TextureData& data) :
        ITexture(dims, format) {
        set_data(data);
    }

    Texture(ITexture::InternalFormats format, SizeType size, const TextureData& data) :
        ITexture(dims, format), _size(size) {
        set_data(data);
    }

   public:
    void empty(SizeType size) {
        set_size(size);
        empty();
    }
    virtual void empty() override;

    /* Getters */
    SizeType get_size() {
        return _size;
    }

    /* Setters */
    void set_size(SizeType size) {
        _size = size;
    }
    void set_data(const TextureData& data) {
        // Validate data's dimensions.
        if (data.get_dimensions() != Dimensions) {
            throw interface_error("Dimension mismatch when assigning TextureData to Texture.");
        }
        _data = &data;
    }
    void clear_data() {
        _data = nullptr;
    }

    /* Uploadable */
    virtual size_t get_upload_cost() override {
        if (_data != nullptr) {
            uint32_t buffer_size = _data->get_data_length();
            size_t tokens = buffer_size / 1024;
            if ((buffer_size % 1024) != 0) {
                tokens += 1;
            }
            return tokens;
        }
        return 0;
    }
    virtual void upload() override;

    static Texture<dims> create_invalid() {
        return Texture<dims>();
    }

    friend class StaticFactory;
    friend class Framebuffer;
};

using Texture1D = Texture<ITexture::Dimensionality::Texture1D>;

using Texture2D = Texture<ITexture::Dimensionality::Texture2D>;

using Texture3D = Texture<ITexture::Dimensionality::Texture3D>;

}  // namespace resatr::opengl

#endif /*RESATR_OPENGL_TEXTURE_HPP*/