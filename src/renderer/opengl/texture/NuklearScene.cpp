#include "Drawable.hpp"
#include "Logger.hpp"
#include "Scene.hpp"

#include <Trace.hpp>
#include <glm/vec2.hpp>
#include <opengl/core/Types.hpp>
#include <opengl/texture/Texture.hpp>
#include <opengl/utils/DrawCommands.hpp>

#include <cstdint>
#include <cstring>
#include <string>

#include <nuklear.h>

namespace resatr::opengl {

static const char* NUKLEAR_VERTEX_SRC =
     "#version 330 core\n"
     "uniform vec2 size;\n"
     "in vec2 vertex;\n"
     "in vec2 vertex_tex;\n"
     "in vec4 vertex_color;\n"
     "out vec2 texCoord;\n"
     "out vec4 vertexColor;\n"
     "void main() {\n"
     "  texCoord = vertex_tex;\n"
     "  vertexColor = vertex_color;\n"
     "  vec2 normalized_vertex = vec2(vertex.x, size.y - vertex.y);\n"
     "  normalized_vertex = (normalized_vertex / size) * 2.0 - 1.0;\n"
     "  gl_Position = vec4(normalized_vertex, 0, 1);\n"
     "}\n";

static const char* NUKLEAR_FRAGMENT_SRC =
     "#version 330 core\n"
     "uniform vec4 bounds;\n"
     "out vec4 fragColor;\n"
     "in vec2 texCoord;\n"
     "in vec4 vertexColor;\n"
     "uniform sampler2D tex;\n"
     "void main(){\n"
     "  // Clip test.\n"
     "  if (any(lessThan(gl_FragCoord.xy, bounds.xy))) discard;\n"
     "  if (any(greaterThan(gl_FragCoord.xy, bounds.zw))) discard;\n"
     "  fragColor = vertexColor * texture(tex, texCoord);\n"
     "}\n";

uint32_t NK_BUFFER_STRIDE =
     opengl_sizeof({{Types::Float, 2}, {Types::Float, 2}, {Types::UnsignedByte, 4}});
uint32_t NK_BUFFER_OFFSET = 0;
uint32_t NK_BUFFER_TEX_OFFSET = opengl_sizeof({{Types::Float, 2}});
uint32_t NK_BUFFER_COLOR_OFFSET = opengl_sizeof({{Types::Float, 2}, {Types::Float, 2}});

class NuklearAtlasLoader final : public TextureData {
   public:
    const void* image;
    int w;
    int h;

    virtual uint32_t get_dimensions() const {
        return 2;
    }

    virtual uint32_t get_size(uint32_t d) const {
        if (d == 0) {
            return w;
        }
        if (d == 1) {
            return h;
        }
        throw std::out_of_range("NuklearAtlasLoader::get_size(" + std::to_string(d) + ")");
    }

    virtual Formats get_format() const {
        return Formats::RGBA;
    }

    virtual Types get_type() const {
        return Types::UnsignedByte;
    }

    virtual const void* get_upload_data() const {
        return image;
    }

    NuklearAtlasLoader() = default;
    ~NuklearAtlasLoader() = default;
};

NuklearOpenGLScene::NuklearOpenGLScene(OpenGLRenderLoop& root, Scene& parent, glm::ivec2 size) :
    OpenGLScene(root, parent, size) {
    // Layer preparation.
    auto ct = root.create_texture(ITexture::InternalFormats::RGBA8, (glm::uvec2) size);
    bind_colors(ct);
    set_clear_color({0.0, 0.0, 0.0, 0.0});
    // Nuklear preparation.
    nk_init_default(&_nk_ctx, nullptr);
    nk_buffer_init_default(&_nk_cmds);
    // OpenGL rendering preparation.
    {
        // Program.
        auto vert = root.create_vertex_shader(NUKLEAR_VERTEX_SRC);
        auto frag = root.create_fragment_shader(NUKLEAR_FRAGMENT_SRC);
        _nk_program = root.create_program(vert, frag);
    }
    auto nk_program_uniforms = _nk_program.get_uniforms();
    auto nk_program_attributes = _nk_program.get_attributes();
    _nk_program_size = nk_program_uniforms["size"];
    _nk_program_size = glm::vec2(_size);
    _nk_program_bounds = nk_program_uniforms["bounds"];
    auto nk_program_tex = nk_program_uniforms["tex"];
    nk_program_tex = 7;
    auto nk_program_vertex = nk_program_attributes["vertex"];
    auto nk_program_vertex_tex = nk_program_attributes["vertex_tex"];
    auto nk_program_vertex_color = nk_program_attributes["vertex_color"];
    // Vertex and index buffers.
    _nk_buffer_bindings = root.create_vertex_attributes();
    _nk_buffer_vertices = root.create_buffer(Buffer::Type::ArrayBuffer);
    _nk_buffer_vertices.set_data(_nk_buffer_loader_vertices);
    _nk_buffer_indexes = root.create_buffer(Buffer::Type::ElementArrayBuffer);
    _nk_buffer_indexes.set_data(_nk_buffer_loader_indexes);
    // Setup buffer to attribute format.
    _nk_buffer_bindings.bind();
    _nk_buffer_vertices.bind();
    _nk_buffer_indexes.bind();
    _nk_buffer_bindings._bind(nk_program_vertex, 2, Types::Float, false, NK_BUFFER_STRIDE,
                              NK_BUFFER_OFFSET);
    _nk_buffer_bindings._bind(nk_program_vertex_tex, 2, Types::Float, false, NK_BUFFER_STRIDE,
                              NK_BUFFER_TEX_OFFSET);
    _nk_buffer_bindings._bind(nk_program_vertex_color, 4, Types::UnsignedByte, true,
                              NK_BUFFER_STRIDE, NK_BUFFER_COLOR_OFFSET);
    _nk_buffer_indexes.unbind();
    _nk_buffer_vertices.unbind();
    _nk_buffer_bindings.unbind();
    // Font texture.
    nk_font_atlas_init_default(&_nk_atlas);
    nk_font_atlas_begin(&_nk_atlas);
    NuklearAtlasLoader atlas_loader;
    atlas_loader.image =
         nk_font_atlas_bake(&_nk_atlas, &atlas_loader.w, &atlas_loader.h, NK_FONT_ATLAS_RGBA32);
    _nk_atlas_texture = root.create_texture<ITexture::Dimensionality::Texture2D>(
         ITexture::InternalFormats::RGBA8, atlas_loader);
    _nk_atlas_texture.set_filtering(ITexture::Filtering::Linear, ITexture::Filtering::Linear);
    nk_font_atlas_end(&_nk_atlas, nk_handle_ptr((void*) (&_nk_atlas_texture)),
                      &_nk_texture_null);
    if (_nk_atlas.default_font != nullptr) {
        nk_style_set_font(&_nk_ctx, &_nk_atlas.default_font->handle);
    }
    _nk_render_config.set_blending(RenderSpec::BlendFuncs::SrcAlpha,
                                   RenderSpec::BlendFuncs::OneMinusSrcAlpha);
    // Last preparation.
    nk_input_begin(&_nk_ctx);
}

NuklearOpenGLScene::~NuklearOpenGLScene() {
    nk_font_atlas_clear(&_nk_atlas);
    nk_buffer_free(&_nk_cmds);
    nk_free(&_nk_ctx);
}

void NuklearOpenGLScene::tick() {
    // Stop processing input. We will be rendering now.
    nk_input_end(&_nk_ctx);
    // Call overloaded function which declares the UI layout and state.
    render(&_nk_ctx);
    // Check and draw only if something changed.
    bool ui_changed = true;
    if (_nk_ctx.memory.allocated == _nk_cmds_last.size() &&
        !std::memcmp(_nk_ctx.memory.memory.ptr, _nk_cmds_last.data(), _nk_cmds_last.size())) {
        ui_changed = false;
    }
    if (ui_changed || _dirty) {
        // Draw contents changed, copy to last.
        _nk_cmds_last.resize(_nk_ctx.memory.allocated);
        std::memcpy(_nk_cmds_last.data(), _nk_ctx.memory.memory.ptr, _nk_cmds_last.size());
        mark_dirty();
    }
    else {
        // Redraw not needed. Prepare for next iteration.
        nk_buffer_clear(&_nk_cmds);
        nk_clear(&_nk_ctx);
        nk_input_begin(&_nk_ctx);
    }
}

bool NuklearOpenGLScene::render() {
    // Calculate buffer data.
    static const nk_draw_vertex_layout_element NK_VERTEX_LAYOUT[] = {
         {NK_VERTEX_POSITION, NK_FORMAT_FLOAT, NK_BUFFER_OFFSET},
         {NK_VERTEX_TEXCOORD, NK_FORMAT_FLOAT, NK_BUFFER_TEX_OFFSET},
         {NK_VERTEX_COLOR, NK_FORMAT_R8G8B8A8, NK_BUFFER_COLOR_OFFSET},
         {NK_VERTEX_LAYOUT_END}};
    nk_convert_config nk_render_cfg;
    nk_render_cfg.shape_AA = NK_ANTI_ALIASING_ON;
    nk_render_cfg.line_AA = NK_ANTI_ALIASING_ON;
    nk_render_cfg.vertex_layout = NK_VERTEX_LAYOUT;
    nk_render_cfg.vertex_size = NK_BUFFER_STRIDE;
    nk_render_cfg.vertex_alignment = 4;
    nk_render_cfg.circle_segment_count = 22;
    nk_render_cfg.curve_segment_count = 22;
    nk_render_cfg.arc_segment_count = 22;
    nk_render_cfg.global_alpha = 1.0f;
    nk_render_cfg.null = _nk_texture_null;
    _nk_buffer_loader_vertices.clear();
    _nk_buffer_loader_indexes.clear();
    nk_convert(&_nk_ctx, &_nk_cmds, _nk_buffer_loader_vertices.get_nk_buffer(),
               _nk_buffer_loader_indexes.get_nk_buffer(), &nk_render_cfg);
    // Upload buffer data.
    _nk_buffer_bindings.bind();
    _nk_buffer_vertices.upload();
    _nk_buffer_indexes.upload();
    // Render.
    clear();
    _nk_buffer_indexes.bind();
    _nk_buffer_vertices.bind();
    _nk_program.bind();
    _nk_render_config.bind();
    _nk_atlas_texture.bind(7);
    const nk_draw_command* cmd;
    const nk_draw_index* offset = NULL;
    nk_draw_foreach(cmd, &_nk_ctx, &_nk_cmds) {
        if (cmd->elem_count != 0) {
            // TODO: multi-texture?
            glm::vec4 clip_rect;
            clip_rect.x = cmd->clip_rect.x;
            clip_rect.y = _size.y - (cmd->clip_rect.y + cmd->clip_rect.h);
            clip_rect.z = clip_rect.x + cmd->clip_rect.w;
            clip_rect.w = clip_rect.y + cmd->clip_rect.h;
            _nk_program_bounds = clip_rect;
            draw_elements(VertexFormats::Triangles, cmd->elem_count, Types::UnsignedShort,
                          (uintptr_t) offset);
            offset += cmd->elem_count;
        }
    }
    _nk_atlas_texture.unbind(7);
    _nk_render_config.unbind();
    _nk_program.unbind();
    _nk_buffer_vertices.unbind();
    _nk_buffer_indexes.unbind();
    _nk_buffer_bindings.unbind();
    // Prepare for next frame.
    nk_buffer_clear(&_nk_cmds);
    nk_clear(&_nk_ctx);
    nk_input_begin(&_nk_ctx);
    return true;
}

void NuklearOpenGLScene::on_size_update(glm::ivec2 old_size, glm::ivec2 new_size) {
    OpenGLScene::on_size_update(old_size, new_size);
    mark_dirty();
    _nk_program_size = glm::vec2(new_size);
}

bool NuklearOpenGLScene::on_keyboard_button_press(const KeyboardButtonPress& event) {
    switch (event.get_key()) {
        case KeyboardButtonPress::Keys::LeftShift:
        case KeyboardButtonPress::Keys::RightShift: {
            mark_dirty();
            nk_input_key(&_nk_ctx, NK_KEY_SHIFT, event.is_pressed());
        } break;
        case KeyboardButtonPress::Keys::LeftControl:
        case KeyboardButtonPress::Keys::RightControl: {
            mark_dirty();
            nk_input_key(&_nk_ctx, NK_KEY_CTRL, event.is_pressed());
        } break;
        case KeyboardButtonPress::Keys::Delete: {
            mark_dirty();
            nk_input_key(&_nk_ctx, NK_KEY_DEL, event.is_pressed());
        } break;
        case KeyboardButtonPress::Keys::Enter: {
            mark_dirty();
            nk_input_key(&_nk_ctx, NK_KEY_ENTER, event.is_pressed());
        } break;
        case KeyboardButtonPress::Keys::Tab: {
            mark_dirty();
            nk_input_key(&_nk_ctx, NK_KEY_TAB, event.is_pressed());
        } break;
        case KeyboardButtonPress::Keys::Backspace: {
            mark_dirty();
            nk_input_key(&_nk_ctx, NK_KEY_BACKSPACE, event.is_pressed());
        } break;
        case KeyboardButtonPress::Keys::Up: {
            mark_dirty();
            nk_input_key(&_nk_ctx, NK_KEY_UP, event.is_pressed());
        } break;
        case KeyboardButtonPress::Keys::Down: {
            mark_dirty();
            nk_input_key(&_nk_ctx, NK_KEY_DOWN, event.is_pressed());
        } break;
        case KeyboardButtonPress::Keys::Left: {
            mark_dirty();
            nk_input_key(&_nk_ctx, NK_KEY_LEFT, event.is_pressed());
        } break;
        case KeyboardButtonPress::Keys::Right: {
            mark_dirty();
            nk_input_key(&_nk_ctx, NK_KEY_RIGHT, event.is_pressed());
        } break;
        case KeyboardButtonPress::Keys::C: {
            if (event.has_control()) {
                mark_dirty();
                nk_input_key(&_nk_ctx, NK_KEY_COPY, event.is_pressed());
            }
        } break;
        case KeyboardButtonPress::Keys::X: {
            if (event.has_control()) {
                mark_dirty();
                nk_input_key(&_nk_ctx, NK_KEY_CUT, event.is_pressed());
            }
        } break;
        case KeyboardButtonPress::Keys::V: {
            if (event.has_control()) {
                mark_dirty();
                nk_input_key(&_nk_ctx, NK_KEY_PASTE, event.is_pressed());
            }
        } break;
        case KeyboardButtonPress::Keys::Home: {
            mark_dirty();
            nk_input_key(&_nk_ctx, NK_KEY_SCROLL_START, event.is_pressed());
        } break;
        case KeyboardButtonPress::Keys::End: {
            mark_dirty();
            nk_input_key(&_nk_ctx, NK_KEY_SCROLL_END, event.is_pressed());
        } break;
        case KeyboardButtonPress::Keys::PageDown: {
            mark_dirty();
            nk_input_key(&_nk_ctx, NK_KEY_SCROLL_DOWN, event.is_pressed());
        } break;
        case KeyboardButtonPress::Keys::PageUp: {
            mark_dirty();
            nk_input_key(&_nk_ctx, NK_KEY_SCROLL_UP, event.is_pressed());
        } break;
        default: {
            /* handled by on_text_input callback */
        } break;
    }
    return false;
}

bool NuklearOpenGLScene::on_mouse_button_press(const MouseButtonPress& event) {
    switch (event.get_key()) {
        case MouseButtonPress::Keys::Left: {
            mark_dirty();
            nk_input_button(&_nk_ctx, NK_BUTTON_LEFT, _last_mouse_pos.x, _last_mouse_pos.y,
                            event.is_pressed());
            if (event.is_pressed()) {
                if (!_in_double_click) {
                    _in_double_click = true;
                    _double_click_timer.start();
                }
                else {
                    _double_click_timer.stop();
                    if (_double_click_timer.get_last_time() <= 600) {
                        // Trigger a double-click.
                        spec::trace("NuklearOpenGLScene::on_mouse_button_press::double_click");
                        mark_dirty();
                        nk_input_button(&_nk_ctx, NK_BUTTON_DOUBLE, _last_mouse_pos.x,
                                        _last_mouse_pos.y, true);
                        mark_dirty();
                        nk_input_button(&_nk_ctx, NK_BUTTON_DOUBLE, _last_mouse_pos.x,
                                        _last_mouse_pos.y, false);
                    }
                    _in_double_click = false;
                }
            }
        } break;
        case MouseButtonPress::Keys::Middle: {
            mark_dirty();
            nk_input_button(&_nk_ctx, NK_BUTTON_MIDDLE, _last_mouse_pos.x, _last_mouse_pos.y,
                            event.is_pressed());
        } break;
        case MouseButtonPress::Keys::Right: {
            mark_dirty();
            nk_input_button(&_nk_ctx, NK_BUTTON_RIGHT, _last_mouse_pos.x, _last_mouse_pos.y,
                            event.is_pressed());
        } break;
        default: {
            /* Scroll is handled below */
        } break;
    }

    if (event.is_scroll()) {
        auto offset = event.get_scroll_vector();
        struct nk_vec2 nk_offset = {offset.x, offset.y};
        mark_dirty();
        nk_input_scroll(&_nk_ctx, nk_offset);
    }
    return false;
}

bool NuklearOpenGLScene::on_mouse_movement(const MouseMovement& event) {
    _last_mouse_pos = event;
    mark_dirty();
    nk_input_motion(&_nk_ctx, event.get_x(), event.get_y());
    return false;
}

bool NuklearOpenGLScene::on_text_input(const char*) {
    // TODO:
    return false;
}

}  // namespace resatr::opengl
