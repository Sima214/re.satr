#include "Framebuffer.hpp"

#include <Trace.hpp>
#include <opengl/StaticFactory.hpp>
#include <opengl/core/Error.hpp>
#include <opengl/texture/Texture.hpp>
#include <opengl/utils/DrawCommands.hpp>
#include <opengl/utils/StateUtils.hpp>

#include <utility>
#include <vector>

#include <glad.h>

namespace resatr::opengl {

Framebuffer::Framebuffer(StaticFactory& factory, glm::ivec2 size, bool colors, bool colors_hdr,
                         bool colors_alpha, bool depth, bool stencil) :
    _factory(&factory) {
    glGenFramebuffers(1, &_uid);
    if (_uid == 0) {
    }
    set_size(size);
    if (colors) {
        auto f =
             colors_hdr ? ITexture::InternalFormats::RGB16F : ITexture::InternalFormats::RGB8UI;
        if (colors_alpha) {
            f = colors_hdr ? ITexture::InternalFormats::RGBA16F :
                             ITexture::InternalFormats::RGBA8UI;
        }
        auto tex = factory.create_texture(f, (glm::uvec2) _size);
        bind_colors(tex);
    }
    if (depth) {
        auto tex = factory.create_texture(ITexture::InternalFormats::DepthComponent32F,
                                          (glm::uvec2) _size);
        bind_depth(tex);
    }
    if (stencil) {
        auto tex = factory.create_texture(ITexture::InternalFormats::StencilIndex8,
                                          (glm::uvec2) _size);
        bind_stencil(tex);
    }
    opengl_error::check("while constructing framebuffer.");
    _draw_spec.set_blending(RenderSpec::BlendFuncs::SrcAlpha,
                            RenderSpec::BlendFuncs::OneMinusSrcAlpha);
}

bool Framebuffer::bind_colors(Texture2D& tex) {
    spec::trace("framebuffer::bind::colors(", _uid, ", ", tex._uid, ")");
    _tex_colors = std::move(tex);
    _tex_colors.set_size(_size);
    _tex_colors.empty();
    _tex_colors.set_filtering(ITexture::Filtering::Nearest, ITexture::Filtering::Nearest);
    _tex_colors.set_wrapping(ITexture::Wrapping::ClampToBorder,
                             ITexture::Wrapping::ClampToBorder);
    {
        SaveFramebuffers old_framebuffers;
        glBindFramebuffer(GL_FRAMEBUFFER, _uid);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                               ITexture::Dimensionality::Texture2D, _tex_colors._uid, 0);
    }
    return ready();
}

bool Framebuffer::bind_depth(Texture2D& tex) {
    spec::trace("framebuffer::bind::depth(", _uid, ", ", tex._uid, ")");
    _tex_depth = std::move(tex);
    _tex_depth.set_size(_size);
    _tex_depth.empty();
    _tex_depth.set_filtering(ITexture::Filtering::Nearest, ITexture::Filtering::Nearest);
    _tex_depth.set_wrapping(ITexture::Wrapping::ClampToBorder,
                            ITexture::Wrapping::ClampToBorder);
    {
        SaveFramebuffers old_framebuffers;
        glBindFramebuffer(GL_FRAMEBUFFER, _uid);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                               ITexture::Dimensionality::Texture2D, _tex_depth._uid, 0);
    }
    return ready();
}

bool Framebuffer::bind_stencil(Texture2D& tex) {
    spec::trace("framebuffer::bind::stencil(", _uid, ", ", tex._uid, ")");
    _tex_stencil = std::move(tex);
    _tex_stencil.set_size(_size);
    _tex_stencil.empty();
    _tex_stencil.set_filtering(ITexture::Filtering::Nearest, ITexture::Filtering::Nearest);
    _tex_stencil.set_wrapping(ITexture::Wrapping::ClampToBorder,
                              ITexture::Wrapping::ClampToBorder);
    {
        SaveFramebuffers old_framebuffers;
        glBindFramebuffer(GL_FRAMEBUFFER, _uid);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT,
                               ITexture::Dimensionality::Texture2D, _tex_stencil._uid, 0);
    }
    return ready();
}

bool Framebuffer::ready() {
    SaveFramebuffers old_framebuffers;
    glBindFramebuffer(GL_FRAMEBUFFER, _uid);
    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    const char* status_str;
    switch (status) {
        case GL_FRAMEBUFFER_COMPLETE: {
            status_str = "Complete";
        } break;
        case GL_FRAMEBUFFER_UNDEFINED: {
            status_str = "Undefined";
        } break;
        case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT: {
            status_str = "IncompleteAttachment";
        } break;
        case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT: {
            status_str = "IncompleteMissingAttachment";
        } break;
        case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER: {
            status_str = "IncompleteDrawBuffer";
        } break;
        case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER: {
            status_str = "IncompleteReadBuffer";
        } break;
        case GL_FRAMEBUFFER_UNSUPPORTED: {
            status_str = "Unsupported";
        } break;
        case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE: {
            status_str = "IncompleteMultisample";
        } break;
        case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS: {
            status_str = "IncompleteLayerTargets";
        } break;
        default: {
            status_str = "";
        } break;
    }
    spec::trace("framebuffer::status(", _uid, ", '", status_str, "')");
    return status == GL_FRAMEBUFFER_COMPLETE;
}

bool Framebuffer::has_colors() {
    return _tex_colors._state != ITexture::State::INVALID;
}
bool Framebuffer::has_depth() {
    return _tex_depth._state != ITexture::State::INVALID;
}
bool Framebuffer::has_stencil() {
    return _tex_stencil._state != ITexture::State::INVALID;
}

void Framebuffer::draw() {
    auto& program = _factory->get_2d_identity();
    auto& bindings = _factory->get_2d_quad_bindings();
    // Bind and activate texture at active slot GL_TEXTURE0.
    _tex_colors.bind(0);
    _draw_spec.bind();
    program.bind();
    bindings.bind();
    draw_arrays(VertexFormats::TriangleStrip, 0, 4);
    bindings.unbind();
    program.unbind();
    _draw_spec.unbind();
    _tex_colors.unbind(0);
}

void Framebuffer::target() {
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, _uid);
}

void Framebuffer::clear() {
    uint32_t op = 0;
    if (_has_clear_color) {
        glClearColor(_clear_color.r, _clear_color.g, _clear_color.b, _clear_color.a);
        op |= GL_COLOR_BUFFER_BIT;
    }
    if (_has_clear_depth) {
        glClearDepth(_clear_depth);
        op |= GL_DEPTH_BUFFER_BIT;
    }
    if (_has_clear_stencil) {
        glClearStencil(_clear_stencil);
        op |= GL_STENCIL_BUFFER_BIT;
    }
    glClear(op);
}

std::vector<resatr::RGBA> Framebuffer::read_color() {
    if (has_colors()) {
        spec::fatal("Unimplemented");
    }
    return std::vector<resatr::RGBA>();
}
std::vector<glm::vec4> Framebuffer::read_hdr() {
    if (has_colors()) {
        spec::fatal("Unimplemented");
    }
    return std::vector<glm::vec4>();
}
std::vector<float> Framebuffer::read_depth() {
    if (has_depth()) {
        spec::fatal("Unimplemented");
    }
    return std::vector<float>();
}
std::vector<uint32_t> Framebuffer::read_stencil() {
    if (has_stencil()) {
        spec::fatal("Unimplemented");
    }
    return std::vector<uint32_t>();
}

void Framebuffer::on_size_update(glm::ivec2, glm::ivec2 dim) {
    if (_tex_colors._state != ITexture::State::INVALID) {
        _tex_colors.set_size(dim);
        _tex_colors.empty();
    }
    if (_tex_depth._state != ITexture::State::INVALID) {
        _tex_depth.set_size(dim);
        _tex_depth.empty();
    }
    if (_tex_stencil._state != ITexture::State::INVALID) {
        _tex_stencil.set_size(dim);
        _tex_stencil.empty();
    }
}

Framebuffer::~Framebuffer() {
    if (_uid != 0) {
        spec::trace("framebuffer::delete(", _uid, ")");
        glDeleteFramebuffers(1, &_uid);
    }
}

}  // namespace resatr::opengl
