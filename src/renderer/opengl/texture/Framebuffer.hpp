#ifndef RESATR_OPENGL_FRAMEBUFFER_HPP
#define RESATR_OPENGL_FRAMEBUFFER_HPP
/**
 * @file
 * @brief Uses textures and framebuffers to
 * implement the Drawable interface in OpenGL.
 */

#include <Drawable.hpp>
#include <Trace.hpp>
#include <glm/vec2.hpp>
#include <opengl/texture/Texture.hpp>
#include <opengl/utils/DrawCommands.hpp>

#include <cstdint>
#include <utility>
#include <vector>

namespace resatr::opengl {

class StaticFactory;

class Framebuffer : public Drawable {
   protected:
    uint32_t _uid;
    Texture2D _tex_colors;
    Texture2D _tex_depth;
    Texture2D _tex_stencil;
    StaticFactory* _factory;
    RenderSpec _draw_spec;

   public:
    /**
     * Invalid framebuffer constructor.
     */
    Framebuffer() : _uid(0), _factory(nullptr) {}

    Framebuffer(Framebuffer&& o) :
        Drawable(std::forward<Drawable>(o)), _uid(std::exchange(o._uid, 0)),
        _tex_colors(std::forward<Texture2D>(o._tex_colors)),
        _tex_depth(std::forward<Texture2D>(o._tex_depth)),
        _tex_stencil(std::forward<Texture2D>(o._tex_stencil)),
        _factory(std::exchange(o._factory, nullptr)), _draw_spec(o._draw_spec) {
        spec::trace("framebuffer::new_move(", _uid, ")");
    }

    Framebuffer& operator=(Framebuffer&& o) {
        Drawable::operator=(std::forward<Drawable>(o));
        _uid = std::exchange(o._uid, 0);
        _tex_colors = std::forward<Texture2D>(o._tex_colors);
        _tex_depth = std::forward<Texture2D>(o._tex_depth);
        _tex_stencil = std::forward<Texture2D>(o._tex_stencil);
        _factory = std::exchange(o._factory, nullptr);
        _draw_spec = o._draw_spec;
        spec::trace("framebuffer::assign_move(", _uid, ")");
        return *this;
    }

    virtual ~Framebuffer();

   protected:
    /**
     * Generates a framebuffer with optional texture attachments.
     *
     * The \p colors texture format is RGB16F by default if \p colors_hdr is true.
     * Otherwise RGB8UI is used as the internal texture format.
     * \p colors_alpha adds the alpha channel to the above commands.
     * The depth's texture format is always float.
     * The stencil's texture format is always unsigned byte.
     */
    Framebuffer(StaticFactory& factory, glm::ivec2 size, bool colors = false,
                bool colors_hdr = true, bool colors_alpha = false, bool depth = false,
                bool stencil = false);

    /**
     * Binds a texture as a color buffer.
     * After this call, this framebuffer owns the texture.
     */
    bool bind_colors(Texture2D& tex);
    /**
     * Binds a texture as a depth buffer.
     * After this call, this framebuffer owns the texture.
     */
    bool bind_depth(Texture2D& tex);
    /**
     * Binds a texture as a stencil buffer.
     * After this call, this framebuffer owns the texture.
     */
    bool bind_stencil(Texture2D& tex);

   public:
    /**
     * Check if the framebuffer is usable.
     */
    bool ready();
    bool has_colors();
    bool has_depth();
    bool has_stencil();

    /* Drawable */
    virtual void draw() override;
    virtual void target() override;

    virtual void clear() override;

    virtual std::vector<resatr::RGBA> read_color() override;
    virtual std::vector<glm::vec4> read_hdr() override;
    virtual std::vector<float> read_depth() override;
    virtual std::vector<uint32_t> read_stencil() override;

   protected:
    virtual void on_size_update(glm::ivec2 old_size, glm::ivec2 new_size) override;

    friend class StaticFactory;
};

}  // namespace resatr::opengl

#endif /*RESATR_FRAMEBUFFER_HPP*/