#ifndef RESATR_OPENGL_LAYER_HPP
#define RESATR_OPENGL_LAYER_HPP
/**
 * @file
 * @brief Renderable framebuffer
 */

#include "Drawable.hpp"

#include <Renderable.hpp>
#include <opengl/texture/Framebuffer.hpp>

namespace resatr::opengl {

class OpenGLLayer : public Framebuffer, public resatr::Renderable {
   public:
    OpenGLLayer() = default;
    virtual ~OpenGLLayer() = default;

    OpenGLLayer(StaticFactory& factory, glm::ivec2 size, bool colors = false,
                bool colors_hdr = true, bool colors_alpha = false, bool depth = false,
                bool stencil = false) :
        Framebuffer(factory, size, colors, colors_hdr, colors_alpha, depth, stencil),
        Renderable() {}

    /* Drawable */
    using Drawable::set_clear_color;
    using Drawable::set_clear_depth;
    using Drawable::set_clear_stencil;

    /* Framebuffer */
    using Framebuffer::clear;
    using Framebuffer::draw;
    using Framebuffer::has_colors;
    using Framebuffer::has_depth;
    using Framebuffer::has_stencil;
    using Framebuffer::ready;
    using Framebuffer::set_size;
    using Framebuffer::target;

    /* Renderable */
    using Renderable::dispatch_render;
    using Renderable::is_dirty;
    using Renderable::mark_dirty;
    using Renderable::unmark_dirty;

   protected:
    /* Framebuffer */
    using Framebuffer::on_size_update;

    /* Renderable */
    using Renderable::render;
    using Renderable::tick;
};

}  // namespace resatr::opengl

#endif /*RESATR_OPENGL_LAYER_HPP*/