#ifndef RESATR_OPENGL_BINDABLE_HPP
#define RESATR_OPENGL_BINDABLE_HPP
/**
 * @file
 * @brief Represents any OpenGL entity with the ability to be bound to the context.
 */

namespace resatr::opengl {

class Bindable {
   public:
    /**
     * Bind to the context.
     */
    virtual void bind() const = 0;

    /**
     * Reverse the action 
     */
    virtual void unbind() const = 0;

    /**
     * Alias for bind.
     */
    void use() {
        bind();
    }

    Bindable() = default;
    virtual ~Bindable() = default;
};

}  // namespace resatr::opengl

#endif /*RESATR_OPENGL_BINDABLE_HPP*/