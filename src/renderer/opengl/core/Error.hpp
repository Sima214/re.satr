#ifndef RESATR_ERROR_HPP
#define RESATR_ERROR_HPP
/**
 * @file
 * @brief An OpenGL command has returned an unexpected value?
 * Immediately throw this for an instant way out (=crash)!
 */

#include <stdexcept>
#include <string>

#include <glad.h>

namespace resatr::opengl {

const inline std::string gl_error_to_string(GLenum error) {
    switch (error) {
        case GL_NO_ERROR: {
            return "GL_NO_ERROR";
        }
        case GL_INVALID_ENUM: {
            return "GL_INVALID_ENUM";
        }
        case GL_INVALID_VALUE: {
            return "GL_INVALID_VALUE";
        }
        case GL_INVALID_OPERATION: {
            return "GL_INVALID_OPERATION";
        }
        case GL_INVALID_FRAMEBUFFER_OPERATION: {
            return "GL_INVALID_FRAMEBUFFER_OPERATION";
        }
        case GL_OUT_OF_MEMORY: {
            return "GL_OUT_OF_MEMORY";
        }
        default: {
            return std::to_string(error);
        }
    }
}

/**
 * Generic runtime error for resatr::opengl.
 *
 * Sometimes the interface allows some nonsensical things,
 * such as trying to access the main framebuffer as an texture.
 * When something like that happens, an interface_error gets thrown.
 */
class interface_error : public std::runtime_error {
   public:
    interface_error(const char* what) : std::runtime_error(what) {}

    interface_error(const std::string& what) : std::runtime_error(what) {}
};

/**
 * glGetError to C++ exception wrapper.
 */
class opengl_error final : public interface_error {
   protected:
    opengl_error(const char* what, GLenum error) :
        interface_error("OpenGL Error(" + gl_error_to_string(error) + "): " + what) {}

    opengl_error(GLenum error) :
        interface_error("OpenGL Error(" + gl_error_to_string(error) + ")") {}

   public:
    opengl_error(const char* what) : opengl_error(what, glGetError()) {}

    opengl_error(const std::string& what) : opengl_error(what.data(), glGetError()) {}

    opengl_error() : opengl_error(glGetError()) {}

    /**
     * Check for OpenGL error and throw an exception
     * if error has occurred.
     */
    static void check() {
        GLenum error = glGetError();
        if (error != GL_NO_ERROR) {
            throw opengl_error(error);
        }
    }
    static void check(const char* what) {
        GLenum error = glGetError();
        if (error != GL_NO_ERROR) {
            throw opengl_error(what, error);
        }
    }
    static void check(const std::string& what) {
        GLenum error = glGetError();
        if (error != GL_NO_ERROR) {
            throw opengl_error(what.data(), error);
        }
    }

    /**
     * Same as check(), but gets disabled in release builds.
     */
    #ifndef NDEBUG
        static void debug() {
            GLenum error = glGetError();
            if (error != GL_NO_ERROR) {
                throw opengl_error(error);
            }
        }
        static void debug(const char* what) {
            GLenum error = glGetError();
            if (error != GL_NO_ERROR) {
                throw opengl_error(what, error);
            }
        }
        static void debug(const std::string& what) {
            GLenum error = glGetError();
            if (error != GL_NO_ERROR) {
                throw opengl_error(what.data(), error);
            }
        }
    #else
        static void debug() {}
        static void debug(const char*) {}
        static void debug(const std::string&) {}
    #endif
};

}  // namespace resatr::opengl

#endif /*RESATR_ERROR_HPP*/
