#ifndef RESATR_OPENGL_TYPES_HPP
#define RESATR_OPENGL_TYPES_HPP
/**
 * @file
 * @brief OpenGL type enums(C++ mapping)
 */

#include <opengl/core/Error.hpp>

#include <initializer_list>
#include <utility>

#include <glad.h>

namespace resatr::opengl {

enum Types {
    Byte = GL_BYTE,
    UnsignedByte = GL_UNSIGNED_BYTE,
    Short = GL_SHORT,
    UnsignedShort = GL_UNSIGNED_SHORT,
    Int = GL_INT,
    UnsignedInt = GL_UNSIGNED_INT,
    HalfFloat = GL_HALF_FLOAT,
    Float = GL_FLOAT,
    Double = GL_DOUBLE,
    UnsignedByte332 = GL_UNSIGNED_BYTE_3_3_2,
    UnsignedByte233Rev = GL_UNSIGNED_BYTE_2_3_3_REV,
    UnsignedShort565 = GL_UNSIGNED_SHORT_5_6_5,
    UnsignedShort565Rev = GL_UNSIGNED_SHORT_5_6_5_REV,
    UnsignedShort4444 = GL_UNSIGNED_SHORT_4_4_4_4,
    UnsignedShort4444Rev = GL_UNSIGNED_SHORT_4_4_4_4_REV,
    UnsignedShort5551 = GL_UNSIGNED_SHORT_5_5_5_1,
    UnsignedShort1555Rev = GL_UNSIGNED_SHORT_1_5_5_5_REV,
    UnsignedInt8888 = GL_UNSIGNED_INT_8_8_8_8,
    UnsignedInt8888Rev = GL_UNSIGNED_INT_8_8_8_8_REV,
    UnsignedInt1010102 = GL_UNSIGNED_INT_10_10_10_2,
    UnsignedInt2101010Rev = GL_UNSIGNED_INT_2_10_10_10_REV,
};

enum Formats {
    Depth = GL_DEPTH,
    Stencil = GL_STENCIL,
    StencilIndex = GL_STENCIL_INDEX,
    DepthComponent = GL_DEPTH_COMPONENT,
    Red = GL_RED,
    Green = GL_GREEN,
    Blue = GL_BLUE,
    Alpha = GL_ALPHA,
    RedGreen = GL_RG,
    RGB = GL_RGB,
    BGR = GL_BGR,
    RGBA = GL_RGBA,
    BGRA = GL_BGRA,
    RedInteger = GL_RED_INTEGER,
    GreenInteger = GL_GREEN_INTEGER,
    BlueInteger = GL_BLUE_INTEGER,
    RedGreenInteger = GL_RG_INTEGER,
    RGBInteger = GL_RGB_INTEGER,
    BGRInteger = GL_BGR_INTEGER,
    RGBAInteger = GL_RGBA_INTEGER,
    BGRAInteger = GL_BGRA_INTEGER,
};

inline constexpr uint32_t opengl_sizeof(Types t, Formats f) {
    uint32_t type_size = 0;
    switch (t) {
        case Byte:
        case UnsignedByte:
        case UnsignedByte332:
        case UnsignedByte233Rev: {
            type_size = 1;
        } break;
        case Short:
        case HalfFloat:
        case UnsignedShort:
        case UnsignedShort565:
        case UnsignedShort565Rev:
        case UnsignedShort4444:
        case UnsignedShort4444Rev:
        case UnsignedShort5551:
        case UnsignedShort1555Rev: {
            type_size = 2;
        } break;
        case Int:
        case Float:
        case UnsignedInt:
        case UnsignedInt8888:
        case UnsignedInt8888Rev:
        case UnsignedInt1010102:
        case UnsignedInt2101010Rev: {
            type_size = 4;
        } break;
        case Double: {
            type_size = 8;
        } break;
        default: {
            #ifndef NDEBUG
                throw interface_error("Invalid OpenGL type enum!");
            #endif
        }
    }
    uint32_t type_components = 1;
    switch (t) {
        case UnsignedByte332:
        case UnsignedByte233Rev:
        case UnsignedShort565:
        case UnsignedShort565Rev: {
            type_components = 3;
        } break;
        case UnsignedShort4444:
        case UnsignedShort4444Rev:
        case UnsignedShort5551:
        case UnsignedShort1555Rev:
        case UnsignedInt8888:
        case UnsignedInt8888Rev:
        case UnsignedInt1010102:
        case UnsignedInt2101010Rev: {
            type_components = 4;
        } break;
        default: {
            /** NOP */
        } break;
    }
    uint32_t format_components = 0;
    switch (f) {
        case Depth:
        case Stencil:
        case StencilIndex:
        case DepthComponent:
        case Red:
        case Green:
        case Blue:
        case Alpha:
        case RedInteger:
        case GreenInteger:
        case BlueInteger: {
            format_components = 1;
        } break;
        case RedGreen:
        case RedGreenInteger: {
            format_components = 2;
        } break;
        case RGB:
        case BGR:
        case RGBInteger:
        case BGRInteger: {
            format_components = 3;
        } break;
        case RGBA:
        case BGRA:
        case RGBAInteger:
        case BGRAInteger: {
            format_components = 4;
        } break;
        default: {
            #ifndef NDEBUG
                throw interface_error("Invalid OpenGL format enum!");
            #endif
        }
    }
    return type_size * format_components / type_components;
}

inline constexpr uint32_t opengl_sizeof(std::initializer_list<std::pair<Types, int>> ltes) {
    // List of Types with Element Sizes.
    uint32_t accum = 0;
    for (auto tes : ltes) {
        Types t = tes.first;
        int element_size = tes.second;
        uint32_t type_size = 0;
        switch (t) {
            case Byte:
            case UnsignedByte:
            case UnsignedByte332:
            case UnsignedByte233Rev: {
                type_size = 1;
            } break;
            case Short:
            case HalfFloat:
            case UnsignedShort:
            case UnsignedShort565:
            case UnsignedShort565Rev:
            case UnsignedShort4444:
            case UnsignedShort4444Rev:
            case UnsignedShort5551:
            case UnsignedShort1555Rev: {
                type_size = 2;
            } break;
            case Int:
            case Float:
            case UnsignedInt:
            case UnsignedInt8888:
            case UnsignedInt8888Rev:
            case UnsignedInt1010102:
            case UnsignedInt2101010Rev: {
                type_size = 4;
            } break;
            case Double: {
                type_size = 8;
            } break;
            default: {
                #ifndef NDEBUG
                    throw interface_error("Invalid OpenGL type enum!");
                #endif
            }
        }
        accum += type_size * element_size;
    }
    return accum;
}

}  // namespace resatr::opengl

#endif /*RESATR_OPENGL_TYPES_HPP*/