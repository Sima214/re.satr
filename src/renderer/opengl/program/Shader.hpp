#ifndef RESATR_OPENGL_SHADER_HPP
#define RESATR_OPENGL_SHADER_HPP
/**
 * @file
 * @brief Collection of all OpenGL shaders.
 */

#include <Loadable.hpp>
#include <Trace.hpp>
#include <Utils.hpp>
#include <opengl/core/Error.hpp>

#include <cstddef>
#include <cstdint>
#include <limits>
#include <stdexcept>
#include <string>
#include <utility>

#include <glad.h>

namespace resatr::opengl {

template<GLenum type> class Shader : public spec::INonCopyable {
   public:
    enum State { INVALID, NO_SOURCE, NOT_COMPILED, COMPILING, COMPILED, COMPILE_FAILED };

   protected:
    uint32_t _uid;
    State _state;

   public:
    void provide_source(const char* src, size_t src_n) {
        if (src_n <= std::numeric_limits<int32_t>::max()) {
            const char* srca[1] = {src};
            if (src_n == 0) {
                provide_source(1, srca, nullptr);
            }
            else {
                int32_t src_na[1] = {(int32_t) src_n};
                provide_source(1, srca, src_na);
            }
        }
        else {
            throw std::range_error("Input string too large!");
        }
    }

    /**
     * Return whether the compilation was a success.
     */
    bool compile() {
        _state = State::COMPILING;
        glCompileShader(_uid);
        int32_t success = 0;
        glGetShaderiv(_uid, GL_COMPILE_STATUS, &success);
        if (success) {
            _state = State::COMPILED;
            return true;
        }
        else {
            _state = State::COMPILE_FAILED;
            return false;
        }
    }

    std::string get_compile_log() {
        int32_t compile_log_size = -1;
        glGetShaderiv(_uid, GL_INFO_LOG_LENGTH, &compile_log_size);
        if (compile_log_size <= 0) {
            return "";
        }
        std::string log(compile_log_size, '\0');
        glGetShaderInfoLog(_uid, log.capacity(), nullptr, &log[0]);
        return log;
    }

    State get_state() {
        return _state;
    }

   protected:
    void provide_source(const int32_t n, const char* src[], const int32_t src_n[]) {
        glShaderSource(_uid, n, src, src_n);
        _state = State::NOT_COMPILED;
    }

    Shader(State state) : _uid(0), _state(state) {}

    Shader() : _uid(glCreateShader(type)), _state(State::NO_SOURCE) {
        spec::trace("shader::new(", type, ", ", _uid, ")");
        if (_uid == 0) {
            throw opengl_error("Error creating shader!");
        }
    }

    Shader(const char* src, size_t src_n) : Shader() {
        provide_source(src, src_n);
    }

    Shader(const std::string& src) : Shader(src.data(), src.size()) {}

   public:
    Shader(Shader&& o) :
        _uid(std::exchange(o._uid, 0)), _state(std::exchange(o._state, State::INVALID)) {
        spec::trace("shader::new_move(", type, ", ", _uid, ")");
    }

    Shader& operator=(Shader&& o) {
        _uid = std::exchange(o._uid, 0);
        _state = std::exchange(o._state, State::INVALID);
        spec::trace("shader::assign_move(", type, ", ", _uid, ")");
        return *this;
    }

    ~Shader() {
        if (_uid != 0) {
            spec::trace("shader::delete(", type, ", ", _uid, ")");
            glDeleteShader(_uid);
        }
    }

    friend class Program;
};

class VertexShader : public Shader<GL_VERTEX_SHADER> {
   protected:
    VertexShader(const std::string& str);
    VertexShader(const char* str);

   public:
    /**
     * Creates an empty shader.
     */
    VertexShader() : Shader(State::INVALID) {}

    friend class StaticFactory;
};

class FragmentShader : public Shader<GL_FRAGMENT_SHADER> {
   protected:
    FragmentShader(const std::string& str);
    FragmentShader(const char* str);

   public:
    /**
     * Creates an empty shader.
     */
    FragmentShader() : Shader(State::INVALID) {}

    friend class StaticFactory;
};

}  // namespace resatr::opengl

#endif /*RESATR_OPENGL_SHADER_HPP*/
