#ifndef RESATR_PROGRAM_HPP
#define RESATR_PROGRAM_HPP
/**
 * @file
 * @brief OpenGL 3.3 program entity and later derivatives.
 */

#include <Trace.hpp>
#include <Uploadable.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>
#include <opengl/core/Bindable.hpp>
#include <opengl/program/Shader.hpp>

#include <cstdint>
#include <string>
#include <utility>

namespace resatr::opengl {

/**
 * Base class supports only static construction.
 */
class Program : public Bindable, public Uploadable {
   public:
    enum State { INVALID, EMPTY, UNLINKED, LINKING, READY, ERRORED };

    /**
     * Thin container object for a vertex attribute.
     */
    class Attribute {
       protected:
        const int32_t _loc;
        const std::string _name;

        Attribute(int32_t location, const char* name) : _loc(location), _name(name) {}

       public:
        int32_t get_location() {
            return _loc;
        }

        const char* get_name() {
            return _name.data();
        }

        friend class Program;
    };

    /**
     * Inspect a linked program's vertex attributes.
     */
    class Attributes {
       protected:
        uint32_t _uid;

        Attributes(uint32_t uid) : _uid(uid) {}

       public:
        template<typename T> Attribute operator[](const T& index_type) {
            return get_attribute(index_type);
        }

        Attribute get_attribute(const std::string& name);
        Attribute get_attribute(const char* name);

        Attribute get_attribute(const int32_t index);

        /**
         * Returns count of active vertex attributes in the program.
         */
        int32_t count();

        friend class Program;
    };

    /**
     * Thin containing referencing a program and a
     * uniform with its location and name.
     */
    class Uniform {
       protected:
        uint32_t _program_uid;
        int32_t _location;
        std::string _name;

        Uniform(uint32_t program_uid, int32_t location, const char* name) :
            _program_uid(program_uid), _location(location), _name(name) {}

       public:
        /**
         * Invalid constructor.
         */
        Uniform() = default;

        template<typename S> Uniform& operator=(S v) {
            set(v);
            return *this;
        }

        void set(float v);
        void set(int32_t v);
        void set(uint32_t v);
        void set(glm::vec2 v);
        void set(glm::ivec2 v);
        void set(glm::uvec2 v);
        void set(glm::vec3 v);
        void set(glm::ivec3 v);
        void set(glm::uvec3 v);
        void set(glm::vec4 v);
        void set(glm::ivec4 v);
        void set(glm::uvec4 v);

        void set(glm::mat4x4 v);

        int32_t get_location() {
            return _location;
        }

        const char* get_name() {
            return _name.data();
        }

        friend class Program;
    };

    class Uniforms {
       protected:
        uint32_t _program_uid;

        Uniforms(uint32_t program_uid) : _program_uid(program_uid) {}

       public:
        template<typename T> Uniform operator[](const T& index_type) {
            return get_uniform(index_type);
        }

        Uniform get_uniform(const std::string& name);
        Uniform get_uniform(const char* name);

        Uniform get_uniform(const int32_t index);

        int32_t count();

        friend class Program;
    };

   protected:
    uint32_t _uid;
    State _state;

   public:
    /**
     * Invalid constructor.
     * Used for placeholder objects
     * before OpenGL initialization.
     */
    Program(State state) : _uid(0), _state(state) {}

    /**
     * Move construct.
     */
    Program(Program&& o) :
        _uid(std::exchange(o._uid, 0)), _state(std::exchange(o._state, State::INVALID)) {
        spec::trace("program::new_move(", _uid, ")");
    }

    Program& operator=(Program&& o) {
        _uid = std::exchange(o._uid, 0);
        _state = std::exchange(o._state, State::INVALID);
        spec::trace("program::assign_move(", _uid, ")");
        return *this;
    }

    virtual ~Program();

   protected:
    /**
     * New empty program constructor.
     */
    Program();

    /**
     * Shader attaching constructor.
     */
    Program(VertexShader& vert, FragmentShader& frag);

    void attach(uint32_t shader_uid);

   public:
    std::string get_link_log();

    State get_state() {
        return _state;
    }

    Attributes get_attributes();

    Uniforms get_uniforms();

    /** Aliases */
    void link() {
        upload();
    }

    /** Bindable */
    virtual void bind() const override;
    virtual void unbind() const override;

    /** Uploadable */
    virtual size_t get_upload_cost() override;
    virtual void upload() override;
    virtual bool ready() override;
    virtual void deallocate() override;

    static Program create_invalid() {
        return Program(State::INVALID);
    }

    friend class StaticFactory;
};

// TODO: class RuntimeProgram :Loadable{};

}  // namespace resatr::opengl

#endif /*RESATR_PROGRAM_HPP*/
