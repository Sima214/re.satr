#include "Program.hpp"

#include <Trace.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <opengl/core/Error.hpp>
#include <opengl/utils/StateUtils.hpp>

#include <string>

#include <glad.h>

namespace resatr::opengl {

Program::Program() : _uid(glCreateProgram()), _state(State::EMPTY) {
    spec::trace("program::new(", _uid, ")");
    if (_uid == 0) {
        throw opengl_error("Error creating program!");
    }
}

Program::~Program() {
    if (_uid != 0) {
        spec::trace("program::delete(", _uid, ")");
        glDeleteProgram(_uid);
    }
}

void Program::attach(uint32_t shader_uid) {
    spec::trace("program::attach(", _uid, ", ", shader_uid, ")");
    glAttachShader(_uid, shader_uid);
    _state = State::UNLINKED;
}

Program::Program(VertexShader& vert, FragmentShader& frag) : Program() {
    attach(vert._uid);
    attach(frag._uid);
}

void Program::bind() const {
    glUseProgram(_uid);
}

void Program::unbind() const {
    glUseProgram(0);
}

size_t Program::get_upload_cost() {
    // Arbitary value, cause compile times can not be calculated.
    return 10000;
}

void Program::upload() {
    // Link
    _state = State::LINKING;
    glLinkProgram(_uid);
    // Detach used shaders.
    int32_t attached_shader_count;
    glGetProgramiv(_uid, GL_ATTACHED_SHADERS, &attached_shader_count);
    uint32_t* attached_shader_uids = new uint32_t[attached_shader_count];
    glGetAttachedShaders(_uid, attached_shader_count, nullptr, attached_shader_uids);
    for (int32_t i = 0; i < attached_shader_count; i++) {
        glDetachShader(_uid, attached_shader_uids[i]);
    }
    delete[] attached_shader_uids;
    // Check link status.
    int32_t link_success = 0;
    glGetProgramiv(_uid, GL_LINK_STATUS, &link_success);
    if (link_success) {
        _state = State::READY;
    }
    else {
        _state = State::ERRORED;
    }
}

std::string Program::get_link_log() {
    int32_t log_length = -1;
    glGetProgramiv(_uid, GL_INFO_LOG_LENGTH, &log_length);
    if (log_length <= 0) {
        return "";
    }
    std::string log(log_length, '\0');
    glGetProgramInfoLog(_uid, log.capacity(), &log_length, &log[0]);
    return log;
}

bool Program::ready() {
    return _state == READY;
}

void Program::deallocate() {
    if (_uid != 0) {
        // Invalidate object.
        spec::trace("program::deallocate(", _uid, ")");
        glDeleteProgram(_uid);
        _uid = 0;
        _state = State::INVALID;
    }
}

Program::Attributes Program::get_attributes() {
    return Program::Attributes(_uid);
}

Program::Attribute Program::Attributes::get_attribute(const std::string& name) {
    return get_attribute(name.data());
}

Program::Attribute Program::Attributes::get_attribute(const char* name) {
    int32_t location = glGetAttribLocation(_uid, name);
    spec::trace("program(", _uid, ")::attribute('", name, "', ", location, ")");
    return Program::Attribute(location, name);
}

Program::Attribute Program::Attributes::get_attribute(const int32_t index) {
    int32_t name_len = 0;
    glGetProgramiv(_uid, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &name_len);
    char* name = new char[name_len];
    int32_t attr_size;
    GLenum attr_type;
    glGetActiveAttrib(_uid, index, name_len, nullptr, &attr_size, &attr_type, name);
    int32_t attr_loc = glGetAttribLocation(_uid, name);
    spec::trace("program(", _uid, ")::attribute(", index, ", '", name, "', ", attr_loc, ", ",
                attr_size, ", ", attr_type, ")");
    // NOTE: Attribute copies c-string in std::string.
    Program::Attribute r(attr_loc, name);
    delete[] name;
    return r;
}

int32_t Program::Attributes::count() {
    int32_t r = 0;
    glGetProgramiv(_uid, GL_ACTIVE_ATTRIBUTES, &r);
    return r;
}

Program::Uniforms Program::get_uniforms() {
    return Program::Uniforms(_uid);
}

Program::Uniform Program::Uniforms::get_uniform(const std::string& name) {
    return get_uniform(name.data());
}

Program::Uniform Program::Uniforms::get_uniform(const char* name) {
    int32_t location = glGetUniformLocation(_program_uid, name);
    spec::trace("program(", _program_uid, ")::uniform('", name, "', ", location, ")");
    return Program::Uniform(_program_uid, location, name);
}

Program::Uniform Program::Uniforms::get_uniform(const int32_t index) {
    int32_t name_len = 0;
    glGetProgramiv(_program_uid, GL_ACTIVE_UNIFORM_MAX_LENGTH, &name_len);
    char* name = new char[name_len];
    glGetActiveUniformName(_program_uid, index, name_len, nullptr, name);
    int32_t location = glGetUniformLocation(_program_uid, name);
    spec::trace("program(", _program_uid, ")::uniform(", index, ", '", name, "', ", location,
                ")");
    Program::Uniform r(_program_uid, location, name);
    delete[] name;
    return r;
}

int32_t Program::Uniforms::count() {
    int32_t r = 0;
    glGetProgramiv(_program_uid, GL_ACTIVE_UNIFORMS, &r);
    return r;
}

void Program::Uniform::set(float v) {
    spec::trace("program(", _program_uid, ")::uniform(", _location, ")::set(", v, ")");
    SwitchProgram binding(_program_uid);
    glUniform1f(_location, v);
    opengl_error::debug();
}
void Program::Uniform::set(int32_t v) {
    spec::trace("program(", _program_uid, ")::uniform(", _location, ")::set(", v, ")");
    SwitchProgram binding(_program_uid);
    glUniform1i(_location, v);
    opengl_error::debug();
}
void Program::Uniform::set(uint32_t v) {
    spec::trace("program(", _program_uid, ")::uniform(", _location, ")::set(", v, ")");
    SwitchProgram binding(_program_uid);
    glUniform1ui(_location, v);
    opengl_error::debug();
}
void Program::Uniform::set(glm::vec2 v) {
    spec::trace("program(", _program_uid, ")::uniform(", _location, ")::set(", v.x, ", ", v.y,
                ")");
    SwitchProgram binding(_program_uid);
    glUniform2f(_location, v.x, v.y);
    opengl_error::debug();
}
void Program::Uniform::set(glm::ivec2 v) {
    spec::trace("program(", _program_uid, ")::uniform(", _location, ")::set(", v.x, ", ", v.y,
                ")");
    SwitchProgram binding(_program_uid);
    glUniform2i(_location, v.x, v.y);
    opengl_error::debug();
}
void Program::Uniform::set(glm::uvec2 v) {
    spec::trace("program(", _program_uid, ")::uniform(", _location, ")::set(", v.x, ", ", v.y,
                ")");
    SwitchProgram binding(_program_uid);
    glUniform2ui(_location, v.x, v.y);
    opengl_error::debug();
}
void Program::Uniform::set(glm::vec3 v) {
    spec::trace("program(", _program_uid, ")::uniform(", _location, ")::set(", v.x, ", ", v.y,
                ", ", v.z, ")");
    SwitchProgram binding(_program_uid);
    glUniform3f(_location, v.x, v.y, v.z);
    opengl_error::debug();
}
void Program::Uniform::set(glm::ivec3 v) {
    spec::trace("program(", _program_uid, ")::uniform(", _location, ")::set(", v.x, ", ", v.y,
                ", ", v.z, ")");
    SwitchProgram binding(_program_uid);
    glUniform3i(_location, v.x, v.y, v.z);
    opengl_error::debug();
}
void Program::Uniform::set(glm::uvec3 v) {
    spec::trace("program(", _program_uid, ")::uniform(", _location, ")::set(", v.x, ", ", v.y,
                ", ", v.z, ")");
    SwitchProgram binding(_program_uid);
    glUniform3ui(_location, v.x, v.y, v.z);
    opengl_error::debug();
}
void Program::Uniform::set(glm::vec4 v) {
    spec::trace("program(", _program_uid, ")::uniform(", _location, ")::set(", v.x, ", ", v.y,
                ", ", v.z, ", ", v.w, ")");
    SwitchProgram binding(_program_uid);
    glUniform4f(_location, v.x, v.y, v.z, v.w);
    opengl_error::debug();
}
void Program::Uniform::set(glm::ivec4 v) {
    spec::trace("program(", _program_uid, ")::uniform(", _location, ")::set(", v.x, ", ", v.y,
                ", ", v.z, ", ", v.w, ")");
    SwitchProgram binding(_program_uid);
    glUniform4i(_location, v.x, v.y, v.z, v.w);
    opengl_error::debug();
}
void Program::Uniform::set(glm::uvec4 v) {
    spec::trace("program(", _program_uid, ")::uniform(", _location, ")::set(", v.x, ", ", v.y,
                ", ", v.z, ", ", v.w, ")");
    SwitchProgram binding(_program_uid);
    glUniform4ui(_location, v.x, v.y, v.z, v.w);
    opengl_error::debug();
}

void Program::Uniform::set(glm::mat4x4 v) {
    spec::trace("program(", _program_uid, ")::uniform(", _location, ")::set(mat4)");
    SwitchProgram binding(_program_uid);
    glUniformMatrix4fv(_location, 1, false, glm::value_ptr(v));
    opengl_error::debug();
}

}  // namespace resatr::opengl
