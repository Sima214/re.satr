#include "Shader.hpp"

#include <string>

resatr::opengl::VertexShader::VertexShader(const std::string& str) : Shader(str) {
    compile();
}
resatr::opengl::VertexShader::VertexShader(const char* str) : Shader(str, 0) {
    compile();
}

resatr::opengl::FragmentShader::FragmentShader(const std::string& str) : Shader(str) {
    compile();
}
resatr::opengl::FragmentShader::FragmentShader(const char* str) : Shader(str, 0) {
    compile();
}
