#include "RenderLoop.hpp"
#include "opengl/utils/StateUtils.hpp"

#include <Attr.hpp>
#include <Logger.hpp>
#include <Trace.hpp>
#include <glfw.hpp>
#include <glm/vec4.hpp>
#include <opengl/core/Error.hpp>
#include <opengl/core/Types.hpp>

#include <cstdint>
#include <functional>
#include <vector>

#include <glad.h>

namespace resatr::opengl {

OpenGLRenderLoop::OpenGLRenderLoop(glfw::Window& context, LoadScheduler& scheduler,
                                   Presenter& presenter) :
    resatr::RenderLoop(context, scheduler, presenter) {
    _context.make_context_current();
    logger.logi("OpenGL version: ", _context.gl_version());
    // Initial setup.
    _presenter.attach();
    caps.scan();
    static_factory_init();
    set_size(_context.get_framebuffer_size());
    // Consume generated events before mainloop.
    glfw::GLFW::get_instance().poll_events();
    RenderLoop::push_events();
}

void OpenGLRenderLoop::present() {
    resatr::RenderLoop::present();
    if ((_presenter.get_current_frame() % 600) == 0) {
        logger.logi(_presenter);
    }
}

void OpenGLRenderLoop::loop() {
    spec::trace("OpenGLRenderLoop::loop::enter");
    auto& glfw = glfw::GLFW::get_instance();
    start_loop_timer();
    while (!_context.should_close()) {
        glfw.poll_events();
        RenderLoop::push_events();
        dispatch_render();
        opengl_error::debug("after OpenGLRenderLoop::render");
        present();
    }
}

void OpenGLRenderLoop::draw() {
    throw interface_error("Cannot use default framebuffer as a texture!");
}
void OpenGLRenderLoop::target() {
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
}

std::vector<resatr::RGBA> OpenGLRenderLoop::read_color() {
    glm::ivec2 size = get_size();
    SaveBuffers old_buffers;
    SaveFramebuffers old_framebuffers;
    glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
    std::vector<resatr::RGBA> buffer(size.x * size.y);
    glReadPixels(0, 0, size.x, size.y, Formats::RGBA, Types::UnsignedByte,
                 static_cast<void*>(&buffer[0]));
    opengl_error::check(" in OpenGLRenderLoop::read_color()");
    return buffer;
}
std::vector<glm::vec4> OpenGLRenderLoop::read_hdr() {
    glm::ivec2 size = get_size();
    SaveBuffers old_buffers;
    SaveFramebuffers old_framebuffers;
    glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
    std::vector<glm::vec4> buffer(size.x * size.y);
    glReadPixels(0, 0, size.x, size.y, Formats::RGBA, Types::Float,
                 static_cast<void*>(&buffer[0]));
    opengl_error::check(" in OpenGLRenderLoop::read_hdr()");
    return buffer;
}
std::vector<float> OpenGLRenderLoop::read_depth() {
    glm::ivec2 size = get_size();
    SaveBuffers old_buffers;
    SaveFramebuffers old_framebuffers;
    glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
    std::vector<float> buffer(size.x * size.y);
    glReadPixels(0, 0, size.x, size.y, Formats::DepthComponent, Types::Float,
                 static_cast<void*>(&buffer[0]));
    opengl_error::check(" in OpenGLRenderLoop::read_depth()");
    return buffer;
}
std::vector<uint32_t> OpenGLRenderLoop::read_stencil() {
    glm::ivec2 size = get_size();
    SaveBuffers old_buffers;
    SaveFramebuffers old_framebuffers;
    glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
    std::vector<uint32_t> buffer(size.x * size.y);
    glReadPixels(0, 0, size.x, size.y, Formats::StencilIndex, Types::UnsignedInt,
                 static_cast<void*>(&buffer[0]));
    opengl_error::check(" in OpenGLRenderLoop::read_color()");
    return buffer;
}

void OpenGLRenderLoop::clear() {
    uint32_t op = 0;
    if (_has_clear_color) {
        glClearColor(_clear_color.r, _clear_color.g, _clear_color.b, _clear_color.a);
        op |= GL_COLOR_BUFFER_BIT;
    }
    if (_has_clear_depth) {
        glClearDepth(_clear_depth);
        op |= GL_DEPTH_BUFFER_BIT;
    }
    if (_has_clear_stencil) {
        glClearStencil(_clear_stencil);
        op |= GL_STENCIL_BUFFER_BIT;
    }
    glClear(op);
}

void OpenGLRenderLoop::on_size_update(glm::ivec2, glm::ivec2 new_size) {
    logger.logd("OpenGLRenderLoop::size(", new_size.x, "x", new_size.y, ")");
    mark_dirty();
    glViewport(0, 0, new_size.x, new_size.y);
}

OpenGLRenderLoop::~OpenGLRenderLoop() {}

}  // namespace resatr::opengl
