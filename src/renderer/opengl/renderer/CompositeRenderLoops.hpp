#ifndef RESATR_OPENGL_COMPOSITERENDERLOOPS_HPP
#define RESATR_OPENGL_COMPOSITERENDERLOOPS_HPP
/**
 * @file
 * @brief Container type RenderLoops.
 */

#include <opengl/renderer/RenderLoop.hpp>
#include <opengl/texture/Layer.hpp>
#include <opengl/texture/Scene.hpp>

namespace resatr::opengl {

class BasicOpenGLRenderLoop final : public OpenGLRenderLoop {
   protected:
    OpenGLScene* _3d_scene = nullptr;
    OpenGLScene* _2d_scene = nullptr;
    OpenGLLayer* _overlay = nullptr;

   public:
    BasicOpenGLRenderLoop(glfw::Window& w, LoadScheduler& s, Presenter& p);

    virtual bool on_keyboard_button_press(const resatr::KeyboardButtonPress& e) override;

    virtual bool on_mouse_button_press(const resatr::MouseButtonPress& e) override;

    virtual bool on_mouse_movement(const resatr::MouseMovement& e) override;

    virtual bool on_mouse_relative_movement(const resatr::MouseMovement& e) override;

    virtual bool on_text_input(const char* str) override;

    OpenGLScene* get_3d_scene();
    OpenGLScene* get_ui();
    OpenGLLayer* get_overlay();

    OpenGLScene* set_3d_scene(OpenGLScene* o);
    OpenGLScene* set_ui(OpenGLScene* o);
    OpenGLLayer* set_overlay(OpenGLLayer* o);

   protected:
    virtual bool render() override;

    virtual void on_size_update(glm::ivec2, glm::ivec2) override;
};

}  // namespace resatr::opengl

#endif /*RESATR_OPENGL_COMPOSITERENDERLOOPS_HPP*/
