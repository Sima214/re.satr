#ifndef RESATR_OPENGL_PRESENTER_HPP
#define RESATR_OPENGL_PRESENTER_HPP
/**
 * @file
 * @brief OpenGL specific presenters.
 */

#include <LoadScheduler.hpp>
#include <Presenter.hpp>
#include <StopWatch.hpp>
#include <glfw.hpp>

#include <cstddef>
#include <ostream>

namespace resatr::opengl {

/**
 * Base OpenGL presenter,
 */
class OpenGLPresenter : public resatr::Presenter {
   protected:
    resatr::LoadScheduler& _loader;

   public:
    OpenGLPresenter(glfw::Window& window, resatr::LoadScheduler& loader);

    virtual void present() override;
    virtual float get_frametime() override;
    virtual resatr::Presenter::Modes get_mode() override;
    virtual void print_stats(std::ostream& o) override;

    OpenGLPresenter() = delete;
    virtual ~OpenGLPresenter() = default;
};

/**
 * Provides no frame rate or upload limiting.
 */
class UnlimitedOpenGLPresenter final : public OpenGLPresenter {
   public:
    UnlimitedOpenGLPresenter(glfw::Window& window, resatr::LoadScheduler& loader);

    virtual void attach() override;
    virtual void present() override;
};

/**
 * Provides vsync frame but no upload rate limiting.
 */
class NoopOpenGLPresenter final : public OpenGLPresenter {
   public:
    NoopOpenGLPresenter(glfw::Window& window, resatr::LoadScheduler& loader);

    virtual void attach() override;
    virtual void present() override;
};

/**
 * Stabilizes framerate using vsync,
 * while also trying to upload
 * assets between frames.
 */
class SyncedOpenGLPresenter final : public OpenGLPresenter {
   protected:
    float _blocked_upload_threshold;
    size_t _loader_idle_threshold;

    spec::StopWatch& _render_timer;
    spec::StopWatch& _blocked_upload_timer;
    size_t _loader_idle_frame_count;

    /**
     * Average time required per upload token in milliseconds.
     */
    float _token_time = 0.001;

   public:
    SyncedOpenGLPresenter(glfw::Window& window, resatr::LoadScheduler& loader,
                          float blocked_upload_threshold = 1.0,
                          size_t loader_idle_threshold = 1000);

    /**
     * Manually sets mode.
     * As a side effect, the internal
     * state of the automode policy
     * also gets reset.
     */
    void set_mode(resatr::Presenter::Modes mode);

    /**
     * \p blocked_upload_threshold : Time in seconds,
     * in which no assets where uploaded,
     * while there are assets in loader queue.
     * After this time has passed this presenter
     * automatically switches to `Loading` mode.
     *
     * \p loader_idle_threshold : Frame count required
     * before automatically switching to `Rendering` mode.
     */
    void set_automode_policy(float blocked_upload_threshold = 1.0,
                             size_t loader_idle_threshold = 1000);

    virtual void attach() override;
    virtual resatr::Presenter::Modes get_mode() override;
    virtual void present() override;
};

}  // namespace resatr::opengl

#endif /*RESATR_OPENGL_PRESENTER_HPP*/