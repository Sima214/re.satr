#ifndef RESATR_OPENGL_RENDERLOOP_HPP
#define RESATR_OPENGL_RENDERLOOP_HPP
/**
 * @file
 * @brief OpenGL main loop.
 */

#include <Color.hpp>
#include <LoadScheduler.hpp>
#include <Presenter.hpp>
#include <RenderLoop.hpp>
#include <glfw.hpp>
#include <glm/vec2.hpp>
#include <glm/vec4.hpp>
#include <opengl/StaticFactory.hpp>
#include <opengl/utils/Capabilities.hpp>

#include <cstddef>
#include <cstdint>
#include <functional>
#include <vector>

namespace resatr::opengl {

class OpenGLRenderLoop : public resatr::RenderLoop, public StaticFactory {
   protected:
    Capabilities caps;

   public:
    OpenGLRenderLoop(glfw::Window& context, LoadScheduler& scheduler, Presenter& presenter);

    /* RenderLoop */
    virtual void present() override;
    virtual void loop() override;

    /* Renderable */
    // virtual void render() = 0;

    /* Drawable */
    virtual void draw() override;
    virtual void target() override;

    using Drawable::set_clear_color;
    using Drawable::set_clear_depth;
    using Drawable::set_clear_stencil;
    virtual void clear() override;

    virtual std::vector<resatr::RGBA> read_color() override;
    virtual std::vector<glm::vec4> read_hdr() override;
    virtual std::vector<float> read_depth() override;
    virtual std::vector<uint32_t> read_stencil() override;

    /* Getters */
    const Capabilities& get_capabilities() const {
        return caps;
    }

    OpenGLRenderLoop() = delete;
    virtual ~OpenGLRenderLoop();

   protected:
    virtual void on_size_update(glm::ivec2 old_size, glm::ivec2 new_size) override;
};

}  // namespace resatr::opengl

#endif /*RESATR_OPENGL_RENDERLOOP_HPP*/
