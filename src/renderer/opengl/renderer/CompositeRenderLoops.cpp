#include "CompositeRenderLoops.hpp"

#include <opengl/texture/Layer.hpp>
#include <opengl/texture/Scene.hpp>

namespace resatr::opengl {

BasicOpenGLRenderLoop::BasicOpenGLRenderLoop(glfw::Window& w, LoadScheduler& s, Presenter& p) :
    OpenGLRenderLoop(w, s, p) {}

bool BasicOpenGLRenderLoop::render() {
    if (_3d_scene != nullptr) {
        _3d_scene->target();
        _3d_scene->dispatch_render();
    }
    if (_2d_scene != nullptr) {
        _2d_scene->target();
        _2d_scene->dispatch_render();
    }
    if (_overlay != nullptr) {
        _overlay->target();
        _overlay->dispatch_render();
    }
    target();
    clear();
    if (_3d_scene != nullptr) {
        _3d_scene->draw();
    }
    if (_2d_scene != nullptr) {
        _2d_scene->draw();
    }
    if (_overlay != nullptr) {
        _overlay->draw();
    }
    // Continuous rendering
    return false;
}

bool BasicOpenGLRenderLoop::on_keyboard_button_press(const resatr::KeyboardButtonPress& e) {
    if (_2d_scene != nullptr) {
        if (_2d_scene->on_keyboard_button_press(e)) {
            return true;
        }
    }
    if (_3d_scene != nullptr) {
        if (_3d_scene->on_keyboard_button_press(e)) {
            return true;
        }
    }
    return false;
}

bool BasicOpenGLRenderLoop::on_mouse_button_press(const resatr::MouseButtonPress& e) {
    if (_2d_scene != nullptr) {
        if (_2d_scene->on_mouse_button_press(e)) {
            return true;
        }
    }
    if (_3d_scene != nullptr) {
        if (_3d_scene->on_mouse_button_press(e)) {
            return true;
        }
    }
    return false;
}

bool BasicOpenGLRenderLoop::on_mouse_movement(const resatr::MouseMovement& e) {
    if (_2d_scene != nullptr) {
        if (_2d_scene->on_mouse_movement(e)) {
            return true;
        }
    }
    if (_3d_scene != nullptr) {
        if (_3d_scene->on_mouse_movement(e)) {
            return true;
        }
    }
    return false;
}

bool BasicOpenGLRenderLoop::on_mouse_relative_movement(const resatr::MouseMovement& e) {
    if (_2d_scene != nullptr) {
        if (_2d_scene->on_mouse_relative_movement(e)) {
            return true;
        }
    }
    if (_3d_scene != nullptr) {
        if (_3d_scene->on_mouse_relative_movement(e)) {
            return true;
        }
    }
    return false;
}

bool BasicOpenGLRenderLoop::on_text_input(const char* str) {
    if (_2d_scene != nullptr) {
        if (_2d_scene->on_text_input(str)) {
            return true;
        }
    }
    if (_3d_scene != nullptr) {
        if (_3d_scene->on_text_input(str)) {
            return true;
        }
    }
    return false;
}

void BasicOpenGLRenderLoop::on_size_update(glm::ivec2 old_size, glm::ivec2 new_size) {
    OpenGLRenderLoop::on_size_update(old_size, new_size);
    if (_3d_scene != nullptr) {
        _3d_scene->set_size(new_size);
        // TODO: _3d_scene->empty
    }
    if (_2d_scene != nullptr) {
        _2d_scene->set_size(new_size);
    }
    if (_overlay != nullptr) {
        _overlay->set_size(new_size);
    }
}

OpenGLScene* BasicOpenGLRenderLoop::get_3d_scene() {
    return _3d_scene;
}
OpenGLScene* BasicOpenGLRenderLoop::get_ui() {
    return _2d_scene;
}
OpenGLLayer* BasicOpenGLRenderLoop::get_overlay() {
    return _overlay;
}

OpenGLScene* BasicOpenGLRenderLoop::set_3d_scene(OpenGLScene* o) {
    OpenGLScene* old = _3d_scene;
    _3d_scene = o;
    return old;
}
OpenGLScene* BasicOpenGLRenderLoop::set_ui(OpenGLScene* o) {
    OpenGLScene* old = _2d_scene;
    _2d_scene = o;
    return old;
}
OpenGLLayer* BasicOpenGLRenderLoop::set_overlay(OpenGLLayer* o) {
    OpenGLLayer* old = _overlay;
    _overlay = o;
    return old;
}

}  // namespace resatr::opengl
