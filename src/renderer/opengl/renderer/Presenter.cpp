#include "Presenter.hpp"

#include <LoadScheduler.hpp>
#include <Presenter.hpp>
#include <glfw.hpp>

#include <ostream>

namespace resatr::opengl {

OpenGLPresenter::OpenGLPresenter(glfw::Window& window, resatr::LoadScheduler& loader) :
    resatr::Presenter(window), _loader(loader) {}

void OpenGLPresenter::present() {
    _window.swap_buffers();
    _present_timer.stop();
    // Start new frame.
    resatr::Presenter::present();
    _present_timer.start();
}

float OpenGLPresenter::get_frametime() {
    return _present_timer.get_last_time();
}

resatr::Presenter::Modes OpenGLPresenter::get_mode() {
    return resatr::Presenter::Modes::Rendering;
}

void OpenGLPresenter::print_stats(std::ostream& o) {
    float avg_fps = _present_timer.get_count() / (_present_timer.get_total_time() / 1000.0f);
    float min_fps = 1.0f / (_present_timer.get_max_time() / 1000.0f);
    float max_fps = 1.0f / (_present_timer.get_min_time() / 1000.0f);
    o << "fps[avg:" << avg_fps << "|min:" << min_fps << "|max:" << max_fps << "]";
    _present_timer.reset();
}

UnlimitedOpenGLPresenter::UnlimitedOpenGLPresenter(glfw::Window& window,
                                                   resatr::LoadScheduler& loader) :
    OpenGLPresenter(window, loader){};

void UnlimitedOpenGLPresenter::attach() {
    OpenGLPresenter::attach();
    _window.disable_vsync();
}

void UnlimitedOpenGLPresenter::present() {
    _loader.upload();
    OpenGLPresenter::present();
}

NoopOpenGLPresenter::NoopOpenGLPresenter(glfw::Window& window, resatr::LoadScheduler& loader) :
    OpenGLPresenter(window, loader){};

void NoopOpenGLPresenter::attach() {
    OpenGLPresenter::attach();
    _window.enable_vsync();
}

void NoopOpenGLPresenter::present() {
    _loader.upload();
    OpenGLPresenter::present();
}

}  // namespace resatr::opengl