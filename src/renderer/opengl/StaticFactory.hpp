#ifndef RESATR_OPENGL_STATICFACTORY_HPP
#define RESATR_OPENGL_STATICFACTORY_HPP
/**
 * @file
 * @brief Provides factory methods
 * for creating OpenGL entities
 * using data stored inside the executable.
 */

#include <Logger.hpp>
#include <glm/gtc/vec1.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <opengl/buffer/Buffer.hpp>
#include <opengl/buffer/VAO.hpp>
#include <opengl/program/Program.hpp>
#include <opengl/program/Shader.hpp>
#include <opengl/texture/Framebuffer.hpp>
#include <opengl/texture/Texture.hpp>

#include <cstdint>
#include <string>

namespace resatr::opengl {

class StaticFactory {
   protected:
    /**
     * Vertex shader which does not apply any transformation on 2D data.
     */
    VertexShader _identity_vertex_2d = VertexShader();
    /**
     * Vertex shader which does not apply any transformation on 2D data.
     */
    VertexShader _identity_vertex_3d = VertexShader();
    /**
     * Fragment texture passthrought shader.
     */
    FragmentShader _identity_fragment = FragmentShader();
    Program _identity_2d = Program(Program::State::INVALID);
    Program _identity_3d = Program(Program::State::INVALID);
    /**
     * Buffer with 2D vertices which draw a quad if rendered with TriangleStrip.
     */
    Buffer _quad_buffer_2d = Buffer(Buffer::State::INVALID);
    VertexAttributeObject _quad_bindings_2d = VertexAttributeObject::create_invalid();
    /**
     * Buffer with 3D vertices which draw a quad if rendered with TriangleStrip.
     */
    Buffer _quad_buffer_3d = Buffer(Buffer::State::INVALID);
    VertexAttributeObject _quad_bindings_3d = VertexAttributeObject::create_invalid();

   public:
    VertexShader create_vertex_shader(const char* str);
    VertexShader create_vertex_shader(const std::string& str);

    FragmentShader create_fragment_shader(const char* str);
    FragmentShader create_fragment_shader(const std::string& str);

    Program create_program(VertexShader& vert, FragmentShader& frag);

    Buffer create_buffer(Buffer::Type type);

    template<typename T, size_t N> Buffer create_array_draw_buffer(const T (&vec_array)[N]) {
        StaticBufferData data(vec_array);
        Buffer r(Buffer::Type::ArrayBuffer, data);
        r.upload();
        r.clear_data();
        return r;
    }

    VertexAttributeObject create_vertex_attributes();

    template<ITexture::Dimensionality dims> Texture<dims> create_texture(ITexture::InternalFormats format, const TextureData& data);

    Texture1D create_texture(ITexture::InternalFormats format, uint32_t size);
    Texture1D create_texture(ITexture::InternalFormats format, glm::uvec1 size);
    Texture2D create_texture(ITexture::InternalFormats format, glm::uvec2 size);
    Texture3D create_texture(ITexture::InternalFormats format, glm::uvec3 size);

    Framebuffer create_framebuffer(glm::ivec2 size = {16, 16}, bool colors = false,
                                   bool colors_hdr = true, bool colors_alpha = false,
                                   bool depth = false, bool stencil = false);

    /* Preloaded resource getters */
    Program& get_2d_identity() {
        return _identity_2d;
    }
    Program& get_3d_identity() {
        return _identity_3d;
    }
    Buffer& get_2d_quad_buffer() {
        return _quad_buffer_2d;
    }
    Buffer& get_3d_quad_buffer() {
        return _quad_buffer_3d;
    }
    VertexAttributeObject& get_2d_quad_bindings() {
        return _quad_bindings_2d;
    }
    VertexAttributeObject& get_3d_quad_bindings() {
        return _quad_bindings_3d;
    }

   protected:
    void static_factory_init();
};

}  // namespace resatr::opengl

#endif /*RESATR_OPENGL_STATICFACTORY_HPP*/
