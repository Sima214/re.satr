#ifndef RESATR_OPENGL_CAPABILITIES_HPP
#define RESATR_OPENGL_CAPABILITIES_HPP
/**
 * @file
 * @brief OpenGL extension parsing.
 */

#include <Utils.hpp>

#include <bitset>
#include <functional>
#include <ostream>
#include <string>
#include <unordered_set>

namespace resatr::opengl {

/**
 * Represents a single OpenGL extension.
 */
class Extension {
   public:
    enum Vendors {
        UNKNOWN,
        _3DFX,
        _3DL,
        AMD,
        ANDROID,
        ANGLE,
        APPLE,
        ARB,
        ARM,
        ATI,
        DMP,
        EXT,
        FJ,
        GREMEDY,
        HP,
        I3D,
        IBM,
        IGLOO,
        IMG,
        INGR,
        INTEL,
        KHR,
        MESA,
        MESAX,
        NV,
        NVX,
        OES,
        OML,
        OVR,
        PGI,
        QCOM,
        REND,
        S3,
        SGI,
        SGIS,
        SGIX,
        SUN,
        SUNX,
        VIV,
        WIN,
        MAX_VENDORS
    };

    static Vendors str2vendor(const std::string& str);

    static const char* vendor2str(Vendors v);

   protected:
    std::bitset<MAX_VENDORS> _vendors;
    std::string _name;

   public:
    /**
     * Splits the extension using regex.
     */
    Extension(const std::string& full_ext);

    /**
     * Comparison.
     */
    bool operator==(const Extension& o) const {
        return get_name() == o.get_name();
    }

    /**
     * Vendor concatenation.
     */
    friend Extension operator+(const Extension& a, const Extension& b) {
        Extension r(Vendors::UNKNOWN, a._name);
        r._vendors = a._vendors | b._vendors;
        return r;
    }

    /**
     * Getters.
     */
    bool has_vendor(Vendors v) const {
        return _vendors[v];
    }
    std::string get_name() const {
        return _name;
    }

    /**
     * Dynamic getter.
     */
    std::string to_string();

    /**
     * Constructs a placeholder
     * Extension for searching.
     */
    static Extension make_search_object(const std::string& search_name) {
        return Extension(Vendors::UNKNOWN, search_name);
    }

   protected:
    Extension(Vendors vendor, const std::string& name);
};

inline std::ostream& operator<<(std::ostream& o, Extension e) {
    o << e.to_string();
    return o;
}

}  // namespace resatr::opengl

namespace std {

template<> struct hash<resatr::opengl::Extension> {
    size_t operator()(const resatr::opengl::Extension& ext) const {
        return std::hash<std::string>{}(ext.get_name());
    }
};

}  // namespace std

namespace resatr::opengl {

class Capabilities : public spec::INonCopyable {
   protected:
    std::unordered_set<Extension> _registry;

   public:
    /**
     * Scans and registers extensions.
     */
    void scan();

    const Extension* get_extension(const Extension& token) const;

    const Extension* get_extension(const std::string& name) const;

    bool has_extension(const Extension& token) const {
        return get_extension(token) != nullptr;
    }

    bool has_extension(const std::string& name) const {
        return get_extension(name) != nullptr;
    }

    Capabilities() = default;
    ~Capabilities() = default;
};

}  // namespace resatr::opengl

#endif /*RESATR_OPENGL_CAPABILITIES_HPP*/
