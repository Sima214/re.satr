#include "StateUtils.hpp"

#include <opengl/buffer/Buffer.hpp>

#include <glad.h>

namespace resatr::opengl {

SaveBuffers::SaveBuffers() {
    glGetIntegerv(GL_ARRAY_BUFFER_BINDING, &_array_buffer);
    glGetIntegerv(GL_ELEMENT_ARRAY_BUFFER_BINDING, &_element_array_buffer);
    glGetIntegerv(GL_PIXEL_PACK_BUFFER_BINDING, &_pixel_pack_buffer);
    glGetIntegerv(GL_PIXEL_UNPACK_BUFFER_BINDING, &_pixel_unpack_buffer);
    glGetIntegerv(GL_TRANSFORM_FEEDBACK_BUFFER_BINDING, &_transform_feedback_buffer);
    glGetIntegerv(GL_UNIFORM_BUFFER_BINDING, &_uniform_buffer);
    // If I haven't misread the documentation, this should unbind all buffers.
    glBindBuffer(Buffer::Type::ArrayBuffer, 0);
}

SaveBuffers::~SaveBuffers() {
    if (_array_buffer != 0) {
        glBindBuffer(Buffer::Type::ArrayBuffer, _array_buffer);
    }
    if (_element_array_buffer != 0) {
        glBindBuffer(Buffer::Type::ElementArrayBuffer, _element_array_buffer);
    }
    if (_pixel_pack_buffer != 0) {
        glBindBuffer(Buffer::Type::PixelPackBuffer, _pixel_pack_buffer);
    }
    if (_pixel_unpack_buffer != 0) {
        glBindBuffer(Buffer::Type::PixelUnpackBuffer, _pixel_unpack_buffer);
    }
    if (_transform_feedback_buffer != 0) {
        glBindBuffer(Buffer::Type::TransformFeedbackBuffer, _transform_feedback_buffer);
    }
    if (_uniform_buffer != 0) {
        glBindBuffer(Buffer::Type::UniformBuffer, _uniform_buffer);
    }
}

SaveFramebuffers::SaveFramebuffers() {
    glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &_draw_framebuffer);
    glGetIntegerv(GL_READ_FRAMEBUFFER_BINDING, &_read_framebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

SaveFramebuffers::~SaveFramebuffers() {
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, _draw_framebuffer);
    glBindFramebuffer(GL_READ_FRAMEBUFFER, _read_framebuffer);
}

SwitchProgram::SwitchProgram() {
    glGetIntegerv(GL_CURRENT_PROGRAM, &_program_uid);
    glUseProgram(0);
}

SwitchProgram::SwitchProgram(uint32_t switch_program_uid) {
    int32_t target_program_uid = switch_program_uid;
    glGetIntegerv(GL_CURRENT_PROGRAM, &_program_uid);
    if (_program_uid != target_program_uid) {
        // Do not switch program if the already bound program is what we want.
        glUseProgram(target_program_uid);
    }
}

SwitchProgram::~SwitchProgram() {
    glUseProgram(_program_uid);
}

}  // namespace resatr::opengl
