#ifndef RESATR_OPENGL_STATEUTILS_HPP
#define RESATR_OPENGL_STATEUTILS_HPP
/**
 * @file
 * @brief Provides a range of RAII style interfaces
 * for managing OpenGL bindings.
 */

#include <Utils.hpp>

#include <cstdint>
#include <tuple>

namespace resatr::opengl {

template<typename... BindableTypes> class ScopedBind : public spec::INonCopyable {
   private:
    std::tuple<BindableTypes&...> _bindables;

   public:
    explicit ScopedBind(BindableTypes&... bindables) : _bindables(std::tie(bindables...)) {
        (bindables.bind(), ...);
    }

    ~ScopedBind() {
        // TODO: reverse?
        std::apply([](auto&... x) { (..., x.unbind()); }, _bindables);
    }
};

class SaveBuffers : public spec::INonCopyable {
   protected:
    int32_t _array_buffer;
    int32_t _element_array_buffer;
    int32_t _pixel_pack_buffer;
    int32_t _pixel_unpack_buffer;
    int32_t _transform_feedback_buffer;
    int32_t _uniform_buffer;

   public:
    SaveBuffers();
    ~SaveBuffers();
};

class SaveFramebuffers : public spec::INonCopyable {
   protected:
    int32_t _draw_framebuffer;
    int32_t _read_framebuffer;

   public:
    SaveFramebuffers();
    ~SaveFramebuffers();
};

/**
 * Temporarily switches to another OpenGL program.
 */
class SwitchProgram : public spec::INonCopyable {
   protected:
    int32_t _program_uid;

   public:
    SwitchProgram();
    SwitchProgram(uint32_t switch_program_uid);
    ~SwitchProgram();
};

}  // namespace resatr::opengl

#endif /*RESATR_OPENGL_STATEUTILS_HPP*/
