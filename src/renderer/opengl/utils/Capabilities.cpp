#include "Capabilities.hpp"

#include <Logger.hpp>
#include <Trace.hpp>

#include <cstdint>
#include <limits>
#include <regex>
#include <string>

#include <glad.h>

static const std::basic_regex GL_EXT_SPLITTER("GL_([^_]+)_(.+)");

namespace resatr::opengl {

Extension::Vendors Extension::str2vendor(const std::string& str) {
    if (str == "3DFX") {
        return Vendors::_3DFX;
    }
    if (str == "3DL") {
        return Vendors::_3DL;
    }
    if (str == "AMD") {
        return Vendors::AMD;
    }
    if (str == "ANDROID") {
        return Vendors::ANDROID;
    }
    if (str == "ANGLE") {
        return Vendors::ANGLE;
    }
    if (str == "APPLE") {
        return Vendors::APPLE;
    }
    if (str == "ARB") {
        return Vendors::ARB;
    }
    if (str == "ARM") {
        return Vendors::ARM;
    }
    if (str == "ATI") {
        return Vendors::ATI;
    }
    if (str == "DMP") {
        return Vendors::DMP;
    }
    if (str == "EXT") {
        return Vendors::EXT;
    }
    if (str == "FJ") {
        return Vendors::FJ;
    }
    if (str == "GREMEDY") {
        return Vendors::GREMEDY;
    }
    if (str == "HP") {
        return Vendors::HP;
    }
    if (str == "I3D") {
        return Vendors::I3D;
    }
    if (str == "IBM") {
        return Vendors::IBM;
    }
    if (str == "IGLOO") {
        return Vendors::IGLOO;
    }
    if (str == "IMG") {
        return Vendors::IMG;
    }
    if (str == "INGR") {
        return Vendors::INGR;
    }
    if (str == "INTEL") {
        return Vendors::INTEL;
    }
    if (str == "KHR") {
        return Vendors::KHR;
    }
    if (str == "MESA") {
        return Vendors::MESA;
    }
    if (str == "MESAX") {
        return Vendors::MESAX;
    }
    if (str == "NV") {
        return Vendors::NV;
    }
    if (str == "NVX") {
        return Vendors::NVX;
    }
    if (str == "OES") {
        return Vendors::OES;
    }
    if (str == "OML") {
        return Vendors::OML;
    }
    if (str == "OVR") {
        return Vendors::OVR;
    }
    if (str == "PGI") {
        return Vendors::PGI;
    }
    if (str == "QCOM") {
        return Vendors::QCOM;
    }
    if (str == "REND") {
        return Vendors::REND;
    }
    if (str == "S3") {
        return Vendors::S3;
    }
    if (str == "SGI") {
        return Vendors::SGI;
    }
    if (str == "SGIS") {
        return Vendors::SGIS;
    }
    if (str == "SGIX") {
        return Vendors::SGIX;
    }
    if (str == "SUN") {
        return Vendors::SUN;
    }
    if (str == "SUNX") {
        return Vendors::SUNX;
    }
    if (str == "VIV") {
        return Vendors::VIV;
    }
    if (str == "WIN") {
        return Vendors::WIN;
    }
    return Vendors::UNKNOWN;
}

const char* Extension::vendor2str(Vendors v) {
    if (v == Vendors::_3DFX) {
        return "3DFX";
    }
    if (v == Vendors::_3DL) {
        return "3DL";
    }
    if (v == Vendors::AMD) {
        return "AMD";
    }
    if (v == Vendors::ANDROID) {
        return "ANDROID";
    }
    if (v == Vendors::ANGLE) {
        return "ANGLE";
    }
    if (v == Vendors::APPLE) {
        return "APPLE";
    }
    if (v == Vendors::ARB) {
        return "ARB";
    }
    if (v == Vendors::ARM) {
        return "ARM";
    }
    if (v == Vendors::ATI) {
        return "ATI";
    }
    if (v == Vendors::DMP) {
        return "DMP";
    }
    if (v == Vendors::EXT) {
        return "EXT";
    }
    if (v == Vendors::FJ) {
        return "FJ";
    }
    if (v == Vendors::GREMEDY) {
        return "GREMEDY";
    }
    if (v == Vendors::HP) {
        return "HP";
    }
    if (v == Vendors::I3D) {
        return "I3D";
    }
    if (v == Vendors::IBM) {
        return "IBM";
    }
    if (v == Vendors::IGLOO) {
        return "IGLOO";
    }
    if (v == Vendors::IMG) {
        return "IMG";
    }
    if (v == Vendors::INGR) {
        return "INGR";
    }
    if (v == Vendors::INTEL) {
        return "INTEL";
    }
    if (v == Vendors::KHR) {
        return "KHR";
    }
    if (v == Vendors::MESA) {
        return "MESA";
    }
    if (v == Vendors::MESAX) {
        return "MESAX";
    }
    if (v == Vendors::NV) {
        return "NV";
    }
    if (v == Vendors::NVX) {
        return "NVX";
    }
    if (v == Vendors::OES) {
        return "OES";
    }
    if (v == Vendors::OML) {
        return "OML";
    }
    if (v == Vendors::OVR) {
        return "OVR";
    }
    if (v == Vendors::PGI) {
        return "PGI";
    }
    if (v == Vendors::QCOM) {
        return "QCOM";
    }
    if (v == Vendors::REND) {
        return "REND";
    }
    if (v == Vendors::S3) {
        return "S3";
    }
    if (v == Vendors::SGI) {
        return "SGI";
    }
    if (v == Vendors::SGIS) {
        return "SGIS";
    }
    if (v == Vendors::SGIX) {
        return "SGIX";
    }
    if (v == Vendors::SUN) {
        return "SUN";
    }
    if (v == Vendors::SUNX) {
        return "SUNX";
    }
    if (v == Vendors::VIV) {
        return "VIV";
    }
    if (v == Vendors::WIN) {
        return "WIN";
    }
    return "";
}

Extension::Extension(Vendors vendor, const std::string& name) : _vendors(), _name(name) {
    if (vendor != Vendors::UNKNOWN) {
        _vendors[vendor] = true;
    }
}

Extension::Extension(const std::string& full_ext) {
    // Split full extension name using regex.
    std::smatch full_ext_match;
    if (std::regex_search(full_ext, full_ext_match, GL_EXT_SPLITTER)) {
        auto vendor = str2vendor(full_ext_match[1].str());
        _vendors[vendor] = true;
        _name = full_ext_match[2].str();
    }
    else {
        spec::fatal("Could not split OpenGL extension: `", full_ext, "`!");
    }
}

std::string Extension::to_string() {
    const std::string PREFIX("GL_");
    std::string vendors("{");
    for (int i = Vendors::UNKNOWN + 1; i < Vendors::MAX_VENDORS; i++) {
        if (_vendors[i]) {
            vendors.append(vendor2str((Vendors) i));
            vendors += ',';
        }
    }
    vendors[vendors.size() - 1] = '}';
    return PREFIX + vendors + "_" + _name;
}

void Capabilities::scan() {
    logger.logi("Parsing extensions...");
    int32_t n;
    glGetIntegerv(GL_NUM_EXTENSIONS, &n);
    for (int32_t i = 0; i < n; i++) {
        std::string full_ext = reinterpret_cast<const char*>(glGetStringi(GL_EXTENSIONS, i));
        Extension ext_new(full_ext);
        if (ext_new.has_vendor(Extension::Vendors::UNKNOWN)) {
            spec::trace("Unrecognized vendor in ", full_ext);
        }
        else {
            auto ext_old = _registry.find(ext_new);
            if (ext_old == _registry.end()) {
                // Simple case: insert new extension.
                _registry.insert(ext_new);
            }
            else {
                // Append case: combine vendors.
                auto ext_comb = *ext_old + ext_new;
                _registry.erase(ext_old);
                _registry.insert(ext_comb);
            }
        }
    }
    for (auto& v : _registry) {
        logger.logd("Parsed extension: ", v);
    }
    logger.logi("Found ", _registry.size(), " extensions!");
}

const Extension* Capabilities::get_extension(const Extension& token) const {
    auto pos = _registry.find(token);
    if (pos != _registry.end()) {
        return &(*pos);
    }
    return nullptr;
}

const Extension* Capabilities::get_extension(const std::string& name) const {
    Extension token = Extension::make_search_object(name);
    return get_extension(token);
}

}  // namespace resatr::opengl
