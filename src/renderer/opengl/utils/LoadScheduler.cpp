#include "LoadScheduler.hpp"

#include <Loadable.hpp>
#include <Logger.hpp>
#include <Uploadable.hpp>
#include <opengl/core/Error.hpp>

#include <pthread.h>

namespace resatr::opengl {

OffLoadScheduler::OffLoadScheduler() :
    _queue(), _upload_queues(), _thread(&OffLoadScheduler::worker, this) {}

OffLoadScheduler::~OffLoadScheduler() {
    // Send exit request to worker.
    bool exit = true;
    _queue.push_back(exit);
    _thread.join();
}

void OffLoadScheduler::append(Loadable& l, Uploadable& u) {
    token_t tk(l, u);
    // Pre-process event.
    l.attach(*static_cast<resatr::LoadScheduler*>(this), l, u);
    _queue.push_back(tk);
}

float OffLoadScheduler::get_load_progress() {
    // No progress tracking.
    return 1.0;
};

size_t OffLoadScheduler::get_upload_queue_length() {
    std::list<upload_token_t>& q = _upload_queues.get_back();
    size_t r = q.size();
    _upload_queues.release(q);
    return r;
}

size_t OffLoadScheduler::get_upload_queue_cost() {
    size_t r = 0;
    std::list<upload_token_t>& q = _upload_queues.get_back();
    for (upload_token_t& c : q) {
        r += c.uploadable.get_upload_cost();
    }
    _upload_queues.release(q);
    return r;
}

size_t OffLoadScheduler::upload(size_t available_tokens) {
    size_t consumed_tokens = 0;
    std::list<upload_token_t>& q = _upload_queues.swap();
    for (upload_token_t& c : q) {
        // Preparations.
        if (!c.upload_ready) {
            c.loadable.prepare_upload(*static_cast<resatr::LoadScheduler*>(this), c.loadable,
                                      c.uploadable);
            c.upload_ready = true;
        }
        size_t current_cost = c.uploadable.get_upload_cost();
        if (available_tokens >= current_cost) {
            // Upload and consume tokens.
            c.uploadable.upload();
            consumed_tokens += current_cost;
            available_tokens -= current_cost;
            c.loadable.complete_upload(*static_cast<resatr::LoadScheduler*>(this), c.loadable,
                                       c.uploadable);
        }
        else {
            // Move everything that won't be uploaded back in queue.
            std::list<upload_token_t>& back_queue = _upload_queues.get_back();
            back_queue.push_back(c);
            _upload_queues.release(back_queue);
        }
    }
    // At the end clear current front buffer.
    q.clear();
    _upload_queues.release(q);
    return consumed_tokens;
}

void OffLoadScheduler::worker() {
    pthread_setname_np(pthread_self(), "OGLOffLoader");
    logger.logi("Worker thread start!");
    while (true) {
        event_t ev;
        size_t evn = _queue.pop_front<event_t>(ev);
        if (evn == sizeof(bool) && ev.exit) {
            // Stop request received.
            break;
        }
        else {
            // Process token event.
            token_t tk = ev.process;
            Loadable::Status tks = tk.loadable.load();
            if (tks.yield && tks.destroy) {
                throw interface_error(
                     "Loadable::load() return status requests both yield and destroy!");
            }
            if (tks.yield) {
                _queue.push_back(tk);
            }
            if (tks.upload) {
                upload_token_t utk(tk);
                std::list<upload_token_t>& q = _upload_queues.get_back();
                q.push_back(utk);
                _upload_queues.release(q);
            }
            if (tks.destroy) {
                tk.loadable.destroy();
            }
        }
    }
    logger.logi("Worker thread exit!");
}

}  // namespace resatr::opengl
