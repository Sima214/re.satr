#ifndef RESATR_OPENGL_DRAWCOMMANDS_HPP
#define RESATR_OPENGL_DRAWCOMMANDS_HPP
/**
 * @file
 * @brief Interface to glDraw* commands.
 * May use different methods based on the available extensions.
 */

#include <opengl/core/Bindable.hpp>
#include <opengl/core/Types.hpp>

#include <cstddef>
#include <cstdint>

#include <glad.h>

namespace resatr::opengl {

enum VertexFormats {
    Points = GL_POINTS,
    Lines = GL_LINES,
    LineLoop = GL_LINE_LOOP,
    LineStrip = GL_LINE_STRIP,
    Triangles = GL_TRIANGLES,
    TriangleStrip = GL_TRIANGLE_STRIP,
    TriangleFan = GL_TRIANGLE_FAN,
};

/**
 * Draw a single object.
 */
void draw_arrays(VertexFormats shape, int32_t start_offset, int32_t vertex_count);
/**
 * Draw a single indexed object.
 */
void draw_elements(VertexFormats shape, int32_t vertex_count, Types index_type,
                   uintptr_t index_offset);

class RenderSpec final : public Bindable {
   public:
    enum BlendEquations {
        Add = GL_FUNC_ADD,
        Subtract = GL_FUNC_SUBTRACT,
        ReverseSubtract = GL_FUNC_REVERSE_SUBTRACT,
        Min = GL_MIN,
        Max = GL_MAX,
    };
    enum BlendFuncs {
        Zero = GL_ZERO,
        One = GL_ONE,
        SrcColor = GL_SRC_COLOR,
        OneMinusSrcColor = GL_ONE_MINUS_SRC_COLOR,
        DstColor = GL_DST_COLOR,
        OneMinusDstColor = GL_ONE_MINUS_DST_COLOR,
        SrcAlpha = GL_SRC_ALPHA,
        OneMinusSrcAlpha = GL_ONE_MINUS_SRC_ALPHA,
        DstAlpha = GL_DST_ALPHA,
        OneMinusDstAlpha = GL_ONE_MINUS_DST_ALPHA,
        ConstantColor = GL_CONSTANT_COLOR,
        OneMinusConstant_color = GL_ONE_MINUS_CONSTANT_COLOR,
        ConstantAlpha = GL_CONSTANT_ALPHA,
        OneMinusConstantAlpha = GL_ONE_MINUS_CONSTANT_ALPHA,
        SrcAlphaSaturate = GL_SRC_ALPHA_SATURATE,
        Src1Color = GL_SRC1_COLOR,
        OneMinusSrc1Color = GL_ONE_MINUS_SRC1_COLOR,
        Src1Alpha = GL_SRC1_ALPHA,
        OneMinusSrc1Alpha = GL_ONE_MINUS_SRC1_ALPHA
    };

    enum CullFace {
        Front = GL_FRONT,
        Back = GL_BACK,
        FrontBack = GL_FRONT_AND_BACK,
    };
    enum CullFrontFace { ClockWise = GL_CW, CounterClockWise = GL_CCW };

    enum DepthFunc {
        Never = GL_NEVER,
        Less = GL_LESS,
        Equal = GL_EQUAL,
        LessEqual = GL_LEQUAL,
        Greater = GL_GREATER,
        NotEqual = GL_NOTEQUAL,
        GreaterEqual = GL_GEQUAL,
        Always = GL_ALWAYS
    };

   protected:
    bool _enable_blending = false;
    BlendEquations _blending_equation = BlendEquations::Add;
    BlendFuncs _blending_source_factor = BlendFuncs::One;
    BlendFuncs _blending_dest_factor = BlendFuncs::Zero;
    BlendFuncs _blending_alpha_source_factor = BlendFuncs::One;
    BlendFuncs _blending_alpha_dest_factor = BlendFuncs::Zero;

    bool _enable_culling = false;
    CullFace _culling_face = CullFace::Back;
    CullFrontFace _culling_front_face = CullFrontFace::CounterClockWise;

    bool _enable_depth = false;
    bool _depth_mask = true;
    DepthFunc _depth_func = DepthFunc::Less;
    double _depth_near = 0;
    double _depth_far = 1.0;

   public:
    RenderSpec(RenderSpec&) = default;
    RenderSpec& operator=(RenderSpec&) = default;
    RenderSpec() = default;

    void set_blending(BlendEquations eq);
    void set_blending(BlendFuncs src, BlendFuncs dst);
    void set_blending(BlendEquations eq, BlendFuncs src, BlendFuncs dst);
    void set_blending(BlendFuncs color_src, BlendFuncs color_dst, BlendFuncs alpha_src,
                      BlendFuncs alpha_dst);
    void set_blending(BlendEquations eq, BlendFuncs color_src, BlendFuncs color_dst,
                      BlendFuncs alpha_src, BlendFuncs alpha_dst);
    void set_culling(CullFace f = CullFace::Back,
                     CullFrontFace o = CullFrontFace::CounterClockWise);

    void set_depth_test(double near = 0.0, double far = 1.0, DepthFunc f = DepthFunc::Less,
                        bool mask = true);

    /* Bindable */
    virtual void bind() const;
    virtual void unbind() const;
};

}  // namespace resatr::opengl

#endif /*RESATR_OPENGL_DRAWCOMMANDS_HPP*/