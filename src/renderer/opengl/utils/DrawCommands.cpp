#include "DrawCommands.hpp"

#include <opengl/core/Bindable.hpp>

#include <glad.h>

namespace resatr::opengl {

void draw_arrays(VertexFormats shape, int32_t start_offset, int32_t vertex_count) {
    glDrawArrays(shape, start_offset, vertex_count);
}

void draw_elements(VertexFormats shape, int32_t vertex_count, Types index_type,
                   uintptr_t index_offset) {
    glDrawElements(shape, vertex_count, index_type, (void*) index_offset);
}

void RenderSpec::set_blending(BlendEquations eq) {
    set_blending(eq, BlendFuncs::One, BlendFuncs::Zero, BlendFuncs::One, BlendFuncs::Zero);
}

void RenderSpec::set_blending(BlendFuncs src, BlendFuncs dst) {
    set_blending(BlendEquations::Add, src, dst, src, dst);
}

void RenderSpec::set_blending(BlendEquations eq, BlendFuncs src, BlendFuncs dst) {
    set_blending(eq, src, dst, src, dst);
}

void RenderSpec::set_blending(BlendFuncs color_src, BlendFuncs color_dst, BlendFuncs alpha_src,
                              BlendFuncs alpha_dst) {
    set_blending(BlendEquations::Add, color_src, color_dst, alpha_src, alpha_dst);
}

void RenderSpec::set_blending(BlendEquations eq, BlendFuncs color_src, BlendFuncs color_dst,
                              BlendFuncs alpha_src, BlendFuncs alpha_dst) {
    _enable_blending = true;
    _blending_equation = eq;
    _blending_source_factor = color_src;
    _blending_dest_factor = color_dst;
    _blending_alpha_source_factor = alpha_src;
    _blending_alpha_dest_factor = alpha_dst;
}

void RenderSpec::set_culling(CullFace f, CullFrontFace o) {
    _enable_culling = true;
    _culling_face = f;
    _culling_front_face = o;
}

void RenderSpec::set_depth_test(double near, double far, DepthFunc f, bool m) {
    _enable_depth = true;
    _depth_near = near;
    _depth_far = far;
    _depth_func = f;
    _depth_mask = m;
}

void RenderSpec::bind() const {
    if (_enable_blending) {
        glEnable(GL_BLEND);
        glBlendEquation(_blending_equation);
        glBlendFuncSeparate(_blending_source_factor, _blending_dest_factor,
                            _blending_alpha_source_factor, _blending_alpha_dest_factor);
    }
    if (_enable_culling) {
        glEnable(GL_CULL_FACE);
        glCullFace(_culling_face);
        glFrontFace(_culling_front_face);
    }
    if (_enable_depth) {
        glEnable(GL_DEPTH_TEST);
        glDepthMask(_depth_mask);
        glDepthFunc(_depth_func);
        glDepthRange(_depth_near, _depth_far);
    }
}

void RenderSpec::unbind() const {
    // Restore state to (OpenGL) default.
    if (_enable_blending) {
        glDisable(GL_BLEND);
        glBlendEquation(BlendEquations::Add);
        glBlendFuncSeparate(BlendFuncs::One, BlendFuncs::Zero, BlendFuncs::One,
                            BlendFuncs::Zero);
    }
    if (_enable_culling) {
        glDisable(GL_CULL_FACE);
        glCullFace(CullFace::Back);
        glFrontFace(CullFrontFace::CounterClockWise);
    }
    if (_enable_depth) {
        glEnable(GL_DEPTH_TEST);
        glDepthMask(true);
        glDepthFunc(DepthFunc::Less);
        glDepthRange(0.0, 1.0);
    }
}

}  // namespace resatr::opengl
