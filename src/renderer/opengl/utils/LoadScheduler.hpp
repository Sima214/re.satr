#ifndef RESATR_OGL_LOADSCHEDULER_HPP
#define RESATR_OGL_LOADSCHEDULER_HPP
/**
 * @file
 * @brief LoadScheduler specific to OpenGL.
 */

#include <DoubleBuffer.hpp>
#include <LoadScheduler.hpp>
#include <Loadable.hpp>
#include <SyncQueue.hpp>
#include <Uploadable.hpp>

#include <list>
#include <thread>

namespace resatr::opengl {

/**
 * Offload non-OpenGL operations to a single seperate thread.
 */
class OffLoadScheduler final : public resatr::LoadScheduler {
   protected:
    /** Internal storage type. */
    struct token_t {
        Loadable& loadable;
        Uploadable& uploadable;

        token_t(Loadable& l, Uploadable& u) : loadable(l), uploadable(u) {}
    };

    struct upload_token_t : public token_t {
        bool upload_ready = false;

        upload_token_t(token_t& o) : token_t(o.loadable, o.uploadable) {}
    };

    /** Collection of possible worker thread events. */
    union event_t {
        bool exit;
        token_t process;

        event_t(){};
    };

    static_assert(sizeof(((event_t*) nullptr)->exit) != sizeof(((event_t*) nullptr)->process),
                  "Cannot distinguish between sub-types of event_t!");
    /** Worker thread's queue. */
    spec::SyncQueue _queue;
    /** Queue for upload operations. */
    SyncedDoubleBuffer<std::list<upload_token_t>> _upload_queues;

    /** Worker thread. */
    std::thread _thread;

   public:
    OffLoadScheduler();

    ~OffLoadScheduler();

    virtual void append(Loadable&, Uploadable&) override;

    virtual float get_load_progress() override;

    virtual size_t get_upload_queue_length() override;

    virtual size_t get_upload_queue_cost() override;

    virtual size_t upload(
         size_t available_tokens = std::numeric_limits<size_t>::max()) override;

   protected:
    /**
     * Main function for the worker thread.
     */
    void worker();
};

}  // namespace resatr::opengl

#endif /*RESATR_OGL_LOADSCHEDULER_HPP*/
