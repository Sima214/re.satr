#include "StaticFactory.hpp"

namespace resatr::opengl {

VertexShader StaticFactory::create_vertex_shader(const char* str) {
    VertexShader r(str);
    if (r.get_state() == VertexShader::State::COMPILE_FAILED) {
        logger.logf(r.get_compile_log());
    }
    return r;
}
VertexShader StaticFactory::create_vertex_shader(const std::string& str) {
    VertexShader r(str);
    if (r.get_state() == VertexShader::State::COMPILE_FAILED) {
        logger.logf(r.get_compile_log());
    }
    return r;
}

FragmentShader StaticFactory::create_fragment_shader(const char* str) {
    FragmentShader r(str);
    if (r.get_state() == FragmentShader::State::COMPILE_FAILED) {
        logger.logf(r.get_compile_log());
    }
    return r;
}
FragmentShader StaticFactory::create_fragment_shader(const std::string& str) {
    FragmentShader r(str);
    if (r.get_state() == FragmentShader::State::COMPILE_FAILED) {
        logger.logf(r.get_compile_log());
    }
    return r;
}

Program StaticFactory::create_program(VertexShader& vert, FragmentShader& frag) {
    Program r(vert, frag);
    do {
        r.link();
    } while (r.get_state() == Program::State::LINKING);
    if (r.get_state() == Program::State::ERRORED) {
        logger.logf(r.get_link_log());
    }
    return r;
}

Buffer StaticFactory::create_buffer(Buffer::Type type) {
    Buffer r(type);
    return r;
}

VertexAttributeObject StaticFactory::create_vertex_attributes() {
    VertexAttributeObject r;
    return r;
}

template<>
Texture1D StaticFactory::create_texture<ITexture::Dimensionality::Texture1D>(
     ITexture::InternalFormats format, const TextureData& data) {
    Texture1D tex(format, data);
    tex.upload();
    tex.clear_data();
    return tex;
}

template<>
Texture2D StaticFactory::create_texture<ITexture::Dimensionality::Texture2D>(
     ITexture::InternalFormats format, const TextureData& data) {
    Texture2D tex(format, data);
    tex.upload();
    tex.clear_data();
    return tex;
}

template<>
Texture3D StaticFactory::create_texture<ITexture::Dimensionality::Texture3D>(
     ITexture::InternalFormats format, const TextureData& data) {
    Texture3D tex(format, data);
    tex.upload();
    tex.clear_data();
    return tex;
}

Texture1D StaticFactory::create_texture(ITexture::InternalFormats format, uint32_t size) {
    return create_texture(format, glm::uvec1(size));
}
Texture1D StaticFactory::create_texture(ITexture::InternalFormats format, glm::uvec1 size) {
    Texture1D tex(format, size);
    tex.empty();
    return tex;
}
Texture2D StaticFactory::create_texture(ITexture::InternalFormats format, glm::uvec2 size) {
    Texture2D tex(format, size);
    tex.empty();
    return tex;
}
Texture3D StaticFactory::create_texture(ITexture::InternalFormats format, glm::uvec3 size) {
    Texture3D tex(format, size);
    tex.empty();
    return tex;
}

Framebuffer StaticFactory::create_framebuffer(glm::ivec2 size, bool colors, bool colors_hdr,
                                              bool colors_alpha, bool depth, bool stencil) {
    Framebuffer r(*this, size, colors, colors_hdr, colors_alpha, depth, stencil);
    return r;
}

static const constexpr char* IDENTITY2D_VERTEX_SRC =
     "#version 330\n"
     "in vec2 vertex;\n"
     "out vec2 texCoord;\n"
     "void main() {\n"
     "  texCoord = (vertex.xy + 1.0f) / 2.0f;\n"
     "  gl_Position = vec4(vertex, 0.0f, 1.0f);\n"
     "}\n";
static const constexpr char* IDENTITY3D_VERTEX_SRC = "#version 330\n"
                                                     "in vec3 vertex;\n"
                                                     "out vec2 texCoord;\n"
                                                     "void main() {\n"
                                                     "  texCoord = (vertex.xy + 1.0f) / 2.0f;\n"
                                                     "  gl_Position = vec4(vertex, 1.0f);\n"
                                                     "}\n";

static const constexpr char* IDENTITY_COLOR_FRAGMENT_SRC =
     "#version 330 core\n"
     "out vec4 fragColor;\n"
     "in vec2 texCoord;\n"
     "uniform sampler2D tex;\n"
     "void main() {\n"
     "    fragColor = texture(tex, texCoord);\n"
     "}\n";

static const constexpr glm::vec2 QUAD2D[] = {
     {1.0f, 1.0f}, {1.0f, -1.0f}, {-1.0f, 1.0f}, {-1.0f, -1.0f}};
static const constexpr glm::vec3 QUAD3D[] = {
     {1.0f, 1.0f, 0.0f}, {1.0f, -1.0f, 0.0f}, {-1.0f, 1.0f, 0.0f}, {-1.0f, -1.0f, 0.0f}};

void StaticFactory::static_factory_init() {
    _identity_vertex_2d = create_vertex_shader(IDENTITY2D_VERTEX_SRC);
    _identity_vertex_3d = create_vertex_shader(IDENTITY3D_VERTEX_SRC);
    _identity_fragment = create_fragment_shader(IDENTITY_COLOR_FRAGMENT_SRC);
    _identity_2d = create_program(_identity_vertex_2d, _identity_fragment);
    _identity_3d = create_program(_identity_vertex_3d, _identity_fragment);
    _quad_buffer_2d = create_array_draw_buffer(QUAD2D);
    _quad_bindings_2d = create_vertex_attributes();
    auto _identity_2d_vertex = _identity_2d.get_attributes()["vertex"];
    _quad_bindings_2d.bind(_identity_2d_vertex, _quad_buffer_2d, 2, Types::Float);
    _quad_buffer_3d = create_array_draw_buffer(QUAD3D);
    auto _identity_3d_vertex = _identity_3d.get_attributes()["vertex"];
    _quad_bindings_3d = create_vertex_attributes();
    _quad_bindings_3d.bind(_identity_3d_vertex, _quad_buffer_3d, 3, Types::Float);
}

}  // namespace resatr::opengl
