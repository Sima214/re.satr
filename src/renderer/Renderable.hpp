#ifndef RESATR_RENDERABLE_HPP
#define RESATR_RENDERABLE_HPP
/**
 * @file
 * @brief A renderable target.
 * Extension of Drawable with the ability to be self-modified.
 * Another way to think of them is as a `Layer`.
 */

#include <Utils.hpp>

namespace resatr {

class Renderable : public virtual spec::INonCopyable {
   protected:
    bool _dirty = true;

    /**
     * Update Drawable's contents.
     *
     * Return true to stop rendering until this is marked dirty again.
     */
    virtual bool render() = 0;

    /**
     * Update Drawable's state.
     *
     * This is called right before render() independently if this is marked dirty.
     * Use this to process UI, or check if the scene has been updated.
     */
    virtual void tick() {}

   public:
    /**
     * Update Drawable's contents if needed.
     */
    void dispatch_render() {
        tick();
        if (is_dirty() && render()) {
            unmark_dirty();
        }
    }

    void unmark_dirty() {
        _dirty = false;
    }

    void mark_dirty() {
        _dirty = true;
    }

    bool is_dirty() {
        return _dirty;
    }

    Renderable(Renderable&&) = default;
    Renderable& operator=(Renderable&&) = default;
    Renderable() = default;
    virtual ~Renderable() = default;
};

}  // namespace resatr

#endif /*RESATR_RENDERABLE_HPP*/
