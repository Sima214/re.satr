#ifndef RESATR_UPLOADABLE_HPP
#define RESATR_UPLOADABLE_HPP
/**
 * @file
 * @brief Represents an OpenGL entity which can upload data to the context.
 */

#include <cstddef>

namespace resatr {

class Uploadable {
   public:
    /**
     * Calculates the upload time
     * in token slots that uploading
     * this entity would require.
     *
     * One token is by convention equal
     * to 1 KiB of texture upload
     * and is always rounded up.
     */
    virtual size_t get_upload_cost() = 0;

    /**
     * Upload data to the context.
     *
     * This data is usually either provided
     * at the object constructor or from a loader.
     *
     * After this function return,
     * then the data should be freed
     * if dynamically allocated.
     */
    virtual void upload() = 0;

    /**
     * Returns true if data has been uploaded
     * at least once(or even partially) to the context.
     */
    virtual bool ready() = 0;

    /**
     * Free data that has been
     * uploaded to the context.
     *
     * If no data has been uploaded,
     * this function call has no effect.
     */
    virtual void deallocate() = 0;

    Uploadable() = default;
    virtual ~Uploadable() = default;
};

}  // namespace resatr

#endif /*RESATR_UPLOADABLE_HPP*/
