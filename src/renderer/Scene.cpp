#include "Scene.hpp"

namespace resatr {

const char* KeyboardButtonPress::to_string(Keys k) {
    switch (k) {
        case Keys::Space: return "Space";
        case Keys::Apostrophe: return "Apostrophe";
        case Keys::Comma: return "Comma";
        case Keys::Minus: return "Minus";
        case Keys::Period: return "Period";
        case Keys::Slash: return "Slash";
        case Keys::_0: return "0";
        case Keys::_1: return "1";
        case Keys::_2: return "2";
        case Keys::_3: return "3";
        case Keys::_4: return "4";
        case Keys::_5: return "5";
        case Keys::_6: return "6";
        case Keys::_7: return "7";
        case Keys::_8: return "8";
        case Keys::_9: return "9";
        case Keys::Semicolon: return "Semicolon";
        case Keys::Equal: return "Equal";
        case Keys::A: return "A";
        case Keys::B: return "B";
        case Keys::C: return "C";
        case Keys::D: return "D";
        case Keys::E: return "E";
        case Keys::F: return "F";
        case Keys::G: return "G";
        case Keys::H: return "H";
        case Keys::I: return "I";
        case Keys::J: return "J";
        case Keys::K: return "K";
        case Keys::L: return "L";
        case Keys::M: return "M";
        case Keys::N: return "N";
        case Keys::O: return "O";
        case Keys::P: return "P";
        case Keys::Q: return "Q";
        case Keys::R: return "R";
        case Keys::S: return "S";
        case Keys::T: return "T";
        case Keys::U: return "U";
        case Keys::V: return "V";
        case Keys::W: return "W";
        case Keys::X: return "X";
        case Keys::Y: return "Y";
        case Keys::Z: return "Z";
        case Keys::LeftBracket: return "LeftBracket";
        case Keys::Backslash: return "Backslash";
        case Keys::RightBracket: return "RightBracket";
        case Keys::GraveAccent: return "GraveAccent";
        case Keys::World1: return "World1";
        case Keys::World2: return "World2";
        case Keys::Escape: return "Escape";
        case Keys::Enter: return "Enter";
        case Keys::Tab: return "Tab";
        case Keys::Backspace: return "Backspace";
        case Keys::Insert: return "Insert";
        case Keys::Delete: return "Delete";
        case Keys::Right: return "Right";
        case Keys::Left: return "Left";
        case Keys::Down: return "Down";
        case Keys::Up: return "Up";
        case Keys::PageUp: return "PageUp";
        case Keys::PageDown: return "PageDown";
        case Keys::Home: return "Home";
        case Keys::End: return "End";
        case Keys::CapsLock: return "CapsLock";
        case Keys::ScrollLock: return "ScrollLock";
        case Keys::NumLock: return "NumLock";
        case Keys::PrintScreen: return "PrintScreen";
        case Keys::Pause: return "Pause";
        case Keys::F1: return "F1";
        case Keys::F2: return "F2";
        case Keys::F3: return "F3";
        case Keys::F4: return "F4";
        case Keys::F5: return "F5";
        case Keys::F6: return "F6";
        case Keys::F7: return "F7";
        case Keys::F8: return "F8";
        case Keys::F9: return "F9";
        case Keys::F10: return "F10";
        case Keys::F11: return "F11";
        case Keys::F12: return "F12";
        case Keys::F13: return "F13";
        case Keys::F14: return "F14";
        case Keys::F15: return "F15";
        case Keys::F16: return "F16";
        case Keys::F17: return "F17";
        case Keys::F18: return "F18";
        case Keys::F19: return "F19";
        case Keys::F20: return "F20";
        case Keys::F21: return "F21";
        case Keys::F22: return "F22";
        case Keys::F23: return "F23";
        case Keys::F24: return "F24";
        case Keys::F25: return "F25";
        case Keys::KP0: return "KP0";
        case Keys::KP1: return "KP1";
        case Keys::KP2: return "KP2";
        case Keys::KP3: return "KP3";
        case Keys::KP4: return "KP4";
        case Keys::KP5: return "KP5";
        case Keys::KP6: return "KP6";
        case Keys::KP7: return "KP7";
        case Keys::KP8: return "KP8";
        case Keys::KP9: return "KP9";
        case Keys::KPDecimal: return "KPDecimal";
        case Keys::KPDivide: return "KPDivide";
        case Keys::KPMultiply: return "KPMultiply";
        case Keys::KPSubtract: return "KPSubtract";
        case Keys::KPAdd: return "KPAdd";
        case Keys::KPEnter: return "KPEnter";
        case Keys::KPEqual: return "KPEqual";
        case Keys::LeftShift: return "LeftShift";
        case Keys::LeftControl: return "LeftControl";
        case Keys::LeftAlt: return "LeftAlt";
        case Keys::LeftSuper: return "LeftSuper";
        case Keys::RightShift: return "RightShift";
        case Keys::RightControl: return "RightControl";
        case Keys::RightAlt: return "RightAlt";
        case Keys::RightSuper: return "RightSuper";
        case Keys::Menu: return "Menu";
        default: return "";
    }
}

KeyboardButtonPress::KeyboardButtonPress(int key, int action, int mods) :
    _key((Keys) key), _pressed(action == GLFW_PRESS), _shift(MASK_TEST(mods, GLFW_MOD_SHIFT)),
    _control(MASK_TEST(mods, GLFW_MOD_CONTROL)), _alt(MASK_TEST(mods, GLFW_MOD_ALT)),
    _super(MASK_TEST(mods, GLFW_MOD_SUPER)), _caps_lock(MASK_TEST(mods, GLFW_MOD_CAPS_LOCK)),
    _num_lock(MASK_TEST(mods, GLFW_MOD_NUM_LOCK)) {}

std::ostream& operator<<(std::ostream& o, const KeyboardButtonPress& v) {
    o << "'" << KeyboardButtonPress::to_string(v.get_key()) << "':";
    o << (v.is_pressed() ? 'P' : 'R');
    if (v.has_shift()) {
        o << 'S';
    }
    if (v.has_control()) {
        o << 'C';
    }
    if (v.has_alt()) {
        o << 'A';
    }
    if (v.has_super()) {
        // OS key.
        o << 'O';
    }
    if (v.has_caps_lock()) {
        o << 'L';
    }
    if (v.has_num_lock()) {
        o << 'N';
    }
    return o;
}

const char* MouseButtonPress::to_string(MouseButtonPress::Keys k) {
    switch (k) {
        case Left: return "L";
        case Right: return "R";
        case Middle: return "M";
        case AUX1: return "A1";
        case AUX2: return "A2";
        case AUX3: return "A3";
        case AUX4: return "A4";
        case AUX5: return "A5";
        case SCROLL_UP: return "SU";
        case SCROLL_DOWN: return "SD";
        case SCROLL_LEFT: return "SL";
        case SCROLL_RIGHT: return "SR";
        default: return "";
    }
}

MouseButtonPress::MouseButtonPress(int key, int action, int mods) :
    _key((Keys) key), _pressed(action == GLFW_PRESS), _shift(MASK_TEST(mods, GLFW_MOD_SHIFT)),
    _control(MASK_TEST(mods, GLFW_MOD_CONTROL)), _alt(MASK_TEST(mods, GLFW_MOD_ALT)),
    _super(MASK_TEST(mods, GLFW_MOD_SUPER)), _caps_lock(MASK_TEST(mods, GLFW_MOD_CAPS_LOCK)),
    _num_lock(MASK_TEST(mods, GLFW_MOD_NUM_LOCK)), _offset(0) {}

/**
 * Scroll wheel button constructor.
 */
MouseButtonPress::MouseButtonPress(Keys key, double offset) :
    _key(key), _pressed(true), _shift(false), _control(false), _alt(false), _super(false),
    _caps_lock(false), _num_lock(false), _offset(offset) {}

bool MouseButtonPress::is_scroll() const {
    return _key == Keys::SCROLL_UP || _key == Keys::SCROLL_DOWN || _key == Keys::SCROLL_LEFT ||
           _key == Keys::SCROLL_RIGHT;
}

glm::vec2 MouseButtonPress::get_scroll_vector() const {
    glm::vec2 v(0.0, 0.0);
    if (is_scroll()) {
        switch (_key) {
            case SCROLL_UP: {
                v.y = _offset;
            } break;
            case SCROLL_DOWN: {
                v.y = -_offset;
            } break;
            case SCROLL_LEFT: {
                v.x = -_offset;
            } break;
            case SCROLL_RIGHT: {
                v.x = _offset;
            } break;
            default: break;
        }
    }
    return v;
}

std::ostream& operator<<(std::ostream& o, const MouseButtonPress& v) {
    o << MouseButtonPress::to_string(v.get_key()) << ":";
    if (v.is_scroll()) {
        o << v.get_scroll_offset();
    }
    else {
        o << (v.is_pressed() ? 'P' : 'R');
        if (v.has_shift()) {
            o << 'S';
        }
        if (v.has_control()) {
            o << 'C';
        }
        if (v.has_alt()) {
            o << 'A';
        }
        if (v.has_super()) {
            // OS key.
            o << 'O';
        }
        if (v.has_caps_lock()) {
            o << 'L';
        }
        if (v.has_num_lock()) {
            o << 'N';
        }
    }
    return o;
}

glm::dvec2 MouseMovement::normalized(glm::ivec2 window_size,
                                     glm::ivec2 framebuffer_size) const {
    glm::dvec2 fs = framebuffer_size;
    glm::dvec2 ws = window_size;
    glm::dvec2 screen_pixel_scale = fs / ws;
    glm::dvec2 screen_pos(_x, _y);
    glm::dvec2 scaled_screen_pos = (screen_pos * screen_pixel_scale) + glm::dvec2(0.5, 0.5);
    glm::dvec2 glpos = glm::dvec2(scaled_screen_pos.x, fs.y - scaled_screen_pos.y);
    glpos = ((glpos / fs) * 2.0) - 1.0;
    return glpos;
}

std::ostream& operator<<(std::ostream& o, const MouseMovement& v) {
    return o << v.get_x() << ", " << v.get_y();
}

}  // namespace resatr
