#ifndef RESATR_COLOR_HPP
#define RESATR_COLOR_HPP
/**
 * @file
 * @brief Packed color formats.
 */

#include <Attr.hpp>

#include <cstdint>

namespace resatr {

class pack_struct RGBA {
   public:
    uint8_t r;
    uint8_t g;
    uint8_t b;
    uint8_t a;

    RGBA() : r(0), g(0), b(0), a(255) {}

    RGBA(uint8_t red, uint8_t green, uint8_t blue) : r(red), g(green), b(blue), a(255) {}

    RGBA(uint8_t red, uint8_t green, uint8_t blue, uint8_t alpha) :
        r(red), g(green), b(blue), a(alpha) {}
};

}  // namespace resatr

#endif /*RESATR_COLOR_HPP*/
