#ifndef RESATR_LOADSCHEDULER_HPP
#define RESATR_LOADSCHEDULER_HPP
/**
 * @file
 * @brief Base class for queueing of Loadable and Uploadable OpenGL/Vulkan entities.
 */

#include <Attr.hpp>
#include <Loadable.hpp>
#include <Uploadable.hpp>
#include <Utils.hpp>

#include <cstddef>
#include <limits>
#include <stdexcept>

namespace resatr {

/**
 * Abstact base class.
 */
class LoadScheduler : public spec::INonCopyable {
   public:
    /**
     * Register a Loadable with an associated Uploadable.
     */
    virtual void append(Loadable&, Uploadable&) = 0;

    /**
     * Register a Loadable which is also an Uploadable.
     */
    template<typename T> void append(T& o) {
        append(o, o);
    }

    /**
     * Get load progress of queued Loadable(s).
     * Returns a value between [0, 1].
     */
    virtual float get_load_progress() = 0;

    /**
     * Get total amount of entities currently in upload queue.
     */
    virtual size_t get_upload_queue_length() = 0;

    /**
     * Get total upload cost of all entities currently in queue.
     */
    virtual size_t get_upload_queue_cost() = 0;

    /**
     * Upload queued entities.
     *
     * Uses up to \p available_tokens.
     * Returns the actual count of used tokens.
     *
     * For details on the token metric
     * refer to the Uploadable interface.
     */
    virtual size_t upload(size_t available_tokens = std::numeric_limits<size_t>::max()) = 0;

    LoadScheduler() = default;
    virtual ~LoadScheduler() = default;
};

/**
 * Default LoadScheduler implementation
 * with no actual ability for loading/uploading.
 */
class NullLoadScheduler final : public LoadScheduler {
   public:
    NullLoadScheduler() = default;

    ~NullLoadScheduler() = default;

    virtual void append(Loadable&, Uploadable&) override {
        throw std::logic_error("NullLoadScheduler does not support Loadables!");
    }

    virtual float get_load_progress() override {
        return 1.0;
    };

    virtual size_t get_upload_queue_length() override {
        return 0;
    };

    virtual size_t get_upload_queue_cost() override {
        return 0;
    };

    virtual size_t upload(
         unused size_t available_tokens = std::numeric_limits<size_t>::max()) override {
        return 0;
    };
};

}  // namespace resatr

#endif /*RESATR_LOADSCHEDULER_HPP*/
