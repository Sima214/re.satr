#include "RenderLoop.hpp"

#include <Scene.hpp>
#include <Trace.hpp>
#include <Unicode.hpp>
#include <glfw.hpp>
#include <glm/vec2.hpp>

#include <functional>

static void _renderloop_on_size_change(resatr::RenderLoop* o, glfw::Window&,
                                       glfw::Window::Size size) {
    o->set_size(size);
}

static void _renderloop_on_keyboard(resatr::RenderLoop* o, glfw::Window&, int key, int scancode,
                                    int action, int mods) {
    if (action != GLFW_REPEAT) {
        if (key != GLFW_KEY_UNKNOWN) {
            resatr::KeyboardButtonPress ev(key, action, mods);
            o->on_keyboard_button_press(ev);
        }
        else {
            spec::trace("Pressed unknown key ", key, " with scancode ", scancode);
        }
    }
}

static void _renderloop_on_character(resatr::RenderLoop* o, glfw::Window&,
                                     unsigned int codepoint) {
    auto utf8 = spec::utf8_codepoint::get(codepoint).to_string();
    o->append_typed_chars(utf8);
}

static void _renderloop_on_mouse_button(resatr::RenderLoop* o, glfw::Window&, int button,
                                        int action, int mods) {
    resatr::MouseButtonPress ev(button, action, mods);
    o->on_mouse_button_press(ev);
}

static void _renderloop_on_mouse_scroll(resatr::RenderLoop* o, glfw::Window&, double xoffset,
                                        double yoffset) {
    if (yoffset > 0) {
        resatr::MouseButtonPress ev(resatr::MouseButtonPress::Keys::SCROLL_UP, yoffset);
        o->on_mouse_button_press(ev);
    }
    else if (yoffset < 0) {
        resatr::MouseButtonPress ev(resatr::MouseButtonPress::Keys::SCROLL_DOWN, -yoffset);
        o->on_mouse_button_press(ev);
    }
    else if (xoffset > 0) {
        resatr::MouseButtonPress ev(resatr::MouseButtonPress::Keys::SCROLL_LEFT, xoffset);
        o->on_mouse_button_press(ev);
    }
    else if (xoffset < 0) {
        resatr::MouseButtonPress ev(resatr::MouseButtonPress::Keys::SCROLL_RIGHT, -xoffset);
        o->on_mouse_button_press(ev);
    }
}

static void _renderloop_on_mouse_move(resatr::RenderLoop* o, glfw::Window& w, double xpos,
                                      double ypos) {
    if (!w.is_mouse_captured()) {
        resatr::MouseMovement ev(xpos, ypos);
        o->on_mouse_movement(ev);
    }
}

resatr::RenderLoop::RenderLoop(glfw::Window& context, LoadScheduler& scheduler,
                               Presenter& presenter) :
    Scene(*this, *this),
    _context(context), _scheduler(scheduler), _presenter(presenter) {
    // Attach callbacks.
    using namespace std::placeholders;
    auto fbsize_binded_cb = std::bind(_renderloop_on_size_change, this, _1, _2);
    _uid_cb_fbsize = _context.register_callback_framebuffer_size(fbsize_binded_cb);
    auto keys_binded_cb = std::bind(_renderloop_on_keyboard, this, _1, _2, _3, _4, _5);
    _uid_cb_keys = _context.register_callback_keypress(keys_binded_cb);
    auto chars_binded_cb = std::bind(_renderloop_on_character, this, _1, _2);
    _uid_cb_chars = _context.register_callback_char_input(chars_binded_cb);
    auto mouse_button_binded_cb = std::bind(_renderloop_on_mouse_button, this, _1, _2, _3, _4);
    _uid_cb_buttons = _context.register_callback_mouse_button(mouse_button_binded_cb);
    auto mouse_scroll_binded_cb = std::bind(_renderloop_on_mouse_scroll, this, _1, _2, _3);
    _uid_cb_scrolls = _context.register_callback_mouse_scroll(mouse_scroll_binded_cb);
    auto mouse_move_binded_cb = std::bind(_renderloop_on_mouse_move, this, _1, _2, _3);
    _uid_cb_move = _context.register_callback_mouse_position(mouse_move_binded_cb);
    // Misc initializations.
    _character_buffer.reserve(4096);
}

resatr::RenderLoop::~RenderLoop() {
    // Detach callbacks.
    _context.unregister_callback_framebuffer_size(_uid_cb_fbsize);
    _context.unregister_callback_keypress(_uid_cb_keys);
    _context.unregister_callback_char_input(_uid_cb_chars);
    _context.unregister_callback_mouse_button(_uid_cb_buttons);
    _context.unregister_callback_mouse_scroll(_uid_cb_scrolls);
    _context.unregister_callback_mouse_position(_uid_cb_move);
}

void resatr::RenderLoop::present() {
    _presenter.present();
}

void resatr::RenderLoop::capture_mouse() {
    if (!_context.is_mouse_captured()) {
        _context.capture_mouse();
        // Set starting point.
        _relative_last = resatr::MouseMovement(_context.get_mouse_pos());
    }
}

void resatr::RenderLoop::release_mouse() {
    _context.release_mouse();
    // Reset starting point.
    _relative_last = resatr::MouseMovement();
}

double resatr::RenderLoop::get_loop_time() {
    return glfw::GLFW::get_instance().get_time() - _loop_timer_offset;
}

void resatr::RenderLoop::start_loop_timer() {
    _loop_timer_offset = glfw::GLFW::get_instance().get_time();
}

void resatr::RenderLoop::append_typed_chars(std::string& s) {
    _character_buffer.append(s);
}

void resatr::RenderLoop::push_events() {
    if (_context.is_mouse_captured()) {
        resatr::MouseMovement _relative_new(_context.get_mouse_pos());
        resatr::MouseMovement _relative_diff = _relative_new - _relative_last;
        if (_relative_diff != 0) {
            on_mouse_relative_movement(_relative_diff);
        }
        _relative_last = _relative_new;
    }
    if (_character_buffer.size() > 0) {
        on_text_input(_character_buffer.data());
        _character_buffer.clear();
    }
}
