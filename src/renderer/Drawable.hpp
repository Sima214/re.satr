#ifndef RESATR_DRAWABLE_HPP
#define RESATR_DRAWABLE_HPP
/**
 * @file
 * @brief A drawable target. Either a framebuffer or a window.
 */

#include <Attr.hpp>
#include <Color.hpp>
#include <Utils.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

#include <cstdint>
#include <vector>

namespace resatr {

class Drawable : public virtual spec::INonCopyable {
   protected:
    bool _has_clear_color : 1;
    bool _has_clear_depth : 1;
    bool _has_clear_stencil : 1;
    glm::vec4 _clear_color;
    double _clear_depth;
    int32_t _clear_stencil;
    glm::ivec2 _size;

   public:
    /**
     * Clears buffer with the specified opaque color in \p color.
     */
    void set_clear_color(glm::vec3 color) {
        set_clear_color(glm::vec4(color, 1.0));
    }

    /**
     * Clears buffer with the specified color in \p color.
     */
    void set_clear_color(glm::vec4 color) {
        _has_clear_color = true;
        _clear_color = color;
    };

    void set_clear_depth(double depth) {
        _has_clear_depth = true;
        _clear_depth = depth;
    }

    void set_clear_stencil(int32_t stencil) {
        _has_clear_stencil = true;
        _clear_stencil = stencil;
    }

    /**
     * Perform clear operation as configured with set_clear_*.
     */
    virtual void clear() = 0;

    /**
     * Combine this drawable's contents
     * with the other currently targeted drawable.
     */
    virtual void draw() = 0;

    /**
     * Sets this drawable as the target for
     * the following draw/render commands.
     */
    virtual void target() = 0;

    /**
     * Reads RGBA color data as packed bytes.
     */
    virtual std::vector<RGBA> read_color() = 0;

    /**
     * Reads RGBA color data as normalized floats.
     */
    virtual std::vector<glm::vec4> read_hdr() = 0;

    /**
     * Reads depth data as floats.
     */
    virtual std::vector<float> read_depth() = 0;

    /**
     * Reads stencil data as 32-bit integers.
     */
    virtual std::vector<uint32_t> read_stencil() = 0;

    /**
     * Get buffer width, height.
     */
    glm::ivec2 get_size() {
        return _size;
    }

    /**
     * Set buffer width, height.
     */
    void set_size(glm::ivec2 dim) {
        glm::ivec2 old = _size;
        _size = dim;
        on_size_update(old, dim);
    }

   protected:
    virtual void on_size_update(glm::ivec2 old_size, glm::ivec2 new_size) = 0;

   public:
    Drawable& operator=(Drawable&&) = default;
    Drawable(Drawable&&) = default;
    Drawable() :
        _has_clear_color(false), _has_clear_depth(false), _has_clear_stencil(false),
        _clear_color(0.0, 0.0, 0.0, 1.0), _clear_depth(0.0), _clear_stencil(0), _size(-1, -1) {}

    virtual ~Drawable() = default;
};

}  // namespace resatr

#endif /*RESATR_DRAWABLE_HPP*/
