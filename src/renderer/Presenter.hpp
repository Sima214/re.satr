#ifndef RESATR_PRESENTER_HPP
#define RESATR_PRESENTER_HPP
/**
 * @file
 * @brief Manages buffer swapping and is used to limit framerate.
 */

#include <StopWatch.hpp>
#include <Trace.hpp>
#include <Utils.hpp>
#include <glfw.hpp>

#include <ostream>

namespace resatr {

class Presenter : public spec::INonCopyable {
   public:
    enum Modes { Loading, Rendering };

   protected:
    glfw::Window& _window;

    spec::StopWatch _present_timer;
    size_t _frame_counter;

   public:
    Presenter(glfw::Window& window) : _window(window) {
        _frame_counter = 0;
    }

    /**
     * Returns the current frame counter.
     */
    size_t get_current_frame() {
        return _frame_counter;
    }

    /**
     * Called after construction and
     * before entering main loop.
     */
    virtual void attach() {
        spec::trace("Presenter::attach");
        _present_timer.start();
    };

    /**
     * Returns current mode of the Renderer.
     * Different modes split rendering time differently.
     */
    virtual Modes get_mode() = 0;

    /**
     * Gets time between presentations.
     *
     * How this is exactly calculated is
     * implementation dependent.
     */
    virtual float get_frametime() = 0;

    /**
     * Output human readable stats.
     *
     * Exactly what is completly
     * implementation dependent.
     */
    virtual void print_stats(std::ostream& o) = 0;

    /**
     * Swaps buffer and everything else this
     * Presenter is responsible for.
     */
    virtual void present() {
        _frame_counter++;
    };

    Presenter() = delete;
    virtual ~Presenter() = default;
};

inline std::ostream& operator<<(std::ostream& o, Presenter& v) {
    v.print_stats(o);
    return o;
}

}  // namespace resatr

#endif /*RESATR_PRESENTER_HPP*/
