#ifndef RESATR_RENDERLOOP_HPP
#define RESATR_RENDERLOOP_HPP
/**
 * @file
 * @brief RenderLoop class:
 * a drawable managing the window,
 * a presenter, a loader and
 * all shader/buffer resources.
 * Also subclasses are responsible
 * for distributing events.
 */

#include <Drawable.hpp>
#include <LoadScheduler.hpp>
#include <Presenter.hpp>
#include <Renderable.hpp>
#include <Scene.hpp>
#include <StopWatch.hpp>
#include <glfw.hpp>

#include <functional>
#include <string>

namespace resatr {

class RenderLoop : public Drawable, public Renderable, public Scene {
   protected:
    glfw::Window& _context;
    LoadScheduler& _scheduler;
    Presenter& _presenter;

   private:
    size_t _uid_cb_fbsize;
    size_t _uid_cb_keys;
    size_t _uid_cb_chars;
    size_t _uid_cb_buttons;
    size_t _uid_cb_scrolls;
    size_t _uid_cb_move;

    std::string _character_buffer;

    MouseMovement _relative_last;
    double _loop_timer_offset;

   public:
    /**
     * Initializes a new RenderLoop.
     */
    RenderLoop(glfw::Window& context, LoadScheduler& scheduler, Presenter& presenter);

    ~RenderLoop();

    /**
     * Buffer swap in OpenGL terms.
     */
    virtual void present();

    /**
     * Enter mainloop.
     *
     * Each iteration 3 main things are done:
     * 1. Input event handling.
     * 2. Screen rendering by calling this->render().
     * 3. Screen update by calling _presenter.present().
     */
    virtual void loop() = 0;

    /**
     * Start relative cursor mode input.
     */
    void capture_mouse();
    /**
     * End relative cursor mode input.
     */
    void release_mouse();

    /* Getters. */
    glfw::Window& get_context() {
        return _context;
    }
    LoadScheduler& get_scheduler() {
        return _scheduler;
    }
    Presenter& get_presenter() {
        return _presenter;
    }

    /**
     * Gets currently elapsed time
     * since the start of the
     * main loop in seconds.
     */
    double get_loop_time();

    /**
     * Append characters to the buffer for the text input event.
     * Intended to be used internally.
     */
    void append_typed_chars(std::string& s);

   protected:
    /**
     * Must be called at start of loop().
     */
    void start_loop_timer();

    /**
     * Must be called right after calling
     * one of glfw::*_events*.
     */
    void push_events();
};

}  // namespace resatr

#endif /*RESATR_RENDERLOOP_HPP*/
