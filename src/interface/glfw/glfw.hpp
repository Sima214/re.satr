#ifndef RESATR_GLFW_HPP
#define RESATR_GLFW_HPP
/**
 * @file
 * @brief C++ header interface for GLFW3.
 */

#include <Attr.hpp>
#include <DispatchTable.hpp>
#include <Trace.hpp>
#include <Utils.hpp>
#include <glm/vec2.hpp>

#include <chrono>
#include <cstddef>
#include <cstdint>
#include <functional>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>

#include <GLFW/glfw3.h>
#include <glad.h>
#include <macros.h>

namespace gl {

using Version = ::gladGLversionStruct;

}  // namespace gl

inline std::ostream& operator<<(std::ostream& out, const gl::Version& v) {
    return out << v.major << '.' << v.minor;
}

namespace glfw {

class glfw_error : public std::runtime_error {
   protected:
    int code;
    std::string description;

   public:
    glfw_error() : std::runtime_error("") {
        // NOTE: slow code path.
        const char* glfw_desc = nullptr;
        int glfw_code = glfwGetError(&glfw_desc);
        set_error_msg(glfw_code, glfw_desc);
    }

    glfw_error(int glfw_code, const char* glfw_desc) : std::runtime_error("") {
        set_error_msg(glfw_code, glfw_desc);
    }

    virtual char const* what() const noexcept override {
        return description.c_str();
    }

    static void check() {
        const char* glfw_desc = nullptr;
        int glfw_code = glfwGetError(&glfw_desc);
        if (glfw_code != GLFW_NO_ERROR) {
            throw glfw_error(glfw_code, glfw_desc);
        }
    }

   private:
    void set_error_msg(int glfw_code, const char* glfw_desc) {
        code = glfw_code;
        std::stringstream tmp("GLFW Error: ");
        if (glfw_desc == nullptr) {
            tmp << code;
        }
        else {
            tmp << glfw_desc << "(" << code << ")";
        }
        description = tmp.str();
    }
};

struct Version {
    int major = -1;
    int minor = -1;
    int revision = -1;

    Version() = default;
    Version(int _major, int _minor, int _revision) :
        major(_major), minor(_minor), revision(_revision) {}

    static Version get_compiled_version() {
        return Version(GLFW_VERSION_MAJOR, GLFW_VERSION_MINOR, GLFW_VERSION_REVISION);
    }

    static Version get_runtime_version() {
        Version r;
        glfwGetVersion(&r.major, &r.minor, &r.revision);
        return r;
    }
};

class GLFWHints {
   private:
    int joystick_hat_buttons = -1;
    int cocoa_chdir_resources = -1;
    int cocoa_menubar = -1;

   public:
    GLFWHints() = default;

    void set_joystick_hat_buttons(bool v) {
        joystick_hat_buttons = v ? GLFW_TRUE : GLFW_FALSE;
    }
    void set_cocoa_chdir_resources(bool v) {
        cocoa_chdir_resources = v ? GLFW_TRUE : GLFW_FALSE;
    }
    void set_cocoa_menubar(bool v) {
        cocoa_menubar = v ? GLFW_TRUE : GLFW_FALSE;
    }

    void apply() {
        if (joystick_hat_buttons != -1) {
            glfwInitHint(GLFW_JOYSTICK_HAT_BUTTONS, joystick_hat_buttons);
        }
        if (cocoa_chdir_resources != -1) {
            glfwInitHint(GLFW_COCOA_CHDIR_RESOURCES, cocoa_chdir_resources);
        }
        if (cocoa_menubar != -1) {
            glfwInitHint(GLFW_COCOA_MENUBAR, cocoa_menubar);
        }
    }
};

class GLFW : public spec::INonCopyable {
   private:
    GLFW() {
        int status = glfwInit();
        if (!status) {
            throw glfw_error();
        }
        spec::trace("glfw::loaded");
    }

   public:
    static GLFW& get_instance();
    /**
     * Create an instance with hints
     * If an instance has already been created,
     * then \p hints has no effect.
     */
    static GLFW& get_instance(GLFWHints hints) {
        hints.apply();
        return get_instance();
    }

    void poll_events() {
        glfwPollEvents();
        glfw_error::check();
    }
    void wait_events() {
        glfwWaitEvents();
        glfw_error::check();
    }
    template<typename D> void wait_events(D timeout) {
        auto seconds =
             std::chrono::duration_cast<std::chrono::duration<double, std::ratio<1, 1>>>(
                  timeout);
        glfwWaitEventsTimeout(seconds.count());
    }

    Version get_compiled_version() {
        return Version::get_compiled_version();
    }

    Version get_runtime_version() {
        return Version::get_runtime_version();
    }

    double get_time() {
        return glfwGetTime();
    }

    ~GLFW() {
        spec::trace("glfw::unloading");
        glfwTerminate();
        spec::trace("glfw::unloaded");
    }
};

class WindowHints {
   public:
    enum ClientApi {
        OpenGL = GLFW_OPENGL_API,
        OpenGLES = GLFW_OPENGL_ES_API,
        None = GLFW_NO_API
    };

    enum ContextCreationApi {
        NativeContextApi = GLFW_NATIVE_CONTEXT_API,
        EglContextApi = GLFW_EGL_CONTEXT_API,
        OsmesaContextApi = GLFW_OSMESA_CONTEXT_API
    };

    enum OpenglProfile {
        OpenGLAnyProfile = GLFW_OPENGL_ANY_PROFILE,
        OpenGLCompatProfile = GLFW_OPENGL_COMPAT_PROFILE,
        OpenGLCoreProfile = GLFW_OPENGL_CORE_PROFILE
    };

    enum ContextRobustness {
        NoRobustness = GLFW_NO_ROBUSTNESS,
        LoseContextOnReset = GLFW_LOSE_CONTEXT_ON_RESET,
        NoResetNotification = GLFW_NO_RESET_NOTIFICATION
    };

    enum ContextReleaseBehavior {
        AnyReleaseBehavior = GLFW_ANY_RELEASE_BEHAVIOR,
        ReleaseBehaviorNone = GLFW_RELEASE_BEHAVIOR_NONE,
        ReleaseBehaviorFlush = GLFW_RELEASE_BEHAVIOR_FLUSH
    };

   private:
    std::int8_t resizable = -1;
    std::int8_t visible = -1;
    std::int8_t decorated = -1;
    std::int8_t focused = -1;
    std::int8_t auto_iconify = -1;
    std::int8_t floating = -1;
    std::int8_t maximized = -1;
    std::int8_t center_cursor = -1;
    std::int8_t transparent_framebuffer = -1;
    std::int8_t focus_on_show = -1;
    std::int8_t scale_to_monitor = -1;

    std::int16_t red_bits = GLFW_DONT_CARE;
    std::int16_t green_bits = GLFW_DONT_CARE;
    std::int16_t blue_bits = GLFW_DONT_CARE;
    std::int16_t alpha_bits = GLFW_DONT_CARE;
    std::int16_t depth_bits = GLFW_DONT_CARE;
    std::int16_t stencil_bits = GLFW_DONT_CARE;
    std::int16_t accum_red_bits = GLFW_DONT_CARE;
    std::int16_t accum_green_bits = GLFW_DONT_CARE;
    std::int16_t accum_blue_bits = GLFW_DONT_CARE;
    std::int16_t accum_alpha_bits = GLFW_DONT_CARE;
    int aux_buffers = GLFW_DONT_CARE;

    int samples = GLFW_DONT_CARE;
    int refresh_rate = GLFW_DONT_CARE;

    std::int8_t stereo = -1;
    std::int8_t srgb_capable = -1;
    std::int8_t doublebuffer = -1;

    ClientApi client_api = ClientApi::OpenGL;
    ContextCreationApi context_creation_api = ContextCreationApi::NativeContextApi;
    int context_version_major = -1;
    int context_version_minor = -1;
    std::int8_t opengl_forward_compat = -1;
    std::int8_t opengl_debug_context = -1;
    OpenglProfile opengl_profile = OpenglProfile::OpenGLAnyProfile;
    ContextRobustness context_robustness = ContextRobustness::NoRobustness;
    ContextReleaseBehavior context_release_behavior =
         ContextReleaseBehavior::AnyReleaseBehavior;
    std::int8_t context_no_error = -1;

    std::int8_t cocoa_retina_framebuffer = -1;
    std::string cocoa_frame_name;
    std::int8_t cocoa_graphics_switching = -1;

    std::string x11_class_name;
    std::string x11_instance_name;

   public:
    WindowHints() = default;

    void set_resizable(bool v) {
        resizable = v;
    }
    void set_visible(bool v) {
        visible = v;
    }
    void set_decorated(bool v) {
        decorated = v;
    }
    void set_focused(bool v) {
        focused = v;
    }
    void set_auto_iconify(bool v) {
        auto_iconify = v;
    }
    void set_floating(bool v) {
        floating = v;
    }
    void set_maximized(bool v) {
        maximized = v;
    }
    void set_center_cursor(bool v) {
        center_cursor = v;
    }
    void set_transparent_framebuffer(bool v) {
        transparent_framebuffer = v;
    }
    void set_focus_on_show(bool v) {
        focus_on_show = v;
    }
    void set_scale_to_monitor(bool v) {
        scale_to_monitor = v;
    }

    void set_red_bits(int v) {
        red_bits = v;
    }
    void set_green_bits(int v) {
        green_bits = v;
    }
    void set_blue_bits(int v) {
        blue_bits = v;
    }
    void set_alpha_bits(int v) {
        alpha_bits = v;
    }
    void set_depth_bits(int v) {
        depth_bits = v;
    }
    void set_stencil_bits(int v) {
        stencil_bits = v;
    }
    void set_accum_red_bits(int v) {
        accum_red_bits = v;
    }
    void set_accum_green_bits(int v) {
        accum_green_bits = v;
    }
    void set_accum_blue_bits(int v) {
        accum_blue_bits = v;
    }
    void set_accum_alpha_bits(int v) {
        accum_alpha_bits = v;
    }
    void set_aux_buffers(int v) {
        aux_buffers = v;
    }

    void set_samples(int v) {
        samples = v;
    }
    void set_refresh_rate(int v) {
        refresh_rate = v;
    }

    void set_stereo(bool v) {
        stereo = v;
    }
    void set_srgb_capable(bool v) {
        srgb_capable = v;
    }
    void set_doublebuffer(bool v) {
        doublebuffer = v;
    }

    void set_client_api(ClientApi v) {
        client_api = v;
    }
    void set_context_creation_api(ContextCreationApi v) {
        context_creation_api = v;
    }
    void set_context_version_major(int v) {
        context_version_major = v;
    }
    void set_context_version_minor(int v) {
        context_version_minor = v;
    }
    void set_opengl_forward_compat(bool v) {
        opengl_forward_compat = v;
    }
    void set_opengl_debug_context(bool v) {
        opengl_debug_context = v;
    }
    void set_opengl_profile(OpenglProfile v) {
        opengl_profile = v;
    }
    void set_context_robustness(ContextRobustness v) {
        context_robustness = v;
    }
    void set_context_release_behavior(ContextReleaseBehavior v) {
        context_release_behavior = v;
    }
    void set_context_no_error(bool v) {
        context_no_error = v;
    }

    void set_cocoa_retina_framebuffer(bool v) {
        cocoa_retina_framebuffer = v;
    }
    void set_cocoa_frame_name(const char* v) {
        cocoa_frame_name = v;
    }
    void set_cocoa_frame_name(std::string& v) {
        cocoa_frame_name = v;
    }
    void set_cocoa_graphics_switching(bool v) {
        cocoa_graphics_switching = v;
    }

    void set_x11_class_name(const char* v) {
        x11_class_name = v;
    }
    void set_x11_class_name(std::string& v) {
        x11_class_name = v;
    }
    void set_x11_instance_name(const char* v) {
        x11_instance_name = v;
    }
    void set_x11_instance_name(std::string& v) {
        x11_instance_name = v;
    }

    void apply() {
        if (resizable != -1) {
            glfwWindowHint(GLFW_RESIZABLE, resizable);
        }
        if (visible != -1) {
            glfwWindowHint(GLFW_VISIBLE, visible);
        }
        if (decorated != -1) {
            glfwWindowHint(GLFW_DECORATED, decorated);
        }
        if (focused != -1) {
            glfwWindowHint(GLFW_FOCUSED, focused);
        }
        if (auto_iconify != -1) {
            glfwWindowHint(GLFW_AUTO_ICONIFY, auto_iconify);
        }
        if (floating != -1) {
            glfwWindowHint(GLFW_FLOATING, floating);
        }
        if (maximized != -1) {
            glfwWindowHint(GLFW_MAXIMIZED, maximized);
        }
        if (center_cursor != -1) {
            glfwWindowHint(GLFW_CENTER_CURSOR, center_cursor);
        }
        if (transparent_framebuffer != -1) {
            glfwWindowHint(GLFW_TRANSPARENT_FRAMEBUFFER, transparent_framebuffer);
        }
        if (focus_on_show != -1) {
            glfwWindowHint(GLFW_FOCUS_ON_SHOW, focus_on_show);
        }
        if (scale_to_monitor != -1) {
            glfwWindowHint(GLFW_SCALE_TO_MONITOR, scale_to_monitor);
        }

        glfwWindowHint(GLFW_RED_BITS, red_bits);
        glfwWindowHint(GLFW_GREEN_BITS, green_bits);
        glfwWindowHint(GLFW_BLUE_BITS, blue_bits);
        glfwWindowHint(GLFW_ALPHA_BITS, alpha_bits);
        glfwWindowHint(GLFW_DEPTH_BITS, depth_bits);
        glfwWindowHint(GLFW_STENCIL_BITS, stencil_bits);
        glfwWindowHint(GLFW_ACCUM_RED_BITS, accum_red_bits);
        glfwWindowHint(GLFW_ACCUM_GREEN_BITS, accum_green_bits);
        glfwWindowHint(GLFW_ACCUM_BLUE_BITS, accum_blue_bits);
        glfwWindowHint(GLFW_ACCUM_ALPHA_BITS, accum_alpha_bits);
        glfwWindowHint(GLFW_AUX_BUFFERS, aux_buffers);

        glfwWindowHint(GLFW_SAMPLES, samples);
        glfwWindowHint(GLFW_REFRESH_RATE, refresh_rate);

        if (stereo != -1) {
            glfwWindowHint(GLFW_STEREO, stereo);
        }
        if (srgb_capable != -1) {
            glfwWindowHint(GLFW_SRGB_CAPABLE, srgb_capable);
        }
        if (doublebuffer != -1) {
            glfwWindowHint(GLFW_DOUBLEBUFFER, doublebuffer);
        }

        glfwWindowHint(GLFW_CLIENT_API, client_api);
        glfwWindowHint(GLFW_CONTEXT_CREATION_API, context_creation_api);
        if (context_version_major != -1) {
            glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, context_version_major);
        }
        if (context_version_minor != -1) {
            glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, context_version_minor);
        }
        if (opengl_forward_compat != -1) {
            glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, opengl_forward_compat);
        }
        if (opengl_debug_context != -1) {
            glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, opengl_debug_context);
        }
        glfwWindowHint(GLFW_OPENGL_PROFILE, opengl_profile);
        glfwWindowHint(GLFW_CONTEXT_ROBUSTNESS, context_robustness);
        glfwWindowHint(GLFW_CONTEXT_RELEASE_BEHAVIOR, context_release_behavior);
        if (context_no_error != -1) {
            glfwWindowHint(GLFW_CONTEXT_NO_ERROR, context_no_error);
        }
        if (cocoa_retina_framebuffer != -1) {
            glfwWindowHint(GLFW_COCOA_RETINA_FRAMEBUFFER, cocoa_retina_framebuffer);
        }
        glfwWindowHintString(GLFW_COCOA_FRAME_NAME, cocoa_frame_name.c_str());
        if (cocoa_graphics_switching != -1) {
            glfwWindowHint(GLFW_COCOA_GRAPHICS_SWITCHING, cocoa_graphics_switching);
        }
        glfwWindowHintString(GLFW_X11_CLASS_NAME, x11_class_name.c_str());
        glfwWindowHintString(GLFW_X11_INSTANCE_NAME, x11_instance_name.c_str());
    }

    void reset() {
        glfwDefaultWindowHints();
    }
};

class Window {
   public:
    struct Size {
        int width = -1;
        int height = -1;

        Size() = default;
        Size(int w, int h) : width(w), height(h) {}

        operator glm::ivec2() {
            return glm::ivec2(width, height);
        }
    };

   protected:
    /**
     * Cached attributes.
     */
    Size _framebuffer_size;
    Size _window_size;

    /**
     * Callback templates.
     */
    using fbsize_cb = void(Window&, Size);
    using keypress_cb = void(Window&, int key, int scancode, int action, int mods);
    using char_cb = void(Window&, unsigned int codepoint);
    using mouse_pos_cb = void(Window&, double xpos, double ypos);
    using mouse_enter_cb = void(Window&, int entered);
    using mouse_button_cb = void(Window&, int button, int action, int mods);
    using mouse_scroll_cb = void(Window&, double xoffset, double yoffset);

   private:
    GLFWwindow* _handle;
    spec::DispatchTable<fbsize_cb> fbsize_cb_dt;
    spec::DispatchTable<keypress_cb> keypress_cb_dt;
    spec::DispatchTable<char_cb> char_cb_dt;
    spec::DispatchTable<mouse_pos_cb> mouse_pos_cb_dt;
    spec::DispatchTable<mouse_enter_cb> mouse_enter_cb_dt;
    spec::DispatchTable<mouse_button_cb> mouse_button_cb_dt;
    spec::DispatchTable<mouse_scroll_cb> mouse_scroll_cb_dt;

   protected:
    /**
     * Full, internal constructor.
     */
    Window(int width, int height, const char* title, GLFWmonitor* monitor, GLFWwindow* share,
           WindowHints* hints) {
        if (hints != nullptr) {
            hints->apply();
        }
        _handle = glfwCreateWindow(width, height, title, monitor, share);
        if (hints != nullptr) {
            hints->reset();
        }
        if (_handle == nullptr) {
            throw glfw_error();
        }
        // Prepare cached attributes.
        glfwGetFramebufferSize(_handle, &_framebuffer_size.width, &_framebuffer_size.height);
        glfwGetWindowSize(_handle, &_window_size.width, &_window_size.height);
        // Prepare callbacks++.
        _crossreference();
        glfwSetFramebufferSizeCallback(_handle, on_framebuffer_size_change);
        glfwSetWindowSizeCallback(_handle, on_window_size_change);
        glfwSetKeyCallback(_handle, on_keypress);
        glfwSetCharCallback(_handle, on_char_input);
        glfwSetCursorPosCallback(_handle, on_mouse_position);
        glfwSetCursorEnterCallback(_handle, on_mouse_enter);
        glfwSetMouseButtonCallback(_handle, on_mouse_button);
        glfwSetScrollCallback(_handle, on_mouse_scroll);
    }

    static pure void* get_proc_address(const char* name) {
        void* r = (void*) glfwGetProcAddress(name);
        glfw_error::check();
        return r;
    }

    void _crossreference() {
        glfwSetWindowUserPointer(_handle, this);
    }
    static Window* _retreive_crossreference(GLFWwindow* h) {
        return (Window*) glfwGetWindowUserPointer(h);
    }

    static void on_framebuffer_size_change(GLFWwindow* gw, int width, int height);
    static void on_window_size_change(GLFWwindow* gw, int width, int height);
    static void on_keypress(GLFWwindow* gw, int key, int scancode, int action, int mods);
    static void on_char_input(GLFWwindow* gw, unsigned int codepoint);
    static void on_mouse_position(GLFWwindow* gw, double xpos, double ypos);
    static void on_mouse_enter(GLFWwindow* gw, int entered);
    static void on_mouse_button(GLFWwindow* gw, int button, int action, int mods);
    static void on_mouse_scroll(GLFWwindow* gw, double xoffset, double yoffset);

   public:
    Window(int width, int height, const char* title) :
        Window(width, height, title, nullptr, nullptr, nullptr) {}

    Window(int width, int height, std::string& title) :
        Window(width, height, title.c_str(), nullptr, nullptr, nullptr) {}

    Window(const char* title) : Window(1, 1, title, nullptr, nullptr, nullptr) {}

    Window(std::string& title) : Window(1, 1, title.c_str(), nullptr, nullptr, nullptr) {}

    Window(int width, int height, const char* title, WindowHints& hints) :
        Window(width, height, title, nullptr, nullptr, &hints) {}

    Window(int width, int height, std::string& title, WindowHints& hints) :
        Window(width, height, title.c_str(), nullptr, nullptr, &hints) {}

    Window(const char* title, WindowHints& hints) :
        Window(1, 1, title, nullptr, nullptr, &hints) {}

    Window(std::string& title, WindowHints& hints) :
        Window(1, 1, title.c_str(), nullptr, nullptr, &hints) {}

    void make_context_current() {
        glfwMakeContextCurrent(_handle);
        glfw_error::check();
        if (!gladLoadGLLoader(get_proc_address)) {
            throw std::runtime_error("Could not initialize glad!");
        }
    }

    gl::Version gl_version() {
        return GLVersion;
    }

    void swap_buffers() {
        glfwSwapBuffers(_handle);
        glfw_error::check();
    }

    Size get_framebuffer_size() {
        return _framebuffer_size;
    }

    Size get_window_size() {
        return _window_size;
    }

    /**
     * Registers a callback.
     */
    size_t register_callback_framebuffer_size(std::function<fbsize_cb> cb) {
        return fbsize_cb_dt.register_callback(cb);
    }
    size_t register_callback_keypress(std::function<keypress_cb> cb) {
        return keypress_cb_dt.register_callback(cb);
    }
    size_t register_callback_char_input(std::function<char_cb> cb) {
        return char_cb_dt.register_callback(cb);
    }
    size_t register_callback_mouse_position(std::function<mouse_pos_cb> cb) {
        return mouse_pos_cb_dt.register_callback(cb);
    }
    size_t register_callback_mouse_enter(std::function<mouse_enter_cb> cb) {
        return mouse_enter_cb_dt.register_callback(cb);
    }
    size_t register_callback_mouse_button(std::function<mouse_button_cb> cb) {
        return mouse_button_cb_dt.register_callback(cb);
    }
    size_t register_callback_mouse_scroll(std::function<mouse_scroll_cb> cb) {
        return mouse_scroll_cb_dt.register_callback(cb);
    }

    /**
     * Unregisters a previously registered callback, or crashes.
     */
    void unregister_callback_framebuffer_size(size_t uid) {
        fbsize_cb_dt.unregister_callback(uid);
    }

    void unregister_callback_keypress(size_t uid) {
        keypress_cb_dt.unregister_callback(uid);
    }
    void unregister_callback_char_input(size_t uid) {
        char_cb_dt.unregister_callback(uid);
    }
    void unregister_callback_mouse_position(size_t uid) {
        mouse_pos_cb_dt.unregister_callback(uid);
    }
    void unregister_callback_mouse_enter(size_t uid) {
        mouse_enter_cb_dt.unregister_callback(uid);
    }
    void unregister_callback_mouse_button(size_t uid) {
        mouse_button_cb_dt.unregister_callback(uid);
    }
    void unregister_callback_mouse_scroll(size_t uid) {
        mouse_scroll_cb_dt.unregister_callback(uid);
    }

    bool should_close() {
        return glfwWindowShouldClose(_handle);
    }

    void should_close(bool v) {
        glfwSetWindowShouldClose(_handle, v);
    }

    void enable_vsync() {
        if (glfwExtensionSupported("WGL_EXT_swap_control_tear") ||
            glfwExtensionSupported("GLX_EXT_swap_control_tear")) {
            glfwSwapInterval(-1);
        }
        else {
            glfwSwapInterval(1);
        }
        glfw_error::check();
    }

    void disable_vsync() {
        glfwSwapInterval(0);
        glfw_error::check();
    }

    void set_vsync(bool state) {
        if (state) {
            enable_vsync();
        }
        else {
            disable_vsync();
        }
    }

    /**
     * Returns true if mouse is captured.
     */
    bool is_mouse_captured() {
        int mode = glfwGetInputMode(_handle, GLFW_CURSOR);
        glfw_error::check();
        return mode == GLFW_CURSOR_DISABLED;
    }

    /**
     * Captures mouse and enables relative mouse positioning.
     *
     * Optionally uses raw mouse input if available.
     */
    void capture_mouse(bool raw = true) {
        if (!is_mouse_captured()) {
            if (raw && glfwRawMouseMotionSupported()) {
                glfwSetInputMode(_handle, GLFW_RAW_MOUSE_MOTION, GLFW_TRUE);
                spec::trace("glfw::window::capture_mouse::raw");
                glfw_error::check();
            }
            glfwSetInputMode(_handle, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
            glfw_error::check();
        }
    }

    /**
     * Releases the mouse if captured.
     */
    void release_mouse() {
        if (is_mouse_captured()) {
            glfwSetInputMode(_handle, GLFW_RAW_MOUSE_MOTION, GLFW_FALSE);
            glfw_error::check();
            glfwSetInputMode(_handle, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
            glfw_error::check();
        }
    }

    glm::dvec2 get_mouse_pos() {
        glm::dvec2 r;
        glfwGetCursorPos(_handle, &r.x, &r.y);
        return r;
    }

    /**
     * Move mouse to center of window.
     */
    void center_mouse() {
        Size s = get_window_size();
        glfwSetCursorPos(_handle, (double) s.width / 2, (double) s.height / 2);
        glfw_error::check();
    }

    bool has_focus() {
        bool focused = glfwGetWindowAttrib(_handle, GLFW_FOCUSED);
        glfw_error::check();
        return focused;
    }

    bool has_mouse_focus() {
        bool hovered = glfwGetWindowAttrib(_handle, GLFW_HOVERED);
        glfw_error::check();
        return hovered;
    }

    /**
     * True if window is focused and mouse
     * is inside the content area.
     */
    bool has_input() {
        return has_focus() && has_mouse_focus();
    }

    ~Window() {
        if (_handle != nullptr) {
            glfwDestroyWindow(_handle);
        }
    }
};

inline std::ostream& operator<<(std::ostream& out, const Version& v) {
    return out << v.major << '.' << v.minor << '.' << v.revision;
}

inline std::ostream& operator<<(std::ostream& out, const Window::Size& v) {
    return out << v.width << 'x' << v.height;
}

}  // namespace glfw

#endif /*RESATR_GLFW_HPP*/
