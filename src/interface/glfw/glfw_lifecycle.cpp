#include "glfw.hpp"

#include <Trace.hpp>

#include <GLFW/glfw3.h>

glfw::GLFW& glfw::GLFW::get_instance() {
    spec::trace("glfw::instance");
    static glfw::GLFW singleton;
    return singleton;
}

void glfw::Window::on_framebuffer_size_change(GLFWwindow* gw, int width, int height) {
    glfw::Window* window = glfw::Window::_retreive_crossreference(gw);
    if (window != nullptr) {
        glfw::Window::Size s(width, height);
        window->_framebuffer_size = s;
        window->fbsize_cb_dt(*window, s);
    }
}

void glfw::Window::on_window_size_change(GLFWwindow* gw, int width, int height) {
    glfw::Window* window = glfw::Window::_retreive_crossreference(gw);
    if (window != nullptr) {
        glfw::Window::Size s(width, height);
        window->_window_size = s;
    }
}

void glfw::Window::on_keypress(GLFWwindow* gw, int key, int scancode, int action, int mods) {
    glfw::Window* window = glfw::Window::_retreive_crossreference(gw);
    if (window != nullptr) {
        window->keypress_cb_dt(*window, key, scancode, action, mods);
    }
}

void glfw::Window::on_char_input(GLFWwindow* gw, unsigned int codepoint) {
    glfw::Window* window = glfw::Window::_retreive_crossreference(gw);
    if (window != nullptr) {
        window->char_cb_dt(*window, codepoint);
    }
}

void glfw::Window::on_mouse_position(GLFWwindow* gw, double xpos, double ypos) {
    glfw::Window* window = glfw::Window::_retreive_crossreference(gw);
    if (window != nullptr) {
        window->mouse_pos_cb_dt(*window, xpos, ypos);
    }
}

void glfw::Window::on_mouse_enter(GLFWwindow* gw, int entered) {
    glfw::Window* window = glfw::Window::_retreive_crossreference(gw);
    if (window != nullptr) {
        window->mouse_enter_cb_dt(*window, entered);
    }
}

void glfw::Window::on_mouse_button(GLFWwindow* gw, int button, int action, int mods) {
    glfw::Window* window = glfw::Window::_retreive_crossreference(gw);
    if (window != nullptr) {
        window->mouse_button_cb_dt(*window, button, action, mods);
    }
}

void glfw::Window::on_mouse_scroll(GLFWwindow* gw, double xoffset, double yoffset) {
    glfw::Window* window = glfw::Window::_retreive_crossreference(gw);
    if (window != nullptr) {
        window->mouse_scroll_cb_dt(*window, xoffset, yoffset);
    }
}
