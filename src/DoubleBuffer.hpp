#ifndef RESATR_DOUBLE_BUFFER_HPP
#define RESATR_DOUBLE_BUFFER_HPP
/**
 * @file
 * @brief Double buffer, but for any type of object.
 */

#include <atomic>
#include <cstdint>
#include <mutex>
#include <stdexcept>

namespace resatr {

template<typename T> class DoubleBuffer {
   protected:
    /**
     * Flag. Zero initialized by default.
     */
    std::atomic<uint_fast8_t> state;
    T a;
    T b;

   public:
    DoubleBuffer() {
        atomic_init(&state, (uint_fast8_t) 0);
    };
    ~DoubleBuffer() = default;

    T& get() {
        return get_front();
    }
    T& get_front() {
        T& ar = a;
        T& br = b;
        return state.load(std::memory_order_acquire) ? ar : br;
    }
    T& get_back() {
        T& ar = a;
        T& br = b;
        return state.load(std::memory_order_acquire) ? br : ar;
    }

    T& swap() {
        // Full mutex lock might not be required here.
        state.fetch_xor(1, std::memory_order_seq_cst);
        return get();
    }

    T& operator()() {
        return get();
    }
};

/**
 * Synchronized variation of DoubleBuffer,
 * where a thread owns exclusively a `side`.
 */
template<typename T> class SyncedDoubleBuffer {
   protected:
    /**
     * Flag. Zero initialized by default.
     */
    std::atomic<uint_fast8_t> _state;
    T _a;
    std::recursive_mutex _mutex_a;
    T _b;
    std::recursive_mutex _mutex_b;

   public:
    SyncedDoubleBuffer() {
        atomic_init(&_state, (uint_fast8_t) 0);
    };
    ~SyncedDoubleBuffer() = default;

    T& get() {
        return get_front();
    }
    T& get_front() {
        T& ar = _a;
        T& br = _b;
        bool state = _state.load(std::memory_order_acquire);
        if (state) {
            _mutex_a.lock();
            return ar;
        }
        else {
            _mutex_b.lock();
            return br;
        }
    }
    T& get_back() {
        T& ar = _a;
        T& br = _b;
        bool state = _state.load(std::memory_order_acquire);
        if (state) {
            _mutex_b.lock();
            return br;
        }
        else {
            _mutex_a.lock();
            return ar;
        }
    }

    void release(T& c) {
        T* ap = &_a;
        T* bp = &_b;
        T* cp = &c;
        if (cp == ap) {
            _mutex_a.unlock();
        }
        else if (cp == bp) {
            _mutex_b.unlock();
        }
        else {
            throw std::runtime_error("Tried to release an unknown object!");
        }
    }

    T& swap() {
        // Full mutex lock might not be required here.
        _state.fetch_xor(1, std::memory_order_seq_cst);
        return get();
    }

    T& operator()() {
        return get();
    }
};

}  // namespace resatr

#endif /*RESATR_DOUBLE_BUFFER_HPP*/
